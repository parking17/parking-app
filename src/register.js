import React, { useState }  from 'react';
import { API_URL } from '../url';
import axios from "axios";
import { StyleSheet, Text, View, Image, TouchableOpacity,TextInput,KeyboardAvoidingView,ScrollView, Alert,Modal,Dimensions,TouchableWithoutFeedback} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faGooglePlusSquare,faFacebookF} from '@fortawesome/free-brands-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { primary800,colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface, primary500,primary600,surfaceMediumEmphasis } from './utils/colorVar';
import Dialog from "react-native-dialog";
import Spinner from "react-native-loading-spinner-overlay";
import { TextInput as PaperTextInput } from 'react-native-paper';
import { Ionicons,MaterialIcons } from '@expo/vector-icons';
var width = Dimensions.get('window').width;
export default function Register ({navigation}) {
  const [email, setEmail] = useState(null);
  const [dialogVisible, setDialogVisible] = useState(false);
  const [spinner, setSpinner] = useState(false);


  const validEmail = () => {
    if(email === null){
      Alert.alert("","El correo es obligatorio");
      return false;
    }
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(email) === false) {
      Alert.alert("Correo inválido","Escriba un correo válido por favor.");
      return false;
    }else {

      setDialogVisible(true);

    }
  }
  const createAccount = (tyc) => {
    setSpinner(true);
    axios.post(`${API_URL}customer/app/create/customer`,{
      email:email,
      tyc:tyc
    }).then(response => {
      setSpinner(false);
      const cod = response.data.ResponseCode;
      console.log(`${JSON.stringify(response.data.ResponseMessage)}`);
      if(cod === 0){
        setDialogVisible(false);
        Alert.alert("",response.data.ResponseMessage);
        navigation.navigate('Login');
      }else{
        Alert.alert("ERROR","No se puede registrar");
      }
    }).catch(error => {
      setSpinner(false);
      setDialogVisible(false);
      //Alert.alert("",error.response.data.ResponseMessage.errors[0].msg);
      console.log(`${JSON.stringify(error)}`);
    })
  }

/*   const createAccount = (tyc) => {
    setSpinner(true);
    axios.post(`${API_URL}customer/app/create/customer`,{
      email:email,
      tyc:tyc
    }).then(response => {
      setSpinner(false);
      const cod = response.data.ResponseCode;
      if(cod === 0){
        setDialogVisible(false);
        Alert.alert("",response.data.ResponseMessage);
        navigation.navigate('Login');
      }else{
        Alert.alert("ERROR","No se puede registrar");
      }
    }).catch(error => {
      setSpinner(false);
      setDialogVisible(false);
      Alert.alert("",error.response.data.ResponseMessage.errors[0].msg);
      console.log(error.response.data);
    })
  } */

  const close = () => {
    global.register =  false;
    navigation.navigate('BarNavigationRegister');
  }
    return(
      <KeyboardAvoidingView style={styles.container}>
         <ScrollView  showsVerticalScrollIndicator={false}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{marginLeft:10,marginTop:10}}>
              <TouchableOpacity onPress={() => close()}>
                <MaterialIcons name="close" size={25} color={secondary} />
              </TouchableOpacity>
            </View>
            <View style={{alignItems:'center',marginBottom:130}}>
              <Image
                  source={require("../assets/register/logoGreen.png")}
                  style={{ marginTop: 40,marginBottom:40 }}
              ></Image>
              <Image
                  source={require("../assets/register/ilustracion1.png")}
                  style={{ marginBottom:30 }}
              ></Image>
              <Text style={styles.title}>Registro</Text>
              <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,marginVertical:20,fontSize:14,}}  onChangeText={text => {setEmail(text)}}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10,fonts:{medium:{fontFamily:'Gotham-Bold'}}}}
                        mode='outlined' label="Registro" outlineColor={colorInputBorder}  />
              <TouchableOpacity style={styles.button1} onPress={validEmail}>
                  <Text style={styles.titleButton1}>CREAR CUENTA</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button2} onPress={() => navigation.navigate('Login')}>
                  <Text style={styles.titleButton2}>INICIAR SESIÓN</Text>
              </TouchableOpacity>
              <Text style={{ fontSize: 15, marginTop: 20,fontFamily:'Gotham-Medium' }}>
                  O regístrate con
              </Text>
              <TouchableOpacity style={styles.button3}>
                  <Text style={styles.titleButton3}><FontAwesomeIcon size={ 16 } color={secondary}  icon={faGooglePlusSquare} style={{marginLeft:30,marginBottom:20}} />  GOOGLE</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button3}>
                  <Text style={styles.titleButton3}><FontAwesomeIcon size={ 16 } color={secondary}  icon={faFacebookF} style={{marginLeft:30,marginBottom:20}} />  FACEBOOK</Text>
              </TouchableOpacity>
            </View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={dialogVisible}
            >
              <TouchableWithoutFeedback onPress={() => setDialogVisible(!dialogVisible)}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <View style={{marginHorizontal:10,}}>
                          <Text style={styles.title2}>Te enviaremos un correo</Text>
                          <Text style={styles.text3}>Recibiras una contraseña temporal por medio del correo que ingresaste</Text>
                        </View>
                        <Text style={styles.title}>Terminos y condiciones</Text>
                        <Text style={styles.text2}>Recibiras una contraseña temporal por medio del correo que ingresaste...........</Text>
                        <View style={{marginBottom:10,alignItems: 'center',justifyContent: 'center'}}>
                            <TouchableOpacity style={styles.button1} onPress={() => createAccount(1)}>
                                <Text style={styles.titleButton1}>ACEPTAR Y CONTINUAR</Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => createAccount(0)}>
                          <Text style={{ fontSize: 15, marginTop: 20,marginBottom:30,fontFamily:'Gotham-Bold', color:primary800,textAlignVertical: "center",textAlign: "center",}}>RECHAZAR</Text>
                        </TouchableOpacity>
                    </View>
                </View>
              </TouchableWithoutFeedback>
            </Modal>
          </ScrollView>
        </KeyboardAvoidingView>
    );

}
const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: surface,
      paddingBottom:20,
    },
    title: {
        color: secondary,
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    title2: {
      color: secondary,
      fontSize: 34,
      fontFamily:'Gotham-Bold',
      textAlignVertical: "center",
      textAlign: "center",
    },
    text2: {
      color: '#000',
      fontSize: 13,
      fontFamily: 'Gotham-Light',
      marginBottom:30,
    },
    text3: {
      color: '#000',
      fontSize: 13,
      fontFamily: 'Gotham-Light',
      marginBottom:30,
      textAlignVertical: "center",
      textAlign: "center",
    },
    input:{
        height:50,  
        color: colorInput, 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        height:50,
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width:width-50,
        alignItems: "center",
      },
      button2: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width:width-50,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton2: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width:width-50,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: colorInputBorder,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })