import React, { useState }  from 'react';
import { Text, View } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faUser,faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendarAlt} from '@fortawesome/free-regular-svg-icons';
import { colorPrimaryLigth,secondary,surface } from '../utils/colorVar';
import { Ionicons,MaterialIcons,FontAwesome5 } from '@expo/vector-icons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Home from './home';
import Account from '../account/account';
import Register from '../register';
import MapsPrueba from '../parking/mapsPrueba';

const Tab = createMaterialBottomTabNavigator();

function SettingsScreen() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }

export default function BarNavigationRegister({navigation}) {
  return (
    <Tab.Navigator initialRouteName={'Mapa'}
    activeColor = {colorPrimaryLigth}
    inactiveColor = {surface}
    barStyle = { { backgroundColor : secondary } } 
    >
       <Tab.Screen name="Favoritos" component={MapsPrueba} 
        options={{tabBarIcon: ({ color }) => (
            <FontAwesomeIcon icon={faHeart} color={color} size={20} />
          ) 
        }}
      />
      <Tab.Screen name="Reservas" component={SettingsScreen}
        options={{tabBarIcon: ({ color }) => (
            <FontAwesomeIcon icon={faCalendarAlt} color={color} size={20} />
          ) 
        }}
      />
      <Tab.Screen name="Mapa" component={Home}
        options={{tabBarIcon: ({ color }) => (
          <Ionicons name="md-location-outline" size={24} color={color} />
          )
    
         }}
      />
      <Tab.Screen name="Perfil" component={global.register ? Account : Register} 
        options={{tabBarIcon: ({ color }) => (
          <FontAwesome5 name="user" size={24} color={color} />
          ) 
        }}
      />
    </Tab.Navigator>
  );
}