import React, { useState }  from 'react';
import { API_URL } from '../../url';
import {productIdReserve,productIdPasadia,productIdCortesia} from '../utils/varService';
import axios from "axios";
import { StyleSheet, Text, View, Image, TouchableOpacity,TextInput,KeyboardAvoidingView,ScrollView, Alert,SafeAreaView,Dimensions,Modal,Switch,TouchableWithoutFeedback} from 'react-native';
import { faMinus,faChevronRight,faExclamationCircle,faDirections,} from '@fortawesome/free-solid-svg-icons';
import {faHeart} from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import BottomSheet from 'reanimated-bottom-sheet';
import Carousel from "react-native-snap-carousel";
import { Chip,Snackbar } from 'react-native-paper';
import { primary300,primary800,colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface, colorGray, colorGrayOpacity, primary600, 
          primary500,surfaseDisabled,surfaceMediumEmphasis,colorPrimarySelect,onSurfaceOverlay8, surfaceHighEmphasis} from '../utils/colorVar';
import Spinner from "react-native-loading-spinner-overlay";
import { TextInput as PaperTextInput } from 'react-native-paper';
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import MapView from 'react-native-maps';
import * as Location from 'expo-location';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class Home extends React.Component {

    constructor(props){
        super(props);
        this._renderItem = this._renderItem.bind(this);
        this._renderItem2 = this._renderItem2.bind(this);
        this._renderItem3 = this._renderItem3.bind(this);
        this._renderItemParking = this._renderItemParking.bind(this);
        this.sheetRef = React.createRef(null);
        this.state = {
            activeIndex:0,
            bottomSheetVisible:true,
            textSearch:'',
            textSearchListParking:'',
            parkingCarrousel:false,
            parkingList:false,
            spinner:false,
            modalVisible:false,
            switchValue: false,
            car:false,
            moto:false,
            bici:false,
            fourthHour:false,
            filterList:'',
            carouselItems: [
                {
                  id:1,
                  name:'Mensualidades',
                  description:'Reserva un lugar para parquear el día, la hora y el tiempo que desees.',
                },
                {
                    id:21,
                    name:'Reservas',
                    description:'Reserva un lugar para parquear el día, la hora y el tiempo que desees.',
                },
                {
                    id:22,
                    name:'Pasadía',
                    description:'Reserva un lugar para parquear el día, la hora y el tiempo que desees.',
                },
                {
                  id:23,
                  name:'Valet Parking',
                  description:'Reserva un lugar para parquear el día, la hora y el tiempo que desees.',
                },
                {
                  id:24,
                  name:'Cortesias',
                  description:'Reserva un lugar para parquear el día, la hora y el tiempo que desees.',
                },
            ],
            carouselItems2: [
              {
                  id:1,
                  name:'Mensualidades',
                  description:'Reserva un lugar para parquear el día, la hora y el tiempo que desees.',
              },
            ],
            carouselItems3: [
              {
                  id:1,
                  description:'Hoy es día pico y placa para placas terminadas en 0, 1, 2',
              },
              {
                  id:2,
                  description:'Hoy es día pico y placa para placas terminadas en 0, 1, 2',
              },

            ],
            company:[
              {
                id:1,
                title:'Beneficios',
                description:'Et enim, a scelerisque massa in habitant pulvinar interdum.'
              },
              {
                id:2,
                title:'Centro de atención al usuario',
                description:'Et enim, a scelerisque massa in habitant pulvinar interdum.'
              },
            ],
            carouselItemsParking: [
                {
                    id:1,
                    name:'Parking Calle 28',
                    address:'Calle 28 # 13-32',
                    status_id:1,
                    status:'Abierto',
                    schedule:'6:00 AM - 11:00 PM',
                    price:30,
                },
                {
                    id:2,
                    name:'Parking Calle 26',
                    address:'Calle 26 # 13-32',
                    status_id:2,
                    status:'Cerrado',
                    schedule:'6:00 PM - 11:00 PM',
                    price:60,
                }
            ],
            dialogVisible:false,
            listParking:[],
            products:[
              {
                id:productIdReserve,
                name:'Reservas',
                select:false,
              },
              {
                id:productIdPasadia,
                name:'Pasadia',
                select:false,
              },
              {
                id:23,
                name:'Valet Parking',
                select:false,
              },
              {
                id:24,
                name:'Mensualidades',
                select:false,
              },
            ],
            showSnack:false,
            snackMessage:'Tu servicio de reserva ha iniciado. Puedes hacerle seguimiento ingresando a productos.',
            showSnack2:false,
            snackMessage2:'Tu servicio d.',
            location:null,
        }

    };


  /////Método switch
  toggleSwitch = (id,index) => {
    var product = this.state.products;
    product[index].select = !this.state.products[index].select; 
    this.setState({products:product});
  };
  
  
///IR A DETAILS
  goDetails = (id) => {
    global.parkingId = id;
    this.props.navigation.navigate('DetailParking');
  }

/////////Contenido de desplegable inferior//////////
    renderContent = () => (
        <ScrollView >
            <View
            style={{
                backgroundColor: colorGray,
                paddingBottom: 16,
                borderRadius:0,
            }}
            >
                
                <ScrollView>
                  <SafeAreaView style={{ flex: 1, paddingTop: 20,backgroundColor: surface,paddingLeft:16,paddingRight:16,marginBottom:10,paddingBottom:20}}>
                    <Text style={styles.title}>Servicios</Text>
                      <View
                      style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "center",
                          marginTop:10,
                      }}
                      >
                          <Carousel
                              layout={"default"}
                              ref={ref => this.carousel = ref}
                              data={this.state.carouselItems}
                              sliderWidth={270}
                              itemWidth={280}
                              renderItem={this._renderItem}
                              onSnapToItem={index => this.setState({ activeIndex: index })}
                          />
                      </View>
                  </SafeAreaView>
                  <SafeAreaView style={{flex: 1, paddingTop: 10, backgroundColor: surface,paddingLeft:16,paddingRight:16,marginBottom:10,paddingBottom:20}}>
                    <View
                      style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "center",
                          marginTop:20,
                      }}
                      >
                          <Carousel
                              layout={"default"}
                              ref={ref => this.carousel = ref}
                              data={this.state.carouselItems2}
                              sliderWidth={280}
                              itemWidth={width-100}
                              renderItem={this._renderItem2}
                              onSnapToItem={index => this.setState({ activeIndex: index })}
                          />
                    </View>
                  </SafeAreaView>
                  <SafeAreaView style={{ flex: 1, paddingTop: 20,backgroundColor: surface,paddingLeft:16,paddingRight:16,marginBottom:10,paddingBottom:20}}>
                    <Text style={styles.title}>Recuerda que...</Text>
                    <View
                    style={{
                        flex: 1,
                        flexDirection: "row",
                        justifyContent: "center",
                        marginTop:10
                    }}
                    >
                        <Carousel
                            layout={"default"}
                            ref={ref => this.carousel = ref}
                            data={this.state.carouselItems3}
                            sliderWidth={270}
                            itemWidth={280}
                            renderItem={this._renderItem3}
                            onSnapToItem={index => this.setState({ activeIndex: index })}
                        />
                    </View>
                  </SafeAreaView>
                  <View style={{backgroundColor: surface,paddingTop:20,paddingLeft:16,paddingRight:16,marginBottom:10,paddingBottom:20}}>
                    <Text style={styles.title}>La compañía</Text>
                    <View style={{marginTop:10}}>
                        {
                        this.state.company.map((com) => {
                            return  <TouchableOpacity key={com.id}>
                                    <View style={{
                                        backgroundColor: colorGrayOpacity,
                                        borderRadius: 5,
                                        marginLeft: 15,
                                        marginRight: 1,
                                        marginBottom:5,
                                        width:280,
                                        padding:15
                                    }}>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16 }}>
                                            {com.title}
                                        </Text>
                                        <View style={{flexDirection: 'row',width:230}} >
                                            <Text style={styles.text}>{com.description}</Text>
                                            <FontAwesomeIcon size={20} icon={faChevronRight} color={secondary} style={{marginTop:10}}/>
                                        </View>

                                    </View>
                                </TouchableOpacity>
                        })
                        }

                    </View>
                  </View>
                  {
                    !global.register &&
                    <View style={{justifyContent:'center',alignItems:'center'}}>
                      <TouchableOpacity style={styles.button1} onPress={() => this.setState({dialogVisible:true})}>
                            <Text style={styles.titleButton1}>REGISTRARSE</Text>
                      </TouchableOpacity>
                    </View>
                  }

                 
                </ScrollView>
            </View>

        </ScrollView>
    );
///seteo de palabra de busqueda 

  searchTextList = (text) => {
    this.setState({filterList:text});
  }
  /////////Contenido desplegable de lista de parqueaderos//////////
  renderContentListParking = () => (
    <ScrollView >
        <View
        style={{
            backgroundColor: surface,
            paddingBottom: 16,
            borderRadius:0,
        }}
        >
            <ScrollView>
              <View style={{ flex: 1, paddingTop: 20,backgroundColor: surface,paddingLeft:16,paddingRight:16,marginBottom:10,paddingBottom:20}}>
                <Text style={{color: secondary,fontSize: 18,fontFamily:'Gotham-Bold',}}>Listado de parqueaderos</Text>
                <PaperTextInput style={{width:width-40,backgroundColor:surface,color:colorInput,borderRadius:10}} 
                        right={<PaperTextInput.Icon 
                                name={() => <MaterialIcons name="search" size={30} color={secondary} />} />}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Buscar" outlineColor={colorInputBorder} onChangeText={(text) => this.searchTextList(text)}/>
                {                     
                        this.state.listParking.filter((parking) =>{
                          if(this.state.filterList == ""){
                            return parking
                          }else if(parking.commercial_name.toLowerCase().includes(this.state.filterList.toLowerCase())){
                              return parking
                          }
                        }).map( (parking) => {
                            return <TouchableOpacity style={{justifyContent:'center'}} key={parking.id} onPress={() => this.goDetails(parking.id)} >
                                        <View
                                        style={{
                                            backgroundColor: surface,
                                            borderRadius: 5,
                                            width:width-40,
                                            marginLeft: 0,
                                            marginRight: 1,
                                            shadowColor: "#000",
                                            shadowOffset: {
                                            width: 0,
                                            height: 1,
                                            },
                                            shadowOpacity: 0.49,
                                            shadowRadius: 4.65,
                                            elevation: 3,
                                            paddingBottom:10,
                                            marginTop:10,
                                            paddingTop:5,
                                        }}
                                        >
                                            <View style={{marginBottom:10,marginLeft:15}}>
                                                <View>
                                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}>
                                                        {parking.commercial_name}
                                                    </Text>
                                                    {
                                                      parking.address !== null ? 
                                                      <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:12,marginTop:5 }}>
                                                          {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}
                                                      </Text>
                                                      :
                                                      <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:12,marginTop:5 }}>
                                                          Este parqueadero no tiene dirección
                                                      </Text>

                                                    }
                                                    
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                            })
                    }
              </View>

             
            </ScrollView>
        </View>

    </ScrollView>
);
///Método para traer los parqueaderos activos
  getParkingList = () =>{
    this.setState({spinner:true});
    axios.get(`${API_URL}user/parking/list`).then(response => {
      this.setState({spinner:false});
      const cod = response.data.ResponseCode;
      if(cod === 0){
        const list = response.data.ResponseMessage;
        this.setState({bottomSheetVisible:false});
        this.setState({listParking:list});
        this.setState({parkingList:true});
      }else{
        Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
      }
    }).catch(error => {
      this.setState({spinner:false});
      Alert.alert("",error.response.data.ResponseMessage);
      console.log(error);
    })
  }

  //Método para ocultar la lista de parqueaderos
    listParkingBack = () => {
    this.setState({parkingList:false});
    this.setState({bottomSheetVisible:true});
  }


    ///Encabezado de desplegable inferior
    renderHeader = () => (
        <View
            style={{
            backgroundColor: surface,
            paddingLeft: 16,
            paddingRight: 16,
            paddingBottom: 16,
            borderRadius:0,
            height: 10,
            marginTop:20,
            }}
        >
            <View style={{alignItems: 'center',marginBottom:10}}>
                <FontAwesomeIcon size={20} icon={faMinus} color={colorInputBorder}/>
            </View>

          
        </View>
    );

  ///Dirigirse a buscar parking
  goToServices = (id) => {
    //this.props.navigation.navigate('ServiceReserve');
    console.log(id);
    if(id === 21){
      this.props.navigation.navigate('ServiceReserve');
    }
    else if ( id === 22){
      this.props.navigation.navigate('Pasadia');
    }
    else if ( id === 1){
      this.props.navigation.navigate('Mensualidad');
    }
    /* if(id === productIdReserve && global.register){
      let config = {
          headers: { Authorization: global.token }
          };
        this.setState({spinner:true});
      axios.get(`${API_URL}customer/app/product/get/status/`+productIdReserve,config).then(response => {
          this.setState({spinner:false});
          const cod = response.data.ResponseCode;
          if(cod === 0){
            if(response.data.Data){
                global.parkingId = response.data.Data.sub_product.sub_products_has_parking.parking_id;
                  global.idReserva = response.data.Data.id;
                  if(response.data.Data.status_id == 7){
                      this.props.navigation.navigate('ServiceReserve');
                  }else if(response.data.Data.status_id == 8){
                      this.props.navigation.navigate('ScheduledReserve');
                  }else if(response.data.Data.status_id == 1){
                      this.props.navigation.navigate('ActiveReserve');
                  }else if(response.data.Data.status_id == 9){
                      this.props.navigation.navigate('ExtendedReserve');
                  }
              }else{
                this.getParkingList();
              }
          }else{
          Alert.alert("ERROR","No se pudo traer el parqueadero");
          }
      }).catch(error => {
          this.setState({spinner:false});
          // Alert.alert("",error.response.data.ResponseMessage);
          console.log(error);
      })
    }else if(id == productIdCortesia && global.register){
      this.props.navigation.navigate('ListCourtesy');
    }else if(id == productIdPasadia){
      this.props.navigation.navigate('Pasadia');
    }else{
      this.getParkingList();

    } */
  }
        
/////////item de carrusel de servicios///
      _renderItem ({ item, index }) {
        return (
          <TouchableOpacity onPress={() => this.goToServices(item.id)}>
            <View
              style={{
                backgroundColor: surface,
                borderRadius: 5,
                height: 110,
                marginLeft: 15,
                marginRight: 1,
                shadowColor: "#000",
                shadowOffset: {
                width: 0,
                height: 1,
                },
                shadowOpacity: 1.69,
                shadowRadius: 5.65,
                elevation: 3,
                marginBottom:5,
              }}
            >
              <View style={{ marginTop: 15, marginLeft: 20, width: 200 }}>
                <Text style={{ fontFamily: "Gotham-Bold", color: primary800,fontSize:25 }}>
                  {item.name}
                </Text>
                <View style={{flexDirection: 'row'}} >
                    <Text style={styles.text}>{item.description}</Text>
                    <FontAwesomeIcon size={20} icon={faChevronRight} color={secondary} style={{marginLeft:20,marginTop:10}}/>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        );
      }
      /////////item de carrusel de mensualidades
      _renderItem2 ({ item, index }) {
        return (
          <TouchableOpacity>
            <View
              style={{
                backgroundColor: surface,
                borderRadius: 5,
                marginLeft: 0,
                marginRight: 1,
                shadowColor: "#000",
                shadowOffset: {
                width: 0,
                height: 1,
                },
                shadowOpacity: 0.49,
                shadowRadius: 4.65,
                elevation: 3,
                marginBottom:10,
              }}
            >
              <View style={{ marginTop: 10, marginLeft: 30, width: 220 }}>
                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:25 }}>
                  {item.name}
                </Text>
                <View style={{marginBottom:10}} >
                    <Text style={styles.text}>{item.description}</Text>
                </View>
              </View>
              <View style={{backgroundColor:colorGrayOpacity,paddingLeft:30,paddingBottom:10,paddingTop:10}}>
                <Text style={{ fontFamily: "Gotham-Bold", color: primary800,fontSize:16 }}>
                    Nombre Plan Destacado
                </Text>
                <Text style={styles.text}>$0.00.00 Cop % Descuento</Text>
              </View>
              <View style={{flexDirection: 'row',marginBottom:10,marginLeft: 30,}}>
                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:13,marginTop:10 }}>
                  CÓNOCE MÁS PLANES
                </Text>
                <FontAwesomeIcon size={20} icon={faChevronRight} color={secondary} style={{marginLeft:20,marginTop:10}}/>
              </View>
            </View>
          </TouchableOpacity>
        );
      }
      //////item de carrusel de recuerda
      _renderItem3 ({ item, index }) {
        return (
          <TouchableOpacity>
            <View
              style={{
                backgroundColor: surface,
                borderRadius: 5,
                height: 70,
                marginLeft: 15,
                marginRight: 1,
                shadowColor: "#000",
                shadowOffset: {
                width: 0,
                height: 1,
                },
                shadowOpacity: 0.69,
                shadowRadius: 4.65,
                elevation: 3,
                marginBottom:5,
              }}
            >
              <View style={{ marginTop: 15, marginLeft: 20, width: 180 }}>
                <View style={{flexDirection: 'row'}} >
                    <FontAwesomeIcon size={20} icon={faExclamationCircle} color={secondary} style={{marginLeft:5,marginTop:10,marginRight:15}}/>
                    <Text style={styles.text}>{item.description}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        );
      }
///Método para buscar parqueadero//////
    search () {
        const app = this.state;
        if(app.textSearch !== ''){
            this.getParkingList();
            
        }else{
            Alert.alert("","Debe escribir un criterio de busqueda");
        }

    }  

////Contenido de tarjetas de parqueaderos////
renderContentParking = () => (
    <ScrollView >
        <View
        style={{
            backgroundColor: 'transparent',
            paddingLeft: 16,
            paddingRight: 16,
            paddingBottom: 16,
            borderRadius:0,
        }}
        >
            <ScrollView>
              <SafeAreaView style={{ flex: 1, paddingTop: 20}}>
                  <View
                  style={{
                      flex: 1,
                      flexDirection: "row",
                      justifyContent: "center"
                  }}
                  >
                      <Carousel
                          layout={"default"}
                          ref={ref => this.carousel = ref}
                          data={this.state.carouselItemsParking}
                          sliderWidth={290}
                          itemWidth={width-90}
                          renderItem={this._renderItemParking}
                          onSnapToItem={index => this.setState({ activeIndex: index })}
                      />
                  </View>
              </SafeAreaView>            
            </ScrollView>
        </View>

    </ScrollView>
);

_renderItemParking ({ item, index }) {
    return (
      <TouchableOpacity onPress={() => {this.props.navigation.navigate('listParking',{parkingId:item.id})}}>
        <View
          style={{
            backgroundColor: surface,
            borderRadius: 5,
            height: 140,
            marginLeft: 15,
            marginRight: 1,
            marginBottom:5,
            shadowColor: "#000",
            shadowOffset: {
            width: 0,
            height: 1,
            },
            shadowOpacity: 0.69,
            shadowRadius: 4.65,
            elevation: 3,
          }}
        >
          <View style={{ marginTop: 15, marginLeft: 20, width: 230 }}>
              <View style={{flexDirection: 'row'}}>
                <Text style={{ fontFamily: "Gotham-Bold", color: primary800,fontSize:20 }}>
                {item.name}
                </Text>
                <FontAwesomeIcon size={20} icon={faHeart} color={secondary}  style={{marginLeft:40,marginTop:5,}}/>
              </View>
            <Text style={styles.text}>{item.address}</Text>
            <View style={{flexDirection: 'row'}} >
                <Text style={item.status_id === 1 ?{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}:{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>{item.status} </Text>
                <Text style={styles.text}>{item.schedule}</Text>
            </View>
            <Text style={styles.text}>${item.price} Minuto</Text>
          </View>
          <View  style={{alignItems: "flex-end",justifyContent: "flex-end",marginRight:10}}>
            <TouchableOpacity style={styles.button2}>
                <View style={{flexDirection: 'row'}}>
                    <FontAwesomeIcon size={20} icon={faDirections} color={secondary}  style={{marginRight:5,}}/>
                    <Text style={styles.titleButton2}>INDICACIONES</Text>
                </View>    
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  changeFirst = () => {
    this.setState({car:true,moto:false, bici:false});
  }
  changeSecond = () => {
    this.setState({car:false,moto:true, bici:false});
  }
  changeThird = () => {
    this.setState({car:false,moto:false, bici:true});
  }

  getActiveProducts = () => {
    this.setState({showSnack:true});
    
  }
  
  //Servicio para traer mensaje
  getMessageReserve = () => {
    console.log(global.token);
    let config = {
      headers: { Authorization: global.token }
    }
    this.setState({spinner:true});
    axios.get(`${API_URL}customer/app/reservation/time/message/${productIdReserve}`,config).then(response => {
      this.setState({spinner:false});
      const cod = response.data.ResponseCode;
      if(cod === 0){
        console.log(response.data);
        this.setState({snackMessage:response.data.ResponseMessage});
        this.setState({showSnack:true});
      }else{
        Alert.alert("ERROR","No se pudo traer los mensajes del servicio");
      }
    }).catch(error => {
      this.setState({spinner:false});
      if(error.response.data.ResponseCode !== 1007){
        Alert.alert("",error.response.data.ResponseMessage);
      }
      console.log(error.response);
    })
  }


  async componentDidMount() {
    if(global.register ){
      // this.getActiveProducts();
      this.getMessageReserve();
      (async () => {
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== 'granted') {
          setErrorMsg('Permission to access location was denied');
          return;
        }
        let location = await Location.getLastKnownPositionAsync({accuracy: 6,});
        //console.log(location);
        this.setState({location:location});
      })();
    }
  }

    render(){
        return(
          <KeyboardAvoidingView style={styles.container}>
             <ScrollView  showsVerticalScrollIndicator={false}>
                <Spinner visible={this.state.spinner}  color={primary600} />
                {
                    !this.state.parkingList ?
                    <View style={{alignItems:'center'}}>
                      <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <PaperTextInput style={{width:width-100,backgroundColor:surface,color:colorInput,borderRadius:10}} 
                        right={<PaperTextInput.Icon 
                                name={() => <MaterialIcons name="search" size={30} color={secondary} onPress={() => this.search()} />} />}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Buscar" outlineColor={colorInputBorder} onChangeText={(text) => this.setState({textSearch:text})}/>
                        <TouchableOpacity style={{elevation:3,borderRadius:25,backgroundColor:surface,marginTop:15,marginLeft:15, padding:10,height:45}}
                        onPress={() => this.setState({modalVisible:true})}>
                          <Feather name="sliders" size={24} color={secondary} />
                        </TouchableOpacity>
                      </View>
                      <Modal
                          animationType="slide"
                          transparent={true}
                          visible={this.state.modalVisible}
                      >
                        <ScrollView style={{marginBottom:50}}>
                          <TouchableOpacity style={styles.centeredView}>
                          <TouchableWithoutFeedback style={styles.modalView}>
                          <View >
                              <View >

                                    <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                        <TouchableOpacity onPress={() => this.setState({modalVisible:false})}>
                                          <MaterialIcons name="close" size={25} color={secondary} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{padding:10}}>
                                      <Text style={styles.titleModal}>Productos y servicios</Text>
                                      <View>
                                      {
                                        this.state.products.map((product,index) => {
                                            return  <View style={{flexDirection:'row',marginTop:20}} key={product.id}>
                                                      <View>
                                                        <Text style={styles.textModal}>{product.name}</Text>
                                                      </View>
                                                      <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                                        <Switch
                                                          trackColor={{ false: surfaseDisabled, true: primary300 }}
                                                          thumbColor={product.select ? primary500 : surfaceMediumEmphasis}
                                                          ios_backgroundColor="#3e3e3e"
                                                          onChange={() => this.toggleSwitch(product.id,index)}
                                                          value={product.select}
                                                        />
                                                      </View>
                                                    </View>
                                        })
                                        }
                                        
                                      </View>
                                    </View>
                                    <View style={{padding:10}}>
                                      <Text style={styles.titleModal}>Tipo de vehículo</Text>
                                      <View style={{flexDirection: 'row',marginBottom:10,overflow:'scroll',marginTop:20}}>
                                        <Chip textStyle={{fontSize:12}} style={this.state.car ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor:onSurfaceOverlay8, marginRight:6,}}  selected={this.state.car} selectedColor={secondary} mode='flat' onPress={this.changeFirst}>
                                            <Ionicons name="car-sport-outline" size={18} color={surfaceHighEmphasis} />
                                            <Text style={{fontFamily: 'Gotham-Light',color:surfaceHighEmphasis,fontSize:14,paddingLeft:10}}>  Automóvil</Text>
                                        </Chip>
                                        <Chip textStyle={{fontSize:12}} style={this.state.moto ? {backgroundColor:colorPrimarySelect,marginRight:6}:{backgroundColor:onSurfaceOverlay8, marginRight:6}} selected={this.state.moto} selectedColor={secondary} mode='flat' onPress={this.changeSecond}>
                                          <FontAwesome name="motorcycle" size={14} color={secondary} />
                                          <Text style={{fontFamily: 'Gotham-Light',color:surfaceHighEmphasis,fontSize:14,marginLeft:10}}>  MotoCicleta</Text>
                                        </Chip>
                                      </View>
                                      <View style={{width:width-265}}>
                                          <Chip textStyle={{fontSize:12}} style={this.state.bici ? {backgroundColor:colorPrimarySelect,marginRight:6}:{backgroundColor:onSurfaceOverlay8, marginRight:6}} selected={this.state.bici} selectedColor={secondary} mode='flat' onPress={this.changeThird}>
                                            <MaterialIcons name="pedal-bike" size={16} color={secondary} />
                                            <Text style={{fontFamily: 'Gotham-Light',color:surfaceHighEmphasis,fontSize:14,marginLeft:10}}>  Bicibleta</Text>
                                          </Chip>  
                                      </View>
                                    </View>  
                                    <View style={{padding:10}}>
                                      <Text style={styles.titleModal}>Horario</Text>
                                    
                                    </View>
                                    <View style={{padding:10}}>
                                      <Text style={styles.titleModal}>Lugares cercanos</Text>
                                        
                                    </View>
                              </View>
                          </View>
                          </TouchableWithoutFeedback>
                          </TouchableOpacity>
                        </ScrollView>
                          <View style={styles.footerModal}>
                          <View style={{flexDirection:'row',justifyContent: 'space-between'}}>
                              <TouchableOpacity style={{marginRight:30}} onPress={() => this.setState({modalVisible:false})}>
                                <Text style={{height:50,paddingTop:16,fontFamily:'Gotham-Medium',fontSize:15,color:secondary,alignContent:'center',justifyContent:'center'}}>CANCELAR</Text>
                              </TouchableOpacity>
                              <TouchableOpacity style={styles.buttonModal}>
                                  <Text style={styles.titleButton1}>!</Text>
                              </TouchableOpacity>
                          </View>
                          </View>
                      </Modal>
                      <View style = {styles.lineStyle} />
                    </View>
                    :
                    <View style={{marginTop:20,marginBottom:30,alignItems:'flex-start',justifyContent:'flex-start',alignContent:'flex-start'}}>
                      <TouchableOpacity onPress={() => this.listParkingBack()}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                      </TouchableOpacity>
                    </View>
                  }
                <View style={{alignItems:'center'}}>
                    {
                      this.state.location ?
                      <MapView 
                        style={{width:width-20,height:height-230,marginBottom:60}}                 
                        initialRegion = { { 
                        latitude : this.state.location.coords.latitude , 
                        longitude : this.state.location.coords.longitude , 
                        latitudeDelta : 0.02, 
                        longitudeDelta : 0.03, 
                        } } 
                        showsUserLocation={true}
                        followsUserLocation={true}
                      />
                      :
                      <Image source={require("../../assets/home/Map.png")} style={{width:width-20,height:height-230,marginBottom:60}}></Image>
                    }
                    { this.state.bottomSheetVisible && 
                        <BottomSheet
                            enabledContentTapInteraction={false}
                            ref={this.sheetRef}
                            snapPoints={[200, height-130, 80]}
                            borderRadius={0}
                            renderHeader={this.renderHeader}
                            renderContent={this.renderContent}
                            enabledGestureInteraction={true}
                            enabledContentGestureInteraction={false}
                            enabledInnerScrolling={true}
                        />
                    }
                    { this.state.parkingCarrousel && 
                        <BottomSheet
                            enabledContentTapInteraction={false}
                            ref={this.sheetRef}
                            snapPoints={[170, height-50, 100]}
                            borderRadius={0}
                            renderContent={this.renderContentParking}
                            enabledGestureInteraction={true}
                            enabledContentGestureInteraction={false}
                            enabledInnerScrolling={true}
                        />
                    }
                    { this.state.parkingList && 
                        <BottomSheet
                            enabledContentTapInteraction={false}
                            ref={this.sheetRef}
                            snapPoints={[height-180, height-180, 80]}
                            borderRadius={0}
                            renderHeader={this.renderHeader}
                            renderContent={this.renderContentListParking}
                            enabledGestureInteraction={true}
                            enabledContentGestureInteraction={false}
                            enabledInnerScrolling={true}
                        />
                    }

                </View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.dialogVisible}
                >
                  <TouchableWithoutFeedback onPress={() => this.setState({dialogVisible:false})}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <View style={{alignItems:'center',alignContent:'center'}}>
                              <Text style={styles.title2}>Registrate y accede a más</Text>
                              <Text style={styles.text2}>Registrate y accede a más.....</Text>
                              <View style={{marginBottom:10}}>
                                  <TouchableOpacity style={styles.button1} onPress={() => this.props.navigation.navigate('Register')}>
                                      <Text style={styles.titleButton1}>CREAR CUENTA</Text>
                                  </TouchableOpacity>
                              </View>
                            </View>
                        </View>
                    </View>
                  </TouchableWithoutFeedback>
                </Modal>
                <Snackbar
                  style={{padding:10}}
                  visible={this.state.showSnack}
                  onDismiss={() => this.setState({showSnack:false})}
                  >
                  {this.state.snackMessage}
                </Snackbar>
              </ScrollView>
            </KeyboardAvoidingView>
        );
    }

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'white'
    },
    title: {
        color: secondary,
        fontSize: 25,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 20,
        fontFamily:'Gotham-Bold',
    },
    titleModal: {
      color: secondary,
      fontSize: 18,
      fontFamily:'Gotham-Bold',
  },
    input:{
        height:50,  
        color: colorInput, 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none',
        flexDirection:'row'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        height:50,
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop:10,
        marginBottom:30,
      },
      buttonModal: {
        height:50,
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-220,
        alignItems: "center",
        alignContent:'flex-end',
        justifyContent:'flex-end',
        marginLeft:'auto',
      },
      button4: {
        height:50,
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop:10,
        marginBottom:30,
      },
      button3: {
        height:50, 
        backgroundColor: surface,
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      lineStyle:{
        marginTop:5,
        marginBottom:15,
        backgroundColor: colorGray,
        height: 2,
        width: width-40,
   },
   text: {
    color: '#000',
    fontSize: 13,
    fontFamily: 'Gotham-Light',
  },
  textModal: {
    color: '#000',
    fontSize: 16,
    fontFamily: 'Gotham-Light',
  },
text2: {
    color: '#000',
    fontSize: 13,
    fontFamily: 'Gotham-Light',
    marginBottom:30,
},
button2: {
    height:28, 
    backgroundColor: surface,
    padding: 4,
    borderRadius: 10,
    width: 140,
    alignItems: "center",
    marginTop: 10,
    marginBottom: 10,
    borderColor: secondary,
    borderWidth: 1.2,
  },
  titleButton2: {
    color: secondary,
    fontFamily:'Gotham-Bold',
    fontSize: 13,
  },
  centeredView: {
    flex: 1,
    backgroundColor:"white",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowColor: "#000",
    elevation: 5,
    marginTop:height-380,
    padding: 20,
  },
  modalView: {
  },
  footerModal:{
    width:width,
    padding:10,
    alignItems: 'center',
    position:'absolute',
    bottom:0,
    backgroundColor:surface ,
    shadowColor: "#000",
    elevation: 5,
  },
  })