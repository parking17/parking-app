import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions,Modal, Image,TouchableWithoutFeedback} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { TextInput as PaperTextInput, HelperText} from 'react-native-paper';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import Checkbox from 'expo-checkbox';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight,faTrash} from '@fortawesome/free-solid-svg-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default function DitailsCourtesy ({navigation}) {
    
    const [spinner, setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [token, setToken] = useState(null);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
      };

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/20`).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getParkingInfo();
          getToken();
        });
        return unsubscribe;

      }, [navigation]);

    return (
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,paddingBottom:10,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('Courtesy')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                <Text style={{fontSize:24, fontFamily:'Gotham-Bold', color:'#005A6D'}}>Cortesia Lorem Ipsum</Text>
                <Text style={{fontSize:14, fontFamily:'Gotham-Medium', color:'#005A6D', marginTop:10}}>En gratitud por los servicios que has adquirido con parking hemos agregado a tu cuenta una cortesia
                para que disfrutes junto con un beneficiario
                </Text>
                <Text style={{fontSize:16, fontFamily:'Gotham-Bold', color:'#005A6D', marginTop:20}}>Carecteristícas de la cortesia</Text>
                <View>
                    <View >
                        <View>
                            <Text style={{fontSize:16, fontFamily:'Gotham-Medium', color:'#005A6D', marginTop:20}}> Carro - Moto </Text>
                        </View>
                        <View style={{flexDirection:'row',marginTop:10}}>
                            <Text style={{marginRight:10,marginLeft:10}}>
                                <FontAwesomeIcon icon={faCar} color={secondary} />
                            </Text>
                            <Text style={{marginRight:10}}> + </Text>
                            <Text>
                                <FontAwesomeIcon icon={faMotorcycle} color={secondary} />
                            </Text>   
                        </View>
                    </View>
                </View>
                <Text style={{fontSize:16,color:'rgba(0, 90, 109, 0.8)',fontFamily:'Gotham-Bold',marginTop:20}}>Horario</Text>
                <Text style={{fontSize:14,color:'rgba(0, 45, 51, 0.6)',fontFamily:'Gotham-Light',marginTop:5}}>8:00 - 22:00</Text>
                <View>
                    <Text style={{fontSize:16,color:'rgba(0, 90, 109, 0.8)',fontFamily:'Gotham-Medium',marginTop:20}}>Vigencia</Text>
                    <Text style={{fontSize:14,color:'rgba(0, 45, 51, 0.6)',fontFamily:'Gotham-Medium',marginTop:5}}>2 Meses</Text>
                </View>
                <Text style={{fontSize:16,color:'rgba(0, 90, 109, 0.8)',fontFamily:'Gotham-Bold',marginTop:20}}>Parqueaderos asociados</Text>
                                <TouchableOpacity style={{justifyContent:'center'}}>
                    <View
                    style={{
                        backgroundColor: surface,
                        borderRadius: 10,
                        width:width-38,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        padding:15,
                    }}
                    >
                        <View style={{flexDirection: 'row'}}>
                            <View >
                                <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                    {
                                        parking !== null ?
                                        parking.name
                                        :
                                        ""
                                    }
                                </Text>
                                {
                                    parking !== null ?
                                        parking.address !== null ? 
                                            <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    :
                                    <Text style={styles.text}>No hay dirección disponible</Text>
                                }
                                <View style={{flexDirection: 'row'}} >
                                    {
                                        parking !== null &&
                                            parking.open == 1 ? 
                                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                            :
                                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                    }    
                                        
                                    {
                                        parking !== null ?
                                            parking.finalHour !== "" && parking.initialHour !== "" ? 
                                                <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay horario disponible</Text>
                                    }
                                </View>
                            </View>
                            <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto',marginTop:20}}>
                                <MaterialIcons name="assistant-direction" size={24} color={secondary} />    
                            </View>
                        </View>
                    </View> 
                </TouchableOpacity>
                <TouchableOpacity style={{marginTop:80}}>
                    <Text style={{fontSize:14,color:'rgba(0, 45, 51, 0.4)',textAlign:'center',fontFamily:'Gotham-Bold'}}>CERRAR</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-300,
      marginTop:0,
    },
})