import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions,Modal, Image} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface,primary600, surfaceMediumEmphasis,
    primary800,OnSurfaceOverlay15,OnSurfaceDisabled,primary500, surfaceHighEmphasis, onSurfaceOverlay8,primary900,
    secondary600,secondary500,colorGray, surfaseDisabled,secondary50,errorColor } from '../../utils/colorVar';
import Carousel from "react-native-snap-carousel";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import Checkbox from 'expo-checkbox';
import { textAlign } from 'styled-system';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
export default function ListCourtesy({navigation}) {
    const [spinner, setSpinner] = useState(false);
    const [modalTermsVisible, setModalTermsVisible] = useState(false);
    const [token, setToken] = useState(null);
    const [code, setCode] = useState(null);
    const [codeInvalid, setCodeInvalid] = useState(false);
    const [isSelected, setSelection] = useState(false);
    const [vehicles, setVehicles] =  useState(null);
    const [valid, setValid] = useState(false);
    const [courtesies, setCourtesies] = useState(
        [
            {
                id:21,
                name:'Nombre de cortesía',
                description:'Reserva un lugar para parquear el día, la hora y el tiempo que desees.',
                date: '19/06/2022'
            },
/*             {
                id:22,
                name:'Pasadía',
                description:'Reserva un lugar para parquear el día, la hora y el tiempo que desees.',
                date: '19/06/2022'
            } */
        ],
    );
    const [courtesiesCompleted,setCourtesiesCompleted] = useState(
        [
  /*           {
                id:21,
                name:'Nombre de cortesía',
                description:'Reserva un lugar para parquear el día, la hora y el tiempo que desees.',
                date: '19/06/2022'
            },
            {
                id:22,
                name:'Nombre de cortesía',
                description:'Reserva un lugar para parquear el día, la hora y el tiempo que desees.',
                date: '19/06/2022'
            },
            {
                id:23,
                name:'Nombre de cotesía',
                description:'Reserva un lugar para parquear el día, la hora y el tiempo que desees.',
                date: '19/06/2022'
            } */
        ],
    );
    const [activeIndex, setActiveIndex] = useState();

    const getToken =  async() => {
        try {
            const value =  await AsyncStorage.getItem('token');
            if (value !== null) {
                setToken(value);
            }
          } catch (error) {
            console.log(error);
          }
    }
    const getDataStorage = async () => {
        try {
          const value1 = await AsyncStorage.getItem("vehicle");
          if (value1 !== null) { 
            const value = JSON.parse(value1);
            setVehicles(value);
            AsyncStorage.removeItem("vehicle");
          }

        } catch (error) {
            console.log(error);
         }
      };
    /////////item de carrusel de cortesias///
    const _renderItem = ({ item }) =>{
        return (
            <View
              style={{
                backgroundColor: surface,
                borderRadius: 5,
                height:width-10,
                marginLeft: 15,
                marginRight: 1,
                shadowColor: "#000",
                shadowOffset: {
                width: 0,
                height: 1,
                },
                shadowOpacity: 1.69,
                shadowRadius: 5.65,
                elevation: 3,
                marginBottom:5,
                padding:15,
              }}
            >
              <View style={{alignItems:'flex-end',marginLeft:'auto'}}>
                    <MaterialIcons name="share" size={20} color={secondary} />
                </View>
              <View style={{ alignItems:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Image source={require('../../../assets/account/Vector.png')} width={70} height={70}/>
                    <Text style={{ fontFamily: "Gotham-Bold", color: primary800,fontSize:18, textAlign:'center', marginTop:20}}>
                    {item.name}
                    </Text>
                    <Text style={{ fontFamily: "Gotham-Light", color: surfaceMediumEmphasis,fontSize:10 , textAlign:'center'}}>
                    VENCE {item.date}
                    </Text>
                    <Text style={{fontFamily: "Gotham-Light", color: surfaceHighEmphasis,fontSize:14, textAlign:'center',marginTop:20}}>{item.description}</Text>
                    <View style={{marginTop:30}}>
                        <View style={{alignItems:'center',alignContent:'center'}}>
                            <TouchableOpacity style={styles.button1}>
                                    <Text style={styles.titleButton1}>USAR CORTESÍA</Text>
                            </TouchableOpacity> 
                        </View>
                        <View style={{flex:3, marginTop:10}}>
                            <TouchableOpacity >
                                <Text style={{color: surfaceMediumEmphasis,fontSize: 15,fontFamily:'Gotham-Medium',textAlign:'center'}}>PARQUEADEROS</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
              </View>
            </View>
        );
      }
    const codValid = () => {
        if(code === null || code === ""){
            Alert.alert("","El codigo es obligatorio");
        }else {
            if(code.length !== 6){
            Alert.alert("","El codigo debe ser de 6 digitos");
            }else{
                setValid(true);
            }
        }
    }
    const terms = () => {
        setModalTermsVisible(true);
    }
    //Método para eliminar vehiculos
    function deleteVehicle () {
        const newList = vehicles.filter((item) => item.id !== id);
        setVehicles(newList);
        if(newList.length === 0){
            setVehicles(null);
        }
    }
    //Metodo para traer las corterias
    
    // const getLisCourtesy = () => {
    //     console.log("ENTRAA");
    //     let config = {
    //         headers: { Authorization: global.token }
    //         };
    //     setSpinner(true);
    //     axios.get(`${API_URL}customer/app/courtesy/user/get`,config).then(response => {
    //         setSpinner(false);
    //         const cod = response.data.ResponseCode;
    //         if(cod === 0){
    //             console.log(response.data);
    //         }else{
    //             Alert.alert("ERROR","No se pudo traer las cortesias");
    //         }
    //         }).catch(error => {
    //         setSpinner(false);
    //         Alert.alert("",error.response.data.ResponseMessage);
    //         console.log(error.response.data.ResponseMessage);
    //         })
        
    // }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
          getDataStorage();
        //   getLisCourtesy();
        });
        return unsubscribe;
      }, [navigation]);
    return (
        <View>
            <ScrollView  showsVerticalScrollIndicator={true}>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,paddingBottom:10,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.goBack(null)}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <View style={styles.container}>
                    <Text style={{fontFamily:'Gotham-Bold',color: secondary,fontSize: 24,marginRight:10}}>
                        Cortesías
                    </Text> 
                    <Text style={{fontFamily:'Gotham-Light',color: secondary,fontSize: 14,marginRight:10}}>
                        Conoce los beneficios que hemos preparado para ti. Vitae ac velit pellentesque consequat tristique nulla at. Ac quis est facilisis nullam pharetra.
                    </Text> 
                </View>
                <View style={{flexDirection:'row',paddingHorizontal:20,paddingVertical:10}}>
                    <Text style={{fontFamily:'Gotham-Bold',color: secondary,fontSize: 20,marginRight:10}}>
                        Vigentes
                    </Text> 
                </View>
                <View style={{backgroundColor:surface,paddingHorizontal:20,paddingVertical:10,}}>
                    {
                        courtesies.length !== 0 &&
                            courtesies.map((courtesy) => {
                                return <TouchableOpacity key={courtesy.id} style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 15, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:10, marginTop:10}}>
                                            <View >
                                                <Text style={{color: secondary,fontSize: 18,fontFamily: 'Gotham-Bold',}}>{courtesy.name}</Text>
                                                <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>{courtesy.description}</Text>
                                                <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily: 'Gotham-Light',textDecorationLine:'underline',marginTop:10}}>Vence: {courtesy.date}</Text>
                                            </View>
                                        </TouchableOpacity>
                            })
                    }
                    
                </View>
{/*                 <View style={{flexDirection:'row',paddingHorizontal:20,paddingVertical:10}}>
                    <Text style={{fontFamily:'Gotham-Bold',color: secondary,fontSize: 20,marginRight:10}}>
                        Historico
                    </Text> 
                </View> */}
                <View style={{backgroundColor:surface,paddingHorizontal:20,paddingVertical:10,}}>
                    {
                        courtesiesCompleted.length !== 0 &&
                            courtesiesCompleted.map((courtesy) => {
                                return <TouchableOpacity key={courtesy.id} style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 15, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:10, marginTop:10}}>
                                            <View >
                                                <Text style={{color: secondary,fontSize: 18,fontFamily: 'Gotham-Bold',}}>{courtesy.name}</Text>
                                                <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>{courtesy.description}</Text>
                                                <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily: 'Gotham-Light',textDecorationLine:'underline',marginTop:10}}>Venció: {courtesy.date}</Text>
                                            </View>
                                        </TouchableOpacity>
                            })
                    }
                    
                </View>
                {/* <View style={{backgroundColor:surface,paddingHorizontal:20,paddingVertical:20,}}>
                    <View
                      style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "center",
                          marginTop:10,
                      }}
                      >
                          <Carousel
                              layout={"default"}
                              data={courtesies}
                              sliderWidth={width-120}
                              itemWidth={width-80}
                              renderItem={_renderItem}
                              onSnapToItem={(index) => setActiveIndex(index)}
                          />
                      </View>
                </View> */}
                <View style={{backgroundColor:surface,paddingHorizontal:20,paddingVertical:20,}}>
                        <View style={{marginTop:10}}>
                            <TouchableOpacity onPress={() => navigation.navigate('Courtesy')}>
                                <Text style={{color: surfaceMediumEmphasis,fontSize: 15,fontFamily:'Gotham-Medium',textAlign:'center',marginTop:200}}>¿TIENES UN CÓDIGO?</Text>
                            </TouchableOpacity>
                        </View>
                </View>
            </ScrollView>
        </View>
    );

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:surface,
        paddingHorizontal:20,
        paddingVertical:10,
    },
    form: {
        alignItems: 'flex-start',
        marginTop: 120,
    },
    title: {
        color: secondary,
        fontSize: 22,
        fontFamily: 'Gotham-Bold',
    },
    text: {
        color: secondary,
        fontSize: 15,
        fontFamily: 'Gotham-Light',
    },
    input: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 320,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    inputPassword:{
        height:50,  
        color: colorInput, 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none',
        flexDirection:'row',
      },
    inputPhone: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 180,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 14,
        fontFamily: 'Gotham-Bold',
        textAlign:'center',
    },
    button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-120,
        alignItems: "center",
    },
    pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 56,
        marginVertical:20,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
    },
    modalView2: {
        elevation:10,
    },
    centeredView2: {
        flex: 1,
        backgroundColor:"white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: "#000",
        elevation: 15,
        marginTop:height-210,
        padding: 20,
      },
      container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
})
