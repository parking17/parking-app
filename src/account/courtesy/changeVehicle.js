import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,CheckBox,Dimensions, TextInput} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {productIdReserve} from "../../utils/varService";
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ChangeVehicle({ route,navigation }) {
    const {number} = route.params;
    const [vehicles, setVehicles] = useState([
        {
            id:1,
            vehicle_type_id:1,
            name:'Automóvil',
            plate:'AAA 290'
        },
        {
            id:2,
            vehicle_type_id:2,
            name:'Moto',
            plate:'OII 27F'
        },
        {
            id:3,
            vehicle_type_id:3,
            name:'Bici',
            plate:'ASDV24F'
        }
    ]);
    const [spinner,setSpinner] = useState(false);
    const selectVehicle = (vehicleTypeId,licensePlate) => {
        save(vehicleTypeId,licensePlate);
        navigation.navigate('ActiveCourtesy');
    }
    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            getVehicles(value);
          }
        } catch (error) { }
      };
    const save = async (vehicleTypeId,licensePlate) => {
        if(number === 1){
            try {
    
              await AsyncStorage.setItem("vehicleTypeId1", vehicleTypeId.toString());
              await AsyncStorage.setItem("licensePlate1", licensePlate);
            } catch (e) {
              // saving error
              console.log('Fallo en guardar storage addVehicle');
            }

        }else{
            try {
    
                await AsyncStorage.setItem("vehicleTypeId2", vehicleTypeId.toString());
                await AsyncStorage.setItem("licensePlate2", licensePlate);
              } catch (e) {
                // saving error
                console.log('Fallo en guardar storage addVehicle');
              }
        }
      };

    ///////////verificar si tiene vehiculos registrados
    const getVehicles = (token) => {
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        axios.get(`${API_URL}customer/app/list/vehicle`,config).then(response => {
            const cod = response.data.ResponseCode;
            setSpinner(false);
            if(cod === 0){
                console.log("ENTRA");
                setVehicles(response.data.ResponseMessage.vehicles);
            }else{
              Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
        });
        return unsubscribe;
    }, [navigation]);
    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('ActiveCourtesy')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
                <Text style={{fontFamily:'Gotham-Bold',color:secondary,marginLeft:10,fontSize:16}}>Cotesía</Text>
            </View>
            <View style={styles.container}>
                <Text style={styles.title2}>Vehículo</Text>
                <Text style={styles.text2}>Selecciona el vehículo...................</Text>
                <View >
                    {
                        vehicles.map( (vehicle) => {
                            return <TouchableOpacity style={{justifyContent:'center'}} key={vehicle.id} onPress={() => selectVehicle(vehicle.vehicle_type_id,vehicle.plate)}>
                                        <View
                                        style={{
                                            backgroundColor: surface,
                                            borderRadius: 5,
                                            width:width-40,
                                            marginLeft: 0,
                                            marginRight: 1,
                                            shadowColor: "#000",
                                            shadowOffset: {
                                            width: 0,
                                            height: 1,
                                            },
                                            shadowOpacity: 0.49,
                                            shadowRadius: 4.65,
                                            elevation: 3,
                                            marginBottom:10,
                                            marginTop:10,
                                            paddingTop:5,
                                        }}
                                        >
                                            <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                                                {
                                                    vehicle.vehicle_type_id === 1 &&
                                                    <FontAwesomeIcon size={20} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                {
                                                    vehicle.vehicle_type_id === 2 &&
                                                    <FontAwesomeIcon size={20} icon={faMotorcycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                {
                                                    vehicle.vehicle_type_id === 3 &&
                                                    <FontAwesomeIcon size={20} icon={faBicycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                <View>
                                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                                        {
                                                            vehicle.vehicle_type_id === 1 &&
                                                            "Automóvil"
                                                        }
                                                        {
                                                            vehicle.vehicle_type_id === 2 &&
                                                            "Motocicleta"
                                                        }
                                                        {
                                                            vehicle.vehicle_type_id === 3 &&
                                                            "Bicicleta"
                                                        }
                                                                
                                                    </Text>
                                                    <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                                        {vehicle.plate}
                                                    </Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                            })
                    }
                    
                </View>
                
            </View>
            <View style={{backgroundColor:surface,paddingBottom:20,paddingTop: 120}}>
                <TouchableOpacity onPress={() => navigation.navigate('AddVehicleCourtesy',{typeCourtesy:2})} >
                    <Text style={{ fontSize: 15,fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center", color:primary800}}>
                        REGISTRAR NUEVO VEHÍCULO
                    </Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingLeft:20,
      paddingBottom:20,
      paddingTop:30,
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:30,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
  })