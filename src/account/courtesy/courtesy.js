import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions,Modal, Image,TouchableWithoutFeedback} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface,primary600, surfaceMediumEmphasis,
    primary800,OnSurfaceOverlay15,OnSurfaceDisabled,primary500, surfaceHighEmphasis, onSurfaceOverlay8,primary900,
    secondary600,secondary500,colorGray, surfaseDisabled,secondary50,errorColor } from '../../utils/colorVar';
import { TextInput as PaperTextInput, HelperText} from 'react-native-paper';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import Checkbox from 'expo-checkbox';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
export default function Courtesy({navigation}) {
    const [spinner, setSpinner] = useState(false);
    const [modalTermsVisible, setModalTermsVisible] = useState(false);
    const [token, setToken] = useState(null);
    const [progress, setProgress] = useState(0.5);
    const [blue, setBlue] = useState(true);
    const [code, setCode] = useState(null);
    const [codeInvalid, setCodeInvalid] = useState(false);
    const [isSelected, setSelection] = useState(false);
    const [vehicles, setVehicles] =  useState([]);
    const [valid, setValid] = useState(false);
    const [modalFormShare, setModalFormShare] = useState(false);
    const [emailShare, setEmailShare] = useState(null);
    const [phoneShare, setPhoneShare] = useState(null);
    const [numberDocumentShare, setNumberDocumentShare] = useState(null);
    const [vehiclesShare, setVehiclesShare] =  useState([]);
    const [mode, setMode] = useState(false);

    const getToken =  async() => {
        try {
            const value =  await AsyncStorage.getItem('token');
            if (value !== null) {
                setToken(value);
            }
          } catch (error) {
            console.log(error);
          }
    }
    const getDataStorage = async () => {
        try {
          const value1 = await AsyncStorage.getItem("vehicle");
          if (value1 !== null) { 
            const value = JSON.parse(value1);
            setVehicles(value);
            AsyncStorage.removeItem("vehicle");
          }

        } catch (error) {
            console.log(error);
         }
      };

    const continueButton = () => {
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        axios.get(`${API_URL}customer/app/list/vehicle`,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                if(response.data.ResponseMessage.vehicles.length === 0){
                    navigation.navigate('AddVehicleBeParking');
                }else{
                   console.log('');
                }
            }else{
                Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
            }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
            })

    }
    const codValid = () => {

        if(code === null || code === ""){
            Alert.alert("","El codigo es obligatorio");
        }else {
            if(code.length !== 6){
            Alert.alert("","El codigo debe ser de 6 digitos");
            }else if (code === '123456'){              
                Alert.alert("", "Código válido");    
            }else if (code === '123457'){              
                Alert.alert("", "Código ya redimido");    
            }
            
         /*    {
                let config = {
                    headers: { Authorization: global.token }
                    };
                setSpinner(true);
                axios.get(`${API_URL}customer/app/courtesy/validate/code/${code}`,config).then(response => {
                    setSpinner(false);
                    const cod = response.data.ResponseCode;
                    if(cod === 0){
                        console.log(response.data);
                        Alert.alert("",response.data.ResponseMessage);
                        setValid(true);
                    }else{
                        Alert.alert("ERROR","No es código válido");
                    }
                    }).catch(error => {
                    setSpinner(false);
                    Alert.alert("",error.response.data.ResponseMessage);
                    console.log(error.response.data);
                    })
            } */
        }
    }
    const terms = () => {
        setModalTermsVisible(true);
    }
    //Método para eliminar vehiculos
    function deleteVehicle (id) {
        const newList = vehicles.filter((item) => item.id !== id);
        setVehicles(newList);
        if(newList.length === 0){
            setVehicles([]);
        }
    }
    //Método para eliminar vehiculos compartidos
    function deleteVehicleShare (document) {
        console.log(document);
        const newList = vehiclesShare.filter((item) => item.document !== document);
        console.log(newList);
        setVehiclesShare(newList);
        if(newList.length === 0){
            setVehiclesShare([]);
        }
    }
    const active = () => {
        console.log("ENTRAA");
        navigation.navigate('ActiveCourtesy');
    }
    //Metodo para abrir compartir cortesia
    const openShare = () => {
        if(vehiclesShare.length !== 2){
            setModalFormShare(true);
        }
    }
    //Metodo para añadir vehiculo
    const addVehicleShare = () => {
        if(vehiclesShare.length === 0){
            setVehiclesShare(vehiclesShare.concat({document:numberDocumentShare,typeId:vehicleTypeId,license:licensePlate}));
        }
        if(vehiclesShare.length === 1){
            setVehiclesShare(vehiclesShare.concat({document:numberDocumentShare,typeId:vehicleTypeId,license:licensePlate}));
        }
        setModalFormShare(false);
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
          getDataStorage();
        });
        return unsubscribe;
      }, [navigation]);
    return (
        <View>
            <ScrollView style={{backgroundColor:surface,}} showsVerticalScrollIndicator={true}>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,paddingBottom:10,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.goBack(null)}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <View style={styles.container}>
                    <Text style={{fontFamily:'Gotham-Bold',color: secondary,fontSize: 24,marginRight:10}}>
                        Cortesías
                    </Text> 
                    <Text style={{fontFamily:'Gotham-Light',color: secondary,fontSize: 15,marginRight:10}}>
                        Conoce los beneficios que hemos preparado para ti. Vitae ac velit pellentesque consequat tristique nulla at. Ac quis est facilisis nullam pharetra.
                    </Text> 
                </View>
                <View style={{backgroundColor:surface,paddingHorizontal:20,paddingVertical:20,marginBottom:height-480}}>
                    <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Bold',}}>Ingresa el código enviado a tu celular</Text>
                    <View >
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,fontSize:14,marginTop:10}} 
                            onChangeText={(text) => {setCode(text)}} 
                            keyboardType='numeric' maxLength={6}
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Código" outlineColor={codeInvalid ? errorColor : colorInputBorder}/>
                        <HelperText type="error" visible={codeInvalid}>
                            Código inválido
                        </HelperText>
                        <TouchableOpacity style={{marginBottom:20,alignContent:'center'}} onPress={codValid}>
                            <Text style={{color: primary900,fontSize: 14,fontFamily:'Gotham-Medium',marginLeft:3,textAlign:'center'}}><MaterialIcons name="check" size={20} color={primary900} /> VALIDAR CÓDIGO</Text>
                        </TouchableOpacity>
                    </View>

                    <View>
                    {
                        code === '123456' ?

                        
                        <TouchableOpacity style={{marginBottom:10}} onPress={() => navigation.navigate('ListVehicleCourtesy')}>
                            <View style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 15, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:10, marginTop:10}}>
                                <View style={{flexDirection: 'row',marginBottom:10}}>
                                    <MaterialCommunityIcons name="car" size={30} color={secondary} style={{textAlign:'left',justifyContent:'flex-start', marginHorizontal:'auto',marginTop:5}}/>    
                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 ,textAlign:'center',justifyContent:'center', marginLeft:'auto'}}>
                                        Vehículos
                                    </Text>
                                    <Text style={{ fontFamily: "Gotham-Medium", color: surfaceMediumEmphasis,fontSize:14,marginTop:10,marginLeft:60,justifyContent:'flex-end', marginLeft:'auto' }}>
                                        SELECCIONAR
                                    </Text>
                                
                                </View>
                                {
                                    vehicles.length != 0 &&
                                    <View style={{flexDirection:'row',justifyContent:'center', alignItems:'center'}}>
                                    {
                                    vehicles.map((vehicle,index) => {
                                        return vehicle.id &&   
                                                <View style={{backgroundColor:onSurfaceOverlay8,borderRadius:20,flexDirection:'row',padding:10,marginRight:5}} key={vehicle.id}> 
                                                    <Text style={{marginRight:10}}>{vehicle.license}</Text>
                                                    <TouchableOpacity onPress={() => deleteVehicle(vehicle.id)}>
                                                        <MaterialCommunityIcons name="close-circle" size={20} color={surfaceMediumEmphasis} />
                                                    </TouchableOpacity>
                                                </View>
                                    })
                                    }
                                    </View>
                                }
                            </View>
                        <TouchableOpacity style={{marginBottom:20,alignContent:'center'}} onPress={() => navigation.navigate('DitailsCourtesy')}>
                                    <Text style={{color: primary900,fontSize: 14,fontFamily:'Gotham-Medium',marginLeft:3}}><MaterialIcons name="check" size={20} color={primary900} /> VER DETALLES DE LA CORTESIA</Text>
                        </TouchableOpacity>
                        </TouchableOpacity>
                        :
                        <View></View>
                    }
                    </View>

{/*                     <TouchableOpacity style={{marginBottom:10}} onPress={() => openShare()}>
                            <View style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 15, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:10, marginTop:10}}>
                                <View style={{flexDirection: 'row',marginBottom:10,}}>
                                    <MaterialCommunityIcons name="share" size={30} color={vehiclesShare.length == 2 ? surfaseDisabled :secondary} style={{textAlign:'left',justifyContent:'flex-start', marginHorizontal:'auto',marginTop:5}}/>    
                                    <Text style={{ fontFamily: "Gotham-Bold", color: vehiclesShare.length == 2 ? surfaseDisabled :secondary,fontSize:16,marginTop:10 ,textAlign:'center',justifyContent:'center', marginLeft:'auto'}}>
                                        Compartir
                                    </Text>
                                    <Text style={{ fontFamily: "Gotham-Medium", color: vehiclesShare.length == 2 ? surfaseDisabled :surfaceMediumEmphasis,fontSize:14,marginTop:10,marginLeft:60,justifyContent:'flex-end', marginLeft:'auto' }}>
                                        SELECCIONAR
                                    </Text>
                                </View>
                                {
                                    vehiclesShare.length != 0 &&
                                    <View style={{flexDirection:'row'}}>
                                    {
                                    vehiclesShare.map((vehicle,index) => {
                                        return vehicle.license &&   
                                                <View style={{backgroundColor:onSurfaceOverlay8,borderRadius:20,flexDirection:'row',padding:10,marginRight:5}} key={vehicle.document}> 
                                                    <Text style={{marginRight:10}}>{vehicle.license}</Text>
                                                    <TouchableOpacity onPress={() => deleteVehicleShare(vehicle.document)}>
                                                        <MaterialCommunityIcons name="close-circle" size={20} color={surfaceMediumEmphasis} />
                                                    </TouchableOpacity>
                                                </View>
                                    })
                                    }
                                    </View>
                                }
                            </View>
                    </TouchableOpacity> */}
                </View>
            </ScrollView>
            <View style={styles.container2}>
                <View style={{flexDirection:'row'}}>
                    <Checkbox
                        value={isSelected}
                        onValueChange={setSelection}
                        style={{ alignSelf: "center",marginLeft:10,color:primary800}}
                        color={isSelected ? primary600 : surfaceMediumEmphasis}
                    />
                    <TouchableOpacity onPress={terms}>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Light',marginTop:10,textDecorationLine:'underline'}}>Acepto terminos y condiciones</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalTermsVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setModalTermsVisible(!modalTermsVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Terminos y condiciones</Text>
                                <Text style={styles.text2}>Tortor bibendum pretium lacinia at risus. Suspendisse volutpat, neque felis, dui, sagittis sapien commodo vulputate. Quis eget tortor amet ipsum, morbi mollis semper. Commodo leo imperdiet fermentum lobortis suspendisse. Dictumst eget in sed nibh. Eu volutpat ut vel adipiscing erat gravida maecenas ut vitae. Nunc sodales arcu magna in libero in. Ligula eget integer sed diam elit tristique. </Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setModalTermsVisible(!modalTermsVisible)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>ACEPTAR Y CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                
                    {/* valid && vehicles && isSelected ? */}
                    <View style={{marginVertical:10,alignItems:'center',alignContent:'center'}} >
                        <TouchableOpacity style={styles.button1} onPress={() => navigation.navigate('AddBeneficiary')}>
                                <Text style={styles.titleButton1}>COMPARTIR CORTESÍA</Text>
                        </TouchableOpacity> 
                    </View>
 
                    <View style={{marginVertical:10,alignItems:'center',alignContent:'center'}}>
                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10,width: width-50,alignItems:'center'}}  onPress={() => active()}>
                                <Text style={{color: '#fff',fontSize: 15,fontFamily:'Gotham-Bold',}}>ACTIVAR CORTESÍA</Text>
                        </TouchableOpacity> 
                    </View>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalFormShare}
                >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <TouchableOpacity style={{marginTop:-10,marginLeft:'auto'}} onPress={() => setModalFormShare(!modalFormShare)}>
                                    <MaterialCommunityIcons name="close" size={24} color={secondary} />
                                </TouchableOpacity>
                                <Text style={styles.title}>Compartir Cortesia</Text>
                                <Text style={styles.text2}>Diligencie los datos del beneficiario</Text>
                                <PaperTextInput style={{width:width-35,backgroundColor:surface,color:colorInput,marginVertical:10,fontSize:14,}}  
                                        onChangeText={(text) => setNumberDocumentShare(text)}
                                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                                        mode='outlined' label="N° identificación" outlineColor={colorInputBorder}/>  
                                <PaperTextInput style={{width:width-35,backgroundColor:surface,color:colorInput,marginVertical:10,fontSize:14,}}  
                                        onChangeText={(text) => setPhoneShare(text)} keyboardType={'numeric'}
                                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                                        mode='outlined' label="Número de celular" outlineColor={colorInputBorder}/>
                                        <PaperTextInput style={{width:width-35,backgroundColor:surface,color:colorInput,marginVertical:10,fontSize:14,}}  
                                        onChangeText={(text) => setEmailShare(text)}
                                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                                        mode='outlined' label="Correo electrónico" outlineColor={colorInputBorder}/>
                                <View style={{marginVertical:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={addVehicleShare}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                </Modal>
                
            </View>
        </View>
    );

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:surface,
        paddingHorizontal:20,
    },
    form: {
        alignItems: 'flex-start',
        marginTop: 120,
    },
    title: {
        color: secondary,
        fontSize: 22,
        fontFamily: 'Gotham-Bold',
    },
    text: {
        color: secondary,
        fontSize: 15,
        fontFamily: 'Gotham-Light',
    },
    input: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 320,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    inputPassword:{
        height:50,  
        color: colorInput, 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none',
        flexDirection:'row',
      },
    inputPhone: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 180,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    titleButton1: {
        color: secondary,
        fontSize: 15,
        fontFamily: 'Gotham-Bold',
    },
    button1: {
        height: 50,
        backgroundColor: '#fff',
        padding: 14,
        borderRadius: 10,
        width: width-50,
        alignItems: "center",
        borderWidth:1,
        borderColor:secondary,
        marginTop:10
    },
    pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 56,
        marginVertical:5,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
    },
    modalView2: {
        elevation:10,
    },
    centeredView2: {
        flex: 1,
        backgroundColor:"white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: "#000",
        elevation: 15,
        marginTop:height-210,
        padding: 20,
      },
      container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
})
