import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, Alert} from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import {Picker} from '@react-native-picker/picker';
import Spinner from "react-native-loading-spinner-overlay";
import { API_URL } from '../../../url';
import axios from "axios";
import {productIdReserve,productIdPasadia} from "../../utils/varService"
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import { TextInput as PaperTextInput } from 'react-native-paper';
import {primary600,primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis, colorInput, colorInputBorder, colorPrimaryLigth } from '../../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function AddVehicleBeParking({ route,navigation }) {
    const [spinner,setSpinner] = useState(false);
    const [vehicleTypeId, setVehicleTypeId] = useState(null);
    const [licensePlate, setLicensePlate] = useState(null);
    const [vehicleType, setVehicleType] = useState([
        {
            id:null,
            name:'Tipo de vehículo'
        },
        {
            id:1,
            name:'Automovil'
        },
        {
            id:2,
            name:'Motocicleta'
        },
        {
            id:3,
            name:'Bicicleta'
        },
    ]);
    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            n = value;
          }
        } catch (error) { }
      };
      ///Método para registrar vehiculos
    const registerVehicle = () => {
        if(vehicleTypeId === null){
            Alert.alert("","El tipo de vehículo es obligatorio.");
        } else if(licensePlate === null || licensePlate === ''){
            Alert.alert("","La placa del vehículo es obligatoria.");
        }else{
            let config = {
                headers: { Authorization: n }
                };
            setSpinner(true);
            axios.post(`${API_URL}customer/app/register/vehicle`,{
                vehicleType:vehicleTypeId,
                plate:licensePlate
            },config).then(response => {
                setSpinner(false);
                const cod = response.data.ResponseCode;
                if(cod === 0){
                Alert.alert("",response.data.ResponseMessage);
                navigation.navigate('BeParking')
                }else{
                Alert.alert("ERROR","No se puede registrar vehículo");
                }
            }).catch(error => {
                setSpinner(false);
                Alert.alert("Error",error.response.data.ResponseMessage);
                console.log(error.response.data);
            })
        }
    }


    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
        });
        return unsubscribe;
      }, [navigation]);
    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                <Text style={styles.title2}>Información del vehículo</Text>
                <Text style={styles.text2}>Descripción opcional...................</Text>
                <View style={styles.pickerStyle}>
                    <Picker 
                        selectedValue={vehicleTypeId}
                        style={{height: 40, width: width-40,borderColor: secondary,borderWidth: 1}} 
                        onValueChange={(itemValue, itemIndex) => setVehicleTypeId(itemValue)}>
                            {
                            vehicleType.map( (vehicle) => {
                            return <Picker.Item label={vehicle.name} color={colorInput} value={vehicle.id} key={vehicle.id}/>
                            })
                            }  
                    </Picker>
                </View>
                <PaperTextInput style={{width:width-35,backgroundColor:surface,color:colorInput,marginVertical:20,fontSize:14,}}  
                        onChangeText={(text) => setLicensePlate(text)}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Placas del vehículo" outlineColor={colorInputBorder}/> 
                <View style={{marginBottom:10,flex:3}}>
                    <TouchableOpacity style={{justifyContent:'center',alignItems:'center',backgroundColor: secondary,padding: 14,borderRadius: 10,width: width-30,}}
                        onPress={registerVehicle}
                    >
                        <Text style={styles.titleButton1}>REGISTRAR VEHÍCULO</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    );

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingBottom:20,
      paddingTop:20,
      paddingHorizontal:16,
      flex:1
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        marginBottom:20,
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:30,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:70,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
  })