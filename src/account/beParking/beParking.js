import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions,Modal, Image} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface,primary600, surfaceMediumEmphasis,
    primary800,OnSurfaceOverlay15,OnSurfaceDisabled,primary500, surfaceHighEmphasis, onSurfaceOverlay8,primary900,
    secondary600,secondary500,colorGray, surfaseDisabled,secondary50 } from '../../utils/colorVar';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { ProgressBar, Colors, Card } from 'react-native-paper';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
export default function BeParking({navigation}) {
    const [spinner, setSpinner] = useState(false);
    const [modalTermsVisible, setModalTermsVisible] = useState(false);
    const [token, setToken] = useState(null);
    const [progress, setProgress] = useState(0.2);
    const [blue, setBlue] = useState(false);
    const [activity, setActivity] = useState(false);
    const [info, setInfo] = useState([])
    

    const getToken =  async() => {
        try {
            const value =  await AsyncStorage.getItem('token');
            if (value !== null) {
                global.token = value;
                setToken(value);
            }
          } catch (error) {
            console.log(error);
          }
    }
    const getInfo = () => {

        const config = {
            headers: { Authorization: global.token }
        };

         axios.get(`${API_URL}customer/app/getInfoCustomer`,config).then(response => {
        setSpinner(false);
        const cod = response.data.ResponseCode; 
        console.log(response.data.ResponseMessage);
        if(cod === 0){
            let info = response.data.ResponseMessage
            setInfo(info)
        }else{
            Alert.alert("ERROR","No se pudo traer los vehiculos");
        }
        }).catch(error => {
        setSpinner(false);
        Alert.alert("",error.response.data.ResponseMessage);
        console.log(error.response.data.ResponseMessage);
        }) 

    }

    const continueButton = () => {
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        axios.get(`${API_URL}customer/app/list/vehicle`,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                if(response.data.ResponseMessage.vehicles.length === 0){
                    navigation.navigate('AddVehicleBeParking');
                }else{
                    setVehicles(true);
                }
            }else{
                Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
            }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
            })

    }
    const terms = () => {
        setModalTermsVisible(true);
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
          getInfo();
        });
        return unsubscribe;
      }, [navigation]);
    return (
        <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                {
                    activity ?
                    <TouchableOpacity onPress={() => setActivity(false)}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity onPress={() => blue ? setBlue(false) : navigation.navigate('BarNavigationRegister')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                }
                
            </View>
            <View style={{backgroundColor:surface,padding:20}}>
                <View>
                    <Image source={require('../../../assets/account/logopeque_green1.png')} width={106} height={62}/>
                </View>
            </View>
            <View style={{alignContent:'center',alignItems:'center',backgroundColor:surface,paddingHorizontal:20,paddingVertical:10}}>
                <Image source={blue ? require('../../../assets/account/blueLogoBe.png') :  require('../../../assets/account/greenLogoBe.png')} width={80} height={80}/>
                <Text style={{color: secondary,fontSize: 24,fontFamily:'Gotham-Bold',textTransform:'capitalize'}}>{info.name}</Text>
                <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily:'Gotham-Light'}}>Estas en nivel <Text style={ blue ? {color:secondary600,fontFamily:'Gotham-Medium'} : {color:primary600,fontFamily:'Gotham-Medium'}}>{blue ? 'Blue':'Green'}</Text></Text>
                <TouchableOpacity onPress={() => navigation.navigate('GetPoints')}>
                    <View style={{flexDirection:'row',backgroundColor:onSurfaceOverlay8,borderRadius:25,marginVertical:10,paddingVertical:8,paddingHorizontal:10}}>
                        <MaterialIcons name="help-outline" size={24} color={surfaceHighEmphasis} />
                        <Text style={{color: surfaceHighEmphasis,fontSize: 14,fontFamily:'Gotham-Medium',marginLeft:3}}>Como obtener puntos</Text>
                    </View>
                </TouchableOpacity>
                <View style={{flexDirection:'row',marginVertical:10}}>
                    <View style={[{borderRadius:10,marginVertical:10,paddingVertical:8,paddingHorizontal:10}, blue ? {backgroundColor:secondary50} :{backgroundColor:onSurfaceOverlay8}]}>
                        <Text style={[{fontSize: 16,fontFamily:'Gotham-Bold'}, blue ? {color: secondary}:{color: primary900}]}>{info.be_parking_points}</Text>
                        <Text style={{color: surfaceHighEmphasis,fontSize: 11,fontFamily:'Gotham-Medium',marginLeft:3}}>Puntos actuales</Text>
                    </View>
                    <View style={[{borderRadius:10,marginVertical:10,marginHorizontal:5,paddingVertical:8,paddingHorizontal:10}, blue ? {backgroundColor:secondary50} :{backgroundColor:onSurfaceOverlay8}]}>
                        <Text style={[{fontSize: 16,fontFamily:'Gotham-Bold'}, blue ? {color: secondary}:{color: primary900}]}>{info.be_parking_points + info.be_parking_redeem}</Text>
                        <Text style={{color: surfaceHighEmphasis,fontSize: 11,fontFamily:'Gotham-Medium',marginLeft:3}}>Total acumulado</Text>
                    </View>
                    <View style={[{borderRadius:10,marginVertical:10,paddingVertical:8,paddingHorizontal:10}, blue ? {backgroundColor:secondary50} :{backgroundColor:onSurfaceOverlay8}]}>
                        <Text style={[{fontSize: 16,fontFamily:'Gotham-Bold'}, blue ? {color: secondary}:{color: primary900}]}>0</Text>
                        <Text style={{color: surfaceHighEmphasis,fontSize: 11,fontFamily:'Gotham-Light',marginLeft:3}}>Bonos digitales</Text>
                    </View>
                </View>
            </View>

            {
                !blue &&
                <View style={{backgroundColor:surface,paddingHorizontal:20,paddingVertical:10}}>
                    <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily:'Gotham-Light'}}>Siguiente nivel: <Text style={{fontFamily:'Gotham-Bold'}}>Blue</Text></Text>
                    <View style={{flexDirection:'row',marginVertical:10}}>
                        <ProgressBar progress={progress} color={secondary500} style={{width:width-100}}/>
                        <TouchableOpacity style={{marginTop:-30}} onPress={() => setBlue(true)}>
                            <Image source={require('../../../assets/account/buttonBlue.png')} width={54} height={54}/>
                        </TouchableOpacity>
                    </View>
                </View>
            }
            <View style={{flexDirection:'row',paddingHorizontal:20,paddingVertical:10}}>
                <Text style={{fontFamily:'Gotham-Bold',color: secondary,fontSize: 24,marginRight:10}}>
                   {
                       activity ? 
                       'Actividad'
                       :
                       blue ? 'Beneficios Blue':'Beneficios Green'

                   } 
                    
                </Text>
                <View style={{alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',flexDirection:'row',marginTop:8}}>
                    <TouchableOpacity onPress={() => setActivity(!activity)}>
                        <MaterialIcons name="timeline" size={24} color={surfaceMediumEmphasis} style={{marginRight:15}} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate('Defeated')}>
                        <MaterialIcons name="history" size={24} color={surfaceMediumEmphasis} />
                    </TouchableOpacity>
                </View>
            </View>
            {
                activity ? 
                <View style={{backgroundColor:surface,paddingHorizontal:20,paddingVertical:10}}>
                    <TouchableOpacity style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 15, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:20, marginTop:20}}>
                        <View >
                            <View style={{flexDirection:'row',justifyContent:'center',}}>
                                <View style={{justifyContent:'center'}}>
                                    <MaterialCommunityIcons name="clock-outline" size={24} color={secondary} />
                                </View>
                                <View style={{marginLeft:20}}>
                                    <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>dd/mm/aa</Text>
                                    <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Bold',textTransform:'capitalize'}}>Nombre parqueadero</Text>
                                    <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily: 'Gotham-Light',}}>Placa / Tiempo / Costo / 00 pts</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 15, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:20, marginTop:20}}>
                        <View >
                            <View style={{flexDirection:'row',justifyContent:'center',}}>
                                <View style={{justifyContent:'center'}}>
                                    <MaterialCommunityIcons name="clock-outline" size={24} color={secondary} />
                                </View>
                                <View style={{marginLeft:20}}>
                                    <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>dd/mm/aa</Text>
                                    <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Bold',textTransform:'capitalize'}}>Nombre parqueadero</Text>
                                    <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily: 'Gotham-Light',}}>Placa / Tiempo / Costo / 00 pts</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                :
                <View>
                    <View style={{backgroundColor:surface,paddingHorizontal:20,paddingVertical:10}}>
                        <TouchableOpacity style={{marginBottom:20, marginTop:20}} onPress={ () => navigation.navigate('ListParksBeParking')} >
                            <View style={{flexDirection:'row',}}>
                                <View style={{justifyContent:'center',marginTop:-15}}>
                                    <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:20}}/>
                                </View>
                                <View style={{alignContent:'center',justifyContent:'center',marginLeft:20}}>
                                    <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Bold',}}>Tiempo Grátis Afiliación</Text>
                                    <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>1 hora | Disponible por 30 días</Text>
                                </View>
                                <View style={{alignContent:'flex-end',justifyContent:'center',marginLeft:'auto'}}>
                                    <MaterialIcons name="keyboard-arrow-right" size={34} color={secondary} />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{marginBottom:60 , marginTop:20}} disabled={true} >
                            <View style={{flexDirection:'row',}}>
                                <View style={{justifyContent:'center',marginTop:-15}}>
                                    <MaterialCommunityIcons name="heart-outline" size={24} color={surfaseDisabled} style={{marginTop:20}}/>
                                </View>
                                <View style={{alignContent:'center',justifyContent:'center',marginLeft:'auto'}}>
                                    <Text style={{color: surfaseDisabled,fontSize: 16,fontFamily: 'Gotham-Bold'}}>Tiempo por 500 pts acumulados</Text>
                                    <Text style={{color: surfaseDisabled,fontSize: 14,fontFamily: 'Gotham-Light',}}>1 hora | Disponible por 30 días</Text>
                                </View>
                                <View style={{alignContent:'flex-end',justifyContent:'center',marginLeft:'auto'}}>
                                    <MaterialIcons name="keyboard-arrow-right" size={34} color={surfaseDisabled} />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
{/*                     <View style={{paddingHorizontal:20,paddingVertical:10}}>
                        <Text style={{fontFamily:'Gotham-Bold',color: secondary,fontSize: 16,marginRight:10}}>Beneficios</Text>
                    </View> */}
                    {/* <View style={{backgroundColor:surface,paddingHorizontal:15,paddingTop:10,flexDirection:'row'}}>
                        <TouchableOpacity onPress={() => navigation.navigate('DescriptionBenefit')} style={{borderTopWidth:0,padding:10,backgroundColor: surface,borderRadius: 5, marginLeft: 4,marginRight: 4,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:20, marginTop:20}}>
                            <Ionicons name="build-outline" size={24} color={surfaceHighEmphasis} style={{textAlign:'center'}} />
                            <Text style={{fontFamily:'Gotham-Light',color: surfaceHighEmphasis,fontSize: 10,textAlign:'center'}}>BE WASH</Text>
                            <Text style={{color: surfaceHighEmphasis,fontSize: 18,fontFamily: 'Gotham-Bold',textAlign:'center'}}>10% Descuento</Text>
                            <Text style={{fontFamily:'Gotham-Light',color: surfaceHighEmphasis,fontSize: 14,textAlign:'center'}}>Lavado</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{borderTopWidth:0,padding:10,backgroundColor: surface,borderRadius: 5, marginLeft: 4,marginRight: 4,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:20, marginTop:20}}>
                            <Ionicons name="build-outline" size={24} color={surfaceHighEmphasis} style={{textAlign:'center'}} />
                            <Text style={{fontFamily:'Gotham-Light',color: surfaceHighEmphasis,fontSize: 10,textAlign:'center'}}>BE WASH</Text>
                            <Text style={{color: surfaceHighEmphasis,fontSize: 18,fontFamily: 'Gotham-Bold',textAlign:'center'}}>10% Descuento</Text>
                            <Text style={{fontFamily:'Gotham-Light',color: surfaceHighEmphasis,fontSize: 14,textAlign:'center'}}>Lavado</Text>
                        </TouchableOpacity>
                    </View> */}
                    {/* <View style={{backgroundColor:surface,paddingHorizontal:15,flexDirection:'row'}}>
                        <TouchableOpacity disabled style={{borderTopWidth:0,padding:10,backgroundColor: surface,borderRadius: 5, marginLeft: 4,marginRight: 4,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:20, marginTop:5}}>
                            <Ionicons name="build-outline" size={24} color={surfaseDisabled} style={{textAlign:'center'}} />
                            <Text style={{fontFamily:'Gotham-Light',color: surfaseDisabled,fontSize: 10,textAlign:'center'}}>BE WASH</Text>
                            <Text style={{color: surfaseDisabled,fontSize: 18,fontFamily: 'Gotham-Bold',textAlign:'center'}}>10% Descuento</Text>
                            <Text style={{fontFamily:'Gotham-Light',color: surfaseDisabled,fontSize: 14,textAlign:'center'}}>Lavado</Text>
                        </TouchableOpacity>
                    </View> */}
                </View>


            }
        </ScrollView>
    );

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:surface,
        padding:20,
    },
    form: {
        alignItems: 'flex-start',
        marginTop: 120,
    },
    title: {
        color: secondary,
        fontSize: 22,
        fontFamily: 'Gotham-Bold',
    },
    text: {
        color: secondary,
        fontSize: 15,
        fontFamily: 'Gotham-Light',
    },
    input: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 320,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    inputPassword:{
        height:50,  
        color: colorInput, 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none',
        flexDirection:'row',
      },
    inputPhone: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 180,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily: 'Gotham-Bold',
    },
    button1: {
        marginTop: 90,
        height: 50,
        backgroundColor: secondary,
        padding: 14,
        marginBottom:10,
        borderRadius: 10,
        width: width-50,
        alignItems: "center",
    },
    pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 56,
        marginVertical:20,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
    },
    modalView2: {
        elevation:10,
    },
    centeredView2: {
        flex: 1,
        backgroundColor:"white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: "#000",
        elevation: 15,
        marginTop:height-210,
        padding: 20,
      },
})
