import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, Modal, TouchableOpacity, TouchableWithoutFeedback, ScrollView,Dimensions, TextInput,Alert,LogBox} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { Chip } from 'react-native-paper';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import { TextInput as PaperTextInput } from 'react-native-paper';
import QRCode from 'react-native-qrcode-svg';
import CountDown from 'react-native-countdown-component';
import axios from "axios";
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity, onSurfaceOverlay8, colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50,primary500  } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions.']);

var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function PurchasedBonus ({navigation}) {
    
    const [spinner,setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [qrValue, setQrValue] = useState(null);
    const [totalDuration, setTotalDuration] = useState(null);
    const [progress, setProgress] = useState(null);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
    };

    /* const getTimeReserve = () =>{
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}customer/app/reservation/time/start/`+global.idReserva,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setTotalDuration(response.data.ResponseMessage);
            }else{
              Alert.alert("ERROR","No se pudo traer el tiempo de inicio de reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
    } */
    /* const getDataReserve = () => {
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.get(`${API_URL}customer/app/reservation/get/`+global.idReserva,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                var date = moment(response.data.ResponseMessage.initial_date).format('MMMM DD YYYY');
                var timeI = moment(response.data.ResponseMessage.initial_date).format('HH:mm');
                setHour(timeI);
                var timeF = moment(response.data.ResponseMessage.final_date).format('HH:mm');
                setDuration(response.data.ResponseMessage.duration);
                setHourFinal(timeF);
                setDate(date);
                setPrice(response.data.ResponseMessage.price);
                setTypeVehicleId(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.vehicle_type_id);
                setPlate(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.plate);
            }else{
              Alert.alert("ERROR","No se en traer datos de reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
        
    }; */
        useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          // getTimeReserve();
         // getDataReserve();
          getToken();
        });

        return unsubscribe;

      }, [navigation]);
    return(
        <ScrollView>
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('SelectVehicle')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <Spinner visible={spinner}  color={primary600} />
            <View style={styles.container}>
                <Text style={{color:secondary,fontSize:24,fontFamily:'Gotham-Bold',textAlign:'center'}}>Bono Adquirido</Text>
                <Text style={{color:secondary,fontSize:24,fontFamily:'Gotham-Bold',marginTop:20}}>Detalles del servicio</Text>
                <View>
                    <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Fecha</Text>
                    <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>03-22-2022</Text>
                    <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Tiempo adquirido</Text>
                    <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>1 Hora</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={{marginRight:100}}>
                        <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Hora inicial</Text>
                        <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>6:00 pm</Text>
                    </View>
                    <View>
                        <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Hora final</Text>
                        <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>7:00 pm</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={{marginRight:60}}>
                        <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Típo de vehículo</Text>
                        <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>Automovíl</Text>
                    </View>
                    <View>
                        <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Placas del vehículo</Text>
                        <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>AAA123</Text>
                    </View>
                </View>
                <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Costo</Text>
                <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>$ 0 - costo de bono</Text>
                <View style={{marginTop:30}}>
                    <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Light',textAlign:'center'}}>Presenta este código a la entrada y salida del punto de servicio cuando uses tu beneficio </Text>
                </View>
                <View style={{alignItems:'center', marginLeft: -25, marginBottom:30,marginTop:30}}>
                    <QRCode
                        value={ qrValue ? qrValue : 'NA' }
                        size={ 100 }
                        color= 'white'
                        backgroundColor='#519B00'
                    >
                    </QRCode>
                </View>
                <Text style={{color:secondary,fontSize:14,marginTop:10}}>LLega al parqueadero en: </Text>
                <View style={{alignContent:'flex-start',alignItems:'flex-start',fontFamily:'Gotham-Bold'}}>
                    { totalDuration &&
                        <CountDown
                            until={totalDuration}
                            style={{fontFamily:'Gotham-Bold'}}
                            digitTxtStyle={{color:secondary,fontFamily:'Gotham-Bold'}}
                            separatorStyle={{color:secondary,fontSize:15}}
                            digitStyle={{backgroundColor:'transparent'}}
                            showSeparator={true}
                            timeLabels={{d: '', h: '', m: '', s: ''}}
                            timetoShow={('M', 'S')}
                            size={18}
                        />
                    }
                    <ProgressBar progress={progress} color={primary500} style={{width:300}}/>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('ActivedBonus')}>
                    <Text style={{color:secondary,marginTop:20}}>Bono activo</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=> navigation.navigate('BeParking')}>
                    <Text style={{color:'rgba(0, 45, 51, 0.6)',fontSize:16,textAlign:'center',marginTop:60,fontFamily:'Gotham-Light'}}>CERRAR</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
  
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-350,
      marginTop:16,
    },
})