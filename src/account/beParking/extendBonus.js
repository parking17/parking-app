import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, Modal, TouchableOpacity, Image, TouchableWithoutFeedback, ScrollView,Dimensions, TextInput,Alert,LogBox} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { Chip } from 'react-native-paper';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import { TextInput as PaperTextInput } from 'react-native-paper';
import QRCode from 'react-native-qrcode-svg';
import CountDown from 'react-native-countdown-component';
import axios from "axios";
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity, onSurfaceOverlay8, colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50,primary500  } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions.']);

var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ExtendBonus ({navigation}) {
    
    const [spinner,setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [qrValue, setQrValue] = useState(null);
    const [totalDuration, setTotalDuration] = useState(null);
    const [progress, setProgress] = useState(null);
    const [parking,setParking] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [expanded, setExpanded] = useState(true);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
      };

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
       //Mostrar acordion
    const _handlePress = () => {
        setExpanded(!expanded);
    }
     useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getParkingInfo();
/*           getTimeReserve();
          getDataReserve(); */
          getToken();
        });
        return unsubscribe;

      }, [navigation]);
    return(
        <ScrollView>
             <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('ActivedBonus')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <Spinner visible={spinner}  color={primary600} />
            <View style={styles.container}>
                <Text style={{fontFamily:'Gotham-Bold',fontSize:24,color:secondary,textAlign:'center'}}>Bono Activo<    /Text>
                <Image
                    source={require("../../../assets/account/ilustracion_paseo.png")}
                    style={{ marginBottom:30, marginTop:20 }}
                ></Image>
                <View>
                    <Text style={{fontFamily:'Gotham-Bold',fontSize:16,textAlign:'center',color:secondary}}>Para usar tu BONO presenta este código a la entrada y salida del punto de servicio</Text>
                </View>
                <View style={{alignItems:'center', marginTop:20}}>
                    <QRCode
                        value={ qrValue ? qrValue : 'NA' }
                        size={ 100 }
                        color= 'white'
                        backgroundColor='#519B00'
                    >
                </QRCode>
                </View>
                <Text style={{color:secondary,fontSize:14,marginTop:10}}>Tu bono ha vencido </Text>
                <View style={{alignContent:'flex-start',alignItems:'flex-start',fontFamily:'Gotham-Bold'}}>
                    { totalDuration &&
                        <CountDown
                            until={totalDuration}
                            style={{fontFamily:'Gotham-Bold'}}
                            digitTxtStyle={{color:secondary,fontFamily:'Gotham-Bold'}}
                            separatorStyle={{color:secondary,fontSize:15}}
                            digitStyle={{backgroundColor:'transparent'}}
                            showSeparator={true}
                            timeLabels={{d: '', h: '', m: '', s: ''}}
                            timetoShow={('M', 'S')}
                            size={18}
                        />
                    }
                    <ProgressBar progress={progress} color={primary500} style={{width:300}}/>
                </View>
                <View style={{flexDirection:'row',marginTop:20}}>
                    <View style={{marginRight:'auto'}}>
                        <Text  style={{fontFamily:'Gotham-Bold',color:'#D40059',fontSize:16}}>Tiempo adicional</Text>
                    </View>
                    <View>
                        <Text style={{fontFamily:'Gotham-Bold',color:secondary,fontSize:16}}> 00:00:00 Hrs</Text>
                    </View>
                </View>
                <View style={{marginBottom:20}}>
                    <Text style={{color:secondary,fontSize:14,fontFamily:'Gotham-Bold',marginTop:20}}>Esta usando tiempo adicional</Text>
                    <Text style={{color:secondary,fontSize:14,fontFamily:'Gotham-Light'}}>A partir del vencimiento de tu bono se aplica la tarifa base para este 
                    parqueaadero de acuerdo al tiempo extra utilizado</Text>
                    <View style={{flexDirection:'row',marginTop:20,marginLeft:20}}>
                        <View style={{marginRight:'auto'}}>
                            <Text style={{color:secondary,fontSize:14,fontFamily:'Gotham-Light'}}>Costo por servicio adicional: </Text>
                        </View>
                        <View>
                            <Text style={{color:secondary,fontSize:14,fontFamily:'Gotham-Bold'}}>$ 00.00</Text>
                        </View>
                    </View>
                </View>
                                    <View style={styles.containerAccordion}>
                        <NativeBaseProvider>
                            <Center flex={1}>
                                <ScrollView>
                                    <Box m={3} style={{border:0}}>
                                        <Accordion allowMultiple _text={{border:0}} border={0} onTouchStart={_handlePress}>
                                            <Accordion.Item _text={{color:primary800}} >
                                                <Accordion.Summary  _expanded={{backgroundColor:surface,}}>
                                                    <View style={{flexDirection:'row',alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                                        <Text style={{fontFamily:'Gotham-Medium',fontSize:14,color:primary800}}>NOTIFICAR SALIDA</Text>
                                                        <View style={{marginLeft:20}}>
                                                            <MaterialIcons name={expanded ? "keyboard-arrow-down":"keyboard-arrow-up"} size={20} color={primary800} />
                                                        </View> 
                                                    </View> 
                                                </Accordion.Summary> 
                                                <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                    <TouchableOpacity style={{backgroundColor:onSurfaceOverlay8,borderRadius:25,width:width-120,padding:10}} onPress={() => navigation.navigate('FinalyBonus')} >
                                                        <Text style={{fontFamily:'Gotham-Light',color:secondary,textAlign:'center'}}>Notificar Ahora</Text>
                                                    </TouchableOpacity>
                                                </Accordion.Details>
                                                <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                    <TouchableOpacity style={{backgroundColor:onSurfaceOverlay8,borderRadius:25,width:width-120,padding:10}}>
                                                        <Text style={{fontFamily:'Gotham-Light',color:secondary,textAlign:'center'}}>Cancelar</Text>
                                                    </TouchableOpacity>
                                                </Accordion.Details>
                                            </Accordion.Item>
                                        </Accordion>
                                    </Box>
                                </ScrollView>
                            </Center>
                        </NativeBaseProvider>
                    </View>
                <View style={{flexDirection:'row', marginTop:20}}>
                    <View style={{marginRight:'auto'}}>
                        <Text style={{fontFamily:'Gotham-Bold',fontSize:16,color:secondary}}>Actividad Reciente</Text>                 
                    </View>
                    <TouchableOpacity onPress={() => navigation.navigate('Defeated')}>
                        <Text style={{fontFamily:'Gotham-Bold',fontSize:14,color:'rgba(0, 45, 51, 0.6)'}}>VER HISTORIAL</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={{justifyContent:'center'}}>
                    <View
                    style={{
                        backgroundColor: surface,
                        borderRadius: 10,
                        width:width-38,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        padding:15,
                    }}
                    >
                        <View style={{flexDirection: 'row'}}>
                            <View >
                                <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                    {
                                        parking !== null ?
                                        parking.name
                                        :
                                        ""
                                    }
                                </Text>
                                {
                                    parking !== null ?
                                        parking.address !== null ? 
                                            <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    :
                                    <Text style={styles.text}>No hay dirección disponible</Text>
                                }
                                <View style={{flexDirection: 'row'}} >
                                    {
                                        parking !== null &&
                                            parking.open == 1 ? 
                                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                            :
                                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                    }    
                                        
                                    {
                                        parking !== null ?
                                            parking.finalHour !== "" && parking.initialHour !== "" ? 
                                                <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay horario disponible</Text>
                                    }
                                </View>
                            </View>
                            <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto',marginTop:20}}>
                                <MaterialIcons name="assistant-direction" size={24} color={secondary} />    
                            </View>
                        </View>
                    </View> 
                </TouchableOpacity>
                <Text style={{color:secondary,marginTop:20,fontSize:16,fontFamily:'Gotham-Bold'}}>Detalles del servicio</Text>
                <View>
                    <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Fecha de compra</Text>
                    <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>03-22-2022</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={{marginRight:'auto'}}>
                        <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Fecha de activación</Text>
                        <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>03-22-2022</Text>
                    </View>
                    <View>
                        <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Hora de activación</Text>
                        <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>6:00 pm</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={{marginRight:'auto'}}>
                        <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Fecha de culminación</Text>
                        <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>03-22-2022</Text>
                    </View>
                    <View>
                        <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Hora de culminación</Text>
                        <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>7:00 pm</Text>
                    </View>
                </View>
                <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Vigencia</Text>
                <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>03-22-2022</Text>
                                <View style={{flexDirection:'row'}}>
                    <View style={{marginRight:60}}>
                        <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Típo de vehículo</Text>
                        <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>Automovíl</Text>
                    </View>
                    <View>
                        <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Placas del vehículo</Text>
                        <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>AAA123</Text>
                    </View>
                </View>
                <Text style={{color:'#005A6D',fontSize:14,fontFamily:'Gotham-Light',marginTop:20}}>Costo</Text>
                <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold'}}>$ 0 - costo de bono</Text>
                <TouchableOpacity onPress={()=> navigation.navigate('BeParking')}>
                    <Text style={{color:'rgba(0, 45, 51, 0.6)',fontSize:16,textAlign:'center',marginTop:60,fontFamily:'Gotham-Light'}}>CERRAR</Text>
                </TouchableOpacity>
            </View> 
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-350,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
    },
})