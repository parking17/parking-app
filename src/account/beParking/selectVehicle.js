import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, Modal, TouchableOpacity, TouchableWithoutFeedback, ScrollView,Dimensions, TextInput,Alert,LogBox} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import { TextInput as PaperTextInput } from 'react-native-paper';
import axios from "axios";
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity, onSurfaceOverlay8, colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions.']);

var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function SelectVehicle ({navigation}) {
    
    const [spinner,setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [listParking, setListParking] = useState([]);
    const [parking,setParking] = useState(null);
    const [date, setDate] = useState('Hoy');
    const [dateT, setDateT] = useState(null);
    const [dateSend, setDateSend] = useState(null);
    const [showDate, setShowDate] = useState(false);
    const [dateMax, setDateMax] = useState(null);
    const [dateOnly, setDateOnly] = useState(null);
    const [datePlusTime, setDatePlusTime] = useState(null);
    const [dateFinal, setDateFinal] = useState(null);
    const [dateInitial, setDateInitial] = useState(null);
    const [hour, setHour] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [showTime, setShowTime] = useState(false);
    const [firstItem, setFirstItem] = useState(false);
    const [secondItem, setSecondItem] = useState(false);
    const [firstDisabled, setFirstDisabled] = useState(true);
    const [secondDisabled, setSecondDisabled] = useState(true);
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            //getVehicles(value);
            getParkingInfo();
            
          }
        } catch (error) { }
      };

      const changeDateAndHour = () => {
        setShowDate(true);
    }
    const onChange = (event, selectedDate) => {
        setShowDate(false);
        if (event.type !== "dismissed") {
            const date = moment(String(selectedDate)).format("YYYY-MM-DD");
            setDate(date);
            setDateOnly(selectedDate);
            setShowTime(true);
            setShowDate(false);
        }
        setShowDate(false);
    }
    const onChangeTime = (selectedDate) => {
        var hours = selectedDate.getHours();
        var minutes = selectedDate.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        const date1 = moment(new Date).add(2,'h');
        const dateNow = new Date(date1);            
        if((dateOnly.getMonth() === new Date().getMonth()) && (dateOnly.getDate() === new Date().getDate())){
            
            if((selectedDate.getHours() >= dateNow.getHours())){
                var minuts = null;
                if(selectedDate.getMinutes() < 10){
                    minuts = "0"+selectedDate.getMinutes();
                }else{
                    minuts = selectedDate.getMinutes();
                }
                const dateSendFinal = new Date(dateOnly.setHours(selectedDate.getHours(),selectedDate.getMinutes(),selectedDate.getSeconds()));
                setDateFinal(date + " "+selectedDate.getHours()+":"+minuts+":"+selectedDate.getSeconds());
                setHour(strTime);
                setDateSend(dateSendFinal);
                setShowTime(false);
            }else{
                Alert.alert("","Debe ingresar una hora que sea mayor a dos horas desde este momento.");
            }
        }else{
                var minuts = null;
                if(selectedDate.getMinutes() < 10){
                    minuts = "0"+selectedDate.getMinutes();
                }else{
                    minuts = selectedDate.getMinutes();
                }
                const dateSendFinal = new Date(dateOnly.setHours(selectedDate.getHours(),minuts,selectedDate.getSeconds()));
                setDateFinal(date + " "+selectedDate.getHours()+":"+minuts+":"+selectedDate.getSeconds());
                setHour(strTime);
                setDateSend(dateSendFinal);
                setShowTime(false);
                
            }
        setShowTime(false);
    }
        const calcHourFinal = (times) =>{
        if(global.vehicleId){
            const hourFinal = moment(dateSend).add(times,'h');
            const datePlusF = new Date(hourFinal);
            setDatePlusTime(datePlusF);
            getPrice(times);
        }else{
            setFirstHour(false);
        setSecondHour(false);
        setThirdHour(false);
        setFourthHour(false);
        Alert.alert("","Debes seleccionar un vehículo");
        }
    }
    //Metodo de fecha mayor
    const dateMaxCalc = () => {
        const dateString = moment(String(new Date)).add(1,'M');
        const date = new Date(dateString);
        setDateMax(date);
    }
    const changeCar = () => {
        setFirstItem(true);
        setSecondItem(false);
/*         setTimes(1);
        calcHourFinal(1);  */
    }
    const changeMotorcycle = () => {
        setFirstItem(false);
        setSecondItem(true);
/*         setTimes(2);
        calcHourFinal(2); */ 
    }
     const terms = () => {
        setDialogTermsVisible(true);
    }

    const getParkingInfo = async () => {
        setSpinner(true);
        //var id = parseInt(global.parkingId);
        var id =  await AsyncStorage.getItem('parkingId');
        global.parkingId = id;
        //console.log(id);
         axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                //console.log(response.data.ResponseMessage);
                var capacities = response.data.ResponseMessage.capacity;
                capacities.forEach(capacity => {
                    if(capacity.price_1){
                        setFirstDisabled(false);
                    }
                    if(capacity.price_2){
                        setSecondDisabled(false);
                    }
                    if(capacity.price_3){
                        setThirdDisabled(false);
                    }
                    if(capacity.price_4){
                        setFourthDisabled(false);
                    }
                    
                });
                
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            navigation.navigate('BarNavigationRegister');
            console.log(error.response.data.ResponseMessage);
          }) 
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          dateMaxCalc();
          getToken();
           var dates = moment(new Date).add(2,'h');
          var dateI = new Date(dates);
          setDateInitial(dateI);
        });
        return unsubscribe;
      }, [navigation]);


    return (
        <ScrollView>
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('SelectedPark')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <Spinner visible={spinner}  color={primary600} />
            <View style={styles.container}>
                <View style={{paddingLeft: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10,marginBottom:20 }}>
                    <View style={{ marginTop: 10}}>
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                        {
                            parking !== null ?
                            parking.name
                            :
                            ""
                        }
                        </Text>
                        {
                            parking !== null ?
                                parking.address !== null ? 
                                    <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                :
                                <Text style={styles.text}>No hay dirección disponible</Text>
                            :
                            <Text style={styles.text}>No hay dirección disponible</Text>
                        }
                        
                        <View style={{flexDirection: 'row'}} >
                        {
                            parking !== null &&
                                parking.open == 1 ? 
                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                :
                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                        }    
                            
                            {
                                parking !== null ?
                                    parking.finalHour !== "" && parking.initialHour !== "" ? 
                                        <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                    :
                                    <Text style={styles.text}>No hay horario disponible</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                            }
                        </View>
                    </View>
                    <View style={{alignItems: 'center',justifyContent: 'center',}}>
                        <View style = {styles.lineStyle} />
                    </View>
                    <View>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Medium',}}>Tarifa base / Disponibilidad</Text>
                    </View>
                    <View style={{flexDirection:'row',}}>
                        {
                            parking !== null ?
                                parking.tariff.length !== 0 ?
                                
                                    parking.tariff.map( (tarifa) => {
                                        return <View style={{flexDirection:'row',marginTop:10, marginRight:10}} key={tarifa.vehicleType}>
                                                    <FontAwesomeIcon size={20} 
                                                    icon={tarifa.vehicleType === 1 ? faCar
                                                        :tarifa.vehicleType === 2 ? faMotorcycle :tarifa.vehicleType === 3 && faBicycle } color={primary700} />
                                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> ${tarifa.price} / <Text style={{color: surfaceMediumEmphasis}}>24</Text></Text>
                                                </View>
                                        })
                                :
                                <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                                </View>
                            :
                                <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                                </View>
                        }
                    </View>
                </View>
                <Text style={{fontFamily:'Gotham-Bold', fontSize:18,color:'#000',marginTop:10}}>Elige la hora de servicio</Text>
                <TouchableOpacity style={{justifyContent:'center'}} onPress={changeDateAndHour}>
                    <View
                    style={{
                        backgroundColor: surface,
                        borderRadius: 5,
                        width:width-38,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        padding:10,
                    }}
                    >
                        <View style={{flexDirection: 'row'}}>
                            <FontAwesomeIcon size={20} icon={faCalendar} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                            <View>
                                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                    {date}
                                </Text>
                                <Text style={styles.text}>{hour ? hour : 'Hora'}</Text>
                            </View>
                            <Text style={{ fontSize: 14, marginTop: 20,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}}>CAMBIAR</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                    {showDate && (
                    <DateTimePicker
                    minimumDate={new Date()}
                    value={new Date()}
                    mode="date"
                    is24Hour={true}
                    display="calendar"
                    onChange={onChange}
                    maximumDate={dateMax}
                    textColor="black"
                    />
                    )}
                    {showTime && dateInitial ? (
                        // <DateTimePicker
                        // value={dateInitial}
                        // minimumDate={dateInitial}
                        // mode="time"
                        // is24Hour={true}
                        // display="clock"
                        // onChange={onChangeTime}                        
                        // />
                        <DateTimePickerModal
                            isVisible={showTime}
                            mode="time"
                            date={dateInitial}
                            onConfirm={(selectedDate) => onChangeTime(selectedDate)}
                            onCancel={() => {
                            setShowTime(false)
                            }}
                            display="spinner"
                        />
                    )
                :
                <Text> </Text>
                }
                <Text style={{fontFamily:'Gotham-Bold', fontSize:18,color:'#000',marginTop:5, marginBottom:25}}>Elige tú vehiculo</Text>
                <View style={{flexDirection: 'row',marginBottom:10,marginLeft:10,overflow:'scroll'}}>
                    <Chip textStyle={{fontSize:12}} style={firstItem ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor:onSurfaceOverlay8, marginRight:6,}}  selected={firstItem} selectedColor={secondary} mode='flat' onPress={changeCar} disabled={firstDisabled}>CARRO</Chip>
                    <Chip textStyle={{fontSize:12}} style={secondItem ? {backgroundColor:colorPrimarySelect,marginRight:6}:{backgroundColor:onSurfaceOverlay8, marginRight:6}} selected={secondItem} selectedColor={secondary} mode='flat' onPress={changeMotorcycle} disabled={secondDisabled}>MOTO</Chip>
                </View>
                {/* tarjeta */}
                <View 
                    style={{
                        backgroundColor: surface,
                        borderRadius: 5,
                        width:width-38,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        padding:10,
                    }}  
                >
                    <TouchableOpacity>
                        <View style={{flexDirection: 'row'}}>
                            <View>
                                <FontAwesomeIcon icon={faCarSide} size={25} color={secondary} style={{marginTop:10}} />
                            </View>
                            <View>
                                <Text style={{color:'#002D33', fontSize:18,fontFamily:'Gotham-Bold', marginLeft:20}}>Automovíl</Text>
                                <Text style={{color:'#002D33', fontSize:14,fontFamily:'Gotham-Bold', marginLeft:20}}>AAA123</Text>
                            </View>
                            <View style={{marginTop:10,justifyContent:'flex-end',marginLeft:'auto', alignContent: 'center',marginBottom:18,right:10 }}>
                                <Text style={{color:'#519B00', fontSize:14,fontFamily:'Gotham-Bold'}}>SELECT</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('AddVehicle')}>
                    <Text style={{color:'#519B00',fontSize:14,fontFamily:'Gotham-Bold',textAlign:'center',marginTop:30}}>REGISTRAR NUEVA PLACA</Text> 
                </TouchableOpacity>
                <TouchableOpacity style={styles.button2} onPress={terms}>
                    <Text style={{color:'#F2FAE0',fontFamily:'Gotham-Bold',fontSize:14}}>CANJEAR BONO</Text>
                </TouchableOpacity>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogTermsVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={{color:secondary, fontSize:24,fontFamily:'Gotham-Bold',marginTop:20}}>¿deseas confirmar?</Text>
                                <Text style={{color:'#000', fontSize:14,fontFamily:'Gotham-Bold',marginTop:20}}>Se canjeara en bono con las caracteristicas seleccionadas</Text>
                                <View style={{flexDirection:'row',marginTop:20,justifyContent:'flex-end',marginRight:20}}>
                                    <TouchableOpacity style={styles.button1} onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                                        <Text style={{color:' rgba(0, 45, 51, 0.6)',fontSize:16,fontFamily:'Gotham-Bold',textAlign:'center',marginTop:10,fontStyle:'italic'}}>NO</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('PurchasedBonus')}>
                                    <Text style={{color:'#BAE65B',fontSize:16,fontFamily:'Gotham-Bold',textAlign:'center',marginTop:10,fontStyle:'italic'}}>SI</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* fin tarjeta */}
            </View>
        </ScrollView>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:10,
        paddingBottom:width-350,
        paddingTop:10,
    },
    button:{
        width:158,
        height:48,
        backgroundColor:secondary,
        borderRadius:15
    },
       button1:{
        width:158,
        height:48,
        backgroundColor:'#fff',
        borderRadius:15
    },
    button2: {
        width:340,
        height:48,
        backgroundColor: secondary,
        borderRadius:10,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:20,
        alignSelf: 'center',
    },
        modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
})