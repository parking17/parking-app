import React, { useState }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions} from 'react-native';
import { secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis } from '../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;
export default function NoVehicle({navigation}) {
    return(
        <View style={styles.container}>
            <Text style={styles.title2}>Aún no tienes vehículos registrados</Text>
            <Text style={styles.text2}>Descripción opcional...................</Text>
            <View style={{marginBottom:10}}>
                <TouchableOpacity style={{justifyContent:'center',alignItems:'center',backgroundColor: secondary,padding: 14,borderRadius: 10,width: width-30,}}
                onPress={() => navigation.navigate('AddVehicle')}
                >
                    <Text style={styles.titleButton1}>REGISTRAR NUEVO VEHÍCULO</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: surface,
      marginBottom:20,
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        width:320
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:30,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:'#1e294d', 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: "rgba(0, 45, 51, 0.3)",
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: "#B5FF17",
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        height:50,
        backgroundColor: "#002D33",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: "#B5C2C4",
        borderWidth: 1.2,
      },
      titleButton3: {
        color: "#002D33",
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
  })