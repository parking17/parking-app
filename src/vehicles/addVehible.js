import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, Alert,Modal,TouchableWithoutFeedback} from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import {Picker} from '@react-native-picker/picker';
import Spinner from "react-native-loading-spinner-overlay";
import { API_URL } from '../../url';
import axios from "axios";
import {productIdReserve,productIdPasadia} from "../utils/varService"
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import { TextInput as PaperTextInput } from 'react-native-paper';
import {primary600,primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis, colorInput, colorInputBorder, colorPrimaryLigth } from '../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function AddVehicle({navigation }) {
    const [spinner,setSpinner] = useState(false);
    const [vehicleTypeId, setVehicleTypeId] = useState(null);
    const [licensePlate, setLicensePlate] = useState(null);
    const [vehicleType, setVehicleType] = useState([
        {
            id:null,
            name:'Tipo de vehículo'
        },
        {
            id:1,
            name:'Automovil'
        },
        {
            id:2,
            name:'Motocicleta'
        },
        {
            id:3,
            name:'Bicicleta'
        }
      
    ]);
    const [parking, setParking] = useState(null);
    const [modalRegister, setModalRegister] = useState(false);
    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
          }
        } catch (error) { }
      };
    const registerVehicle = (assign) => {
        
        let config = {
            headers: { Authorization: global.token }
            };
        setSpinner(true);
        axios.post(`${API_URL}customer/app/register/vehicle`,{
            vehicleType:vehicleTypeId,
            plate:licensePlate,
            isAssigned:assign
        },config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
            save();
            Alert.alert("",response.data.ResponseMessage);
            if(assign == 0){
                    navigation.navigate('Account');
            }else{
                if(global.reserve){
                    navigation.navigate('Reserve');
                    global.vehicleTypeId = vehicleTypeId;
                    global.licensePlate = licensePlate;
                    global.vehicleId = response.data.Data.id;
                }
            }
            }else{
            Alert.alert("ERROR","No se puede registrar vehículo");
            }
        }).catch(error => {
            setSpinner(false);
            Alert.alert("Error",error.response.data.ResponseMessage);
            console.log(error.response.data);
        })
    }
    const shorModal = () => {
        if(vehicleTypeId === null){
            Alert.alert("","El tipo de vehículo es obligatorio.");
        } else if(licensePlate === null || licensePlate === ''){
            Alert.alert("","La placa del vehículo es obligatoria.");
        }else{
            if(vehicleTypeId === 1){
                var r = /[a-zA-Z]{3}[0-9]{3}|[a-zA-Z]{3}[0-9]{2}[a-zA-Z]/g
                if(r.test(licensePlate) === false){
                    Alert.alert("","La placa es inválida. Debe ser alfanumerica siguiendo el ejemplo: ABC123");
                }else{
                    setModalRegister(true);
                }
            }else if(vehicleTypeId == 2){
                var r = /[a-zA-Z]{3}[0-9],[a-zA-Z]{3}|[a-zA-Z]{3}[0-9]{2}[a-zA-Z]/g;
                if(r.test(licensePlate) === false){
                    Alert.alert("","La placa es inválida. Debe ser alfanumerica siguiendo el ejemplo: ABC12A");
                }else{
                    setModalRegister(true);
                }
            }else{
                setModalRegister(true);
            }

        }
    }
    const save = async () => {
        if(global.productId === productIdReserve){
            try {
    
              await AsyncStorage.setItem("vehicleTypeId", vehicleTypeId.toString());
              await AsyncStorage.setItem("licensePlate", licensePlate);
            } catch (e) {
              // saving error
              console.log('Fallo en guardar storage addVehicle');
            }
        }
      };

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            navigation.navigate('BarNavigationRegister',{register:register});
            console.log(error.response.data.ResponseMessage);
          })
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          //getParkingInfo();
          getToken();
        });
        return unsubscribe;
      }, [navigation]);
    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.goBack(null)}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={{paddingLeft: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10,marginBottom:16 }}>
                <View style={{ marginTop: 15}}>
                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                    {
                        parking !== null ?
                        parking.name
                        :
                        ""
                    }
                    </Text>
                    {/*
                        parking !== null ?
                            parking.address !== null ? 
                                <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                            :
                            <Text style={styles.text}>No hay dirección disponible</Text>
                        :
                        <Text style={styles.text}>No hay dirección disponible</Text>
                */}
                    
                    <View style={{flexDirection: 'row'}} >
                    {
                       /* parking !== null &&
                            parking.open == 1 ? 
                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                            :
                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                    */}    
                        
                        { /*
                            parking !== null ?
                                parking.finalHour !== "" && parking.initialHour !== "" ? 
                                    <Text style={styles.text}>{parking.initialHour} - {parking.finalHour}</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                            :
                            <Text style={styles.text}>No hay horario disponible</Text>
                       */ }
                    </View>
                </View>
            </View>
            <View style={styles.container}>
                <Text style={styles.title2}>Información del vehículo</Text>
                <Text style={styles.text2}>Descripción opcional...................</Text>
                <View style={styles.pickerStyle}>
                    <Picker 
                        selectedValue={vehicleTypeId}
                        style={{height: 40, width: width-40,borderColor: secondary,borderWidth: 1}} 
                        onValueChange={(itemValue, itemIndex) => setVehicleTypeId(itemValue)}>
                            {
                            vehicleType.map( (vehicle) => {
                            return <Picker.Item label={vehicle.name} color={colorInput} value={vehicle.id} key={vehicle.id}/>
                            })
                            }  
                    </Picker>
                </View>
                {
                    vehicleTypeId ?
                        vehicleTypeId != 3 ? 
                        <PaperTextInput style={{width:width-35,backgroundColor:surface,color:colorInput,marginVertical:20,fontSize:14,textTransform:'uppercase'}}  
                                onChangeText={(text) => setLicensePlate(text)} maxLength={6}
                                theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                                mode='outlined' label="Placas del vehículo" outlineColor={colorInputBorder}/> 
                        :
                        <PaperTextInput style={{width:width-35,backgroundColor:surface,color:colorInput,marginVertical:20,fontSize:14,textTransform:'uppercase'}}  
                                onChangeText={(text) => setLicensePlate(text)} maxLength={16}
                                theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                                mode='outlined' label="Placas del vehículo" outlineColor={colorInputBorder}/> 

                    :
                    <View></View>

                }
                <View style={{marginBottom:10,flex:3}}>
                    <TouchableOpacity style={{justifyContent:'center',alignItems:'center',backgroundColor: secondary,padding: 14,borderRadius: 10,width: width-30,}}
                        onPress={() => shorModal()}
                    >
                        <Text style={styles.titleButton1}>REGISTRAR VEHÍCULO</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalRegister}
                >
                    <TouchableWithoutFeedback onPress={() => setModalRegister(!modalRegister)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Registrar Vehiculo</Text>
                                <Text style={styles.text2}>¿Desea registrar el vehículo en su cuenta o sólo para {global.reserve && 'esta reserva?'}</Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => registerVehicle(0)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>REGISTRAR EN CUENTA</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => registerVehicle(1)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>REGISTRAR EN ESTE PRODUCTO</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        </ScrollView>
    );

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingBottom:20,
      paddingTop:20,
      paddingHorizontal:16,
      flex:1
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        marginBottom:20,
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:30,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:70,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })