import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions } from 'react-native';
import { faTimes, faArrowLeft, faArrowRight,faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import {faEye,faEyeSlash} from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import DateTimePicker from "@react-native-community/datetimepicker";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface, colorGray,primary600, surfaceMediumEmphasis } from './utils/colorVar';
import { API_URL } from '../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { TextInput as PaperTextInput } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
export default function formRegister({route, navigation }) {
    const {completeProfile,pass} = route.params;
    const [spinner, setSpinner] = useState(false);
    const [hidePass, setHidePass] = useState(true);
    const [hidePassConfirm, setHidePassConfirm] = useState(true);
    const [numForm, setNumForm] = useState(0);
    const [password, setPassword] = useState(null);
    const [passwordConfirm, setPasswordConfirm] = useState(null);
    const [name, setName] = useState(null);
    const [lastName, setLastName] = useState(null);
    const [phone, setPhone] = useState(null);
    const [areaCodeId, setAreaCodeId] = useState(1);
    const [areaCode, setAreaCode] = useState([
        {
            name:'+57',
            id:1,
        }
    ]);
    const [showDate, setShowDate] = useState(false);
    const [birthday, setBirthday] = useState(null);
    const [labelDate, setLabelDate] = useState('');
    const [token, setToken] = useState(null);

    const getToken =  async() => {
        try {
            const value =  await AsyncStorage.getItem('token');
            if (value !== null) {
                setToken(value);
            }
          } catch (error) {
            console.log(error);
          }
    }

    const next = () => {
        if(completeProfile === 0){
            
            if (password === null || password === '') {
                Alert.alert("", "Los campos son obligatorios");
            } else if (passwordConfirm === null || passwordConfirm === '') {
                Alert.alert("", "Los campos son obligatorios");
            } else if (password !== passwordConfirm) {
                Alert.alert("", "Las contraseñas no coinciden");
            } else {
                const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
                if (regex.test(password)) {
                    let config = {
                        headers: { Authorization: token }
                    };
                    setSpinner(true);
                    t(`${API_URL}user/changePassword`,{
                        oldPassword:pass,
                        newPassword:password,
                        newPasswordConfirmation:passwordConfirm
                    },config).then(response => {
                        setSpinner(false);
                        const cod = response.data.ResponseCode;
                        if(cod === 0){
                            Alert.alert("",response.data.ResponseMessage);
                            navigation.navigate("BarNavigationRegister",{register:true});
                        }else{
                        Alert.alert("ERROR","No se puede cambiar el password");
                        }
                    }).catch(error => {
                        setSpinner(false);
                        Alert.alert("",error.response.data.ResponseMessage);
                        console.log(error.response);
                    })
              } else {
                Alert.alert("", "La contraseña debe terner almenos una mayuscula, una minuscula, un digito y un caracter no alfanumérico");
              }
                
            }
        }else{
            if (numForm === 0) {
                if (password === null || password === '') {
                    Alert.alert("", "Los campos son obligatorios");
                } else if (passwordConfirm === null || passwordConfirm === '') {
                    Alert.alert("", "Los campos son obligatorios");
                } else if (password !== passwordConfirm) {
                    Alert.alert("", "Las contraseñas no coinciden");
                } else {
                    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
                    if (regex.test(password)) {
                        let config = {
                            headers: { Authorization: token }
                        };
                        setSpinner(true);
                        t(`${API_URL}user/changePassword`,{
                            oldPassword:pass,
                            newPassword:password,
                            newPasswordConfirmation:passwordConfirm
                        },config).then(response => {
                            setSpinner(false);
                            const cod = response.data.ResponseCode;
                            if(cod === 0){
                                Alert.alert("",response.data.ResponseMessage);
                                setNumForm(1);
                            }else{
                            Alert.alert("ERROR","No se puede cambiar el password");
                            }
                        }).catch(error => {
                            setSpinner(false);
                            Alert.alert("",error.response.data.ResponseMessage);
                            console.log(error.response);
                        })
                    } else {
                        Alert.alert("", "La contraseña debe terner almenos una mayuscula, una minuscula, un digito y un caracter no alfanumérico");
                    }
                    
                }
            } else if (numForm === 1) {
                if (name === null || name === '') {
                    Alert.alert("", "Los campos son obligatorios.");
                } else if (lastName === null || lastName === '') {
                    Alert.alert("", "Los campos son obligatorios.");
                } else {
                    setNumForm(2);
                }
            } else if(numForm === 2){
                if(phone === null || phone === ''){
                    Alert.alert("", "Número télefono obligatorio.");
                }else if(phone.length !== 10){
                    Alert.alert("", "Número de télefono inválido.");
                }else {
                    setNumForm(3);
                }
            }else if(numForm ===3){
                if(birthday === null){
                    Alert.alert("", "Fecha de cumpleaños obligatoria.");
                }else{
                    let config = {
                        headers: { Authorization: token }
                      };
                    setSpinner(true);
                    t(`${API_URL}customer/app/complete/profile`,{
                        name: name,
                        lastName: lastName,
                        phone: phone,
                        birthday: birthday
                    },config).then(response => {
                        setSpinner(false);
                        const cod = response.data.ResponseCode;
                        if(cod === 0){
                            Alert.alert("",response.data.ResponseMessage);
                            navigation.navigate("BarNavigationRegister",{register:true});
                        }else{
                        Alert.alert("ERROR","No se puede registrar");
                        }
                    }).catch(error => {
                        setSpinner(false);
                        Alert.alert("",error.response.data.ResponseMessage);
                        console.log(error.response);
                    })
                    
                }
            }
        }


    }
    const onChange = (event, selectedDate) => {
        setShowDate(false);
        if (event.type !== "dismissed") {
            const date = moment(String(selectedDate)).format("YYYY-MM-DD");
            setBirthday(date);
            setLabelDate(date);
            setShowDate(false);
        }
        setShowDate(false);
    };

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
        });
        return unsubscribe;
      }, [navigation]);
    return (
        <KeyboardAvoidingView style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Spinner visible={spinner}  color={primary600} />
                {
                    numForm === 0 &&
                    <View style={{ marginLeft: 2, marginTop: 15 }}>
                        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                            <FontAwesomeIcon size={35} icon={faTimes} color={secondary} />
                        </TouchableOpacity>
                    </View>

                }
                {
                    numForm > 0 &&
                    <View style={{ marginLeft: 0, marginTop: 15 }}>
                        <TouchableOpacity onPress={() => setNumForm(numForm - 1)}>
                            <MaterialIcons name="arrow-back" size={24} color={secondary} />
                        </TouchableOpacity>
                    </View>
                }
                {
                    numForm === 0 &&
                    <View style={styles.form}>
                        <Text style={styles.title}>Contraseña</Text>
                        <Text style={styles.text}>Ingrese su contraseña</Text>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:20,fontSize:14}} secureTextEntry={hidePass} onChangeText={(text) => {setPassword(text)}} 
                        value={password}
                        right={<PaperTextInput.Icon 
                            name={() => <Ionicons name={hidePass ?"eye-outline":"eye-off-outline"} size={24} color={surfaceMediumEmphasis} onPress={() => setHidePass(!hidePass)} />} />}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Contraseña" outlineColor={colorInputBorder}/>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:20,fontSize:14}} secureTextEntry={hidePassConfirm} onChangeText={(text) => {setPasswordConfirm(text)}}
                        value={passwordConfirm} 
                        right={<PaperTextInput.Icon 
                            name={() => <Ionicons name={hidePassConfirm ?"eye-outline":"eye-off-outline"} size={24} color={surfaceMediumEmphasis} onPress={() => setHidePassConfirm(!hidePassConfirm)} />} />}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Confirmar Contraseña" outlineColor={colorInputBorder}/>
                        <TouchableOpacity style={styles.button1} onPress={next}>
                            <Text style={styles.titleButton1}>CONTINUAR</Text>
                        </TouchableOpacity>
                    </View>
                }
                {
                    numForm === 1 &&
                    <View style={styles.form}>
                        <Text style={styles.title}>1 <FontAwesomeIcon size={18} icon={faArrowRight} color={secondary} /> Nombres y apellidos</Text>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,marginVertical:20,fontSize:14,}}  onChangeText={text => { setName(text) }} value={name}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Nombres" outlineColor={colorInputBorder}/>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,marginVertical:20,fontSize:14,}}  onChangeText={text => { setLastName(text) }} value={lastName}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Apellidos" outlineColor={colorInputBorder}/>
                        <TouchableOpacity style={styles.button1} onPress={next}>
                            <Text style={styles.titleButton1}>CONTINUAR</Text>
                        </TouchableOpacity>
                    </View>
                }
                {
                    numForm === 2 &&
                    <View>
                        <View style={{alignItems: 'flex-start',marginTop: 120,}}>
                            <Text style={styles.title}>2 <FontAwesomeIcon size={18} icon={faArrowRight} color={secondary} /> Datos de contácto</Text>
                            <View style={{flexDirection: 'row',marginTop:30}}>
                                <View style={{marginTop:23,marginRight:10}}>
                                    <FontAwesomeIcon size={18} icon={faPhoneAlt} color={secondary} />
                                </View>
                                <View style={{marginTop:7}}>
                                    <View style={styles.pickerStyle}>
                                        <Picker 
                                            selectedValue={areaCodeId}
                                            style={{height: 40, width: 95,borderColor: secondary,borderWidth: 1}} 
                                            onValueChange={(itemValue, itemIndex) => setAreaCodeId(itemValue)}>
                                                {
                                                areaCode.map( (code) => {
                                                return <Picker.Item label={code.name} color={colorInput} value={code.id} key={code.id}/>
                                                })
                                                }  
                                        </Picker>
                                    </View> 
                                </View>
                                <View>
                                    <PaperTextInput style={{width:width-180,backgroundColor:surface,color:colorInput,marginBottom:10,fontSize:14,}}  
                                    onChangeText={text => { setPhone(text) }} value={phone} keyboardType="numeric"
                                    theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                                    mode='outlined' label="Número móvil" outlineColor={colorInputBorder}/>                
                                </View>
                            </View>
                        </View>
                        <View style={{alignContent:'center',alignItems:'center'}}>
                            <TouchableOpacity style={styles.button1} onPress={next}>
                                <Text style={styles.titleButton1}>CONTINUAR</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                }
                {
                    numForm === 3 &&
                    <View style={styles.form}>
                        <Text style={styles.title}>3 <FontAwesomeIcon size={18} icon={faArrowRight} color={secondary} /> Cumpleaños</Text>
                        <View style={{marginTop:30}}>
                            <TouchableOpacity onPress={() => setShowDate(true)}>
                                <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,marginBottom:10,fontSize:14,}}  
                                        onChangeText={text => { setPhone(text) }} value={labelDate} editable={false}
                                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                                        mode='outlined' label="Fecha de nacimiento" outlineColor={colorInputBorder}/> 
                            </TouchableOpacity>
                            {showDate && (
                                <DateTimePicker
                                maximumDate={new Date()}
                                value={new Date()}
                                mode="date"
                                is24Hour={true}
                                display="calendar"
                                onChange={onChange}
                                />
                            )}
                            <TouchableOpacity style={styles.button1} onPress={next}>
                                <Text style={styles.titleButton1}>FINALIZAR</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                }
            </ScrollView>
        </KeyboardAvoidingView>
    );

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:surface
    },
    form: {
        alignItems: 'flex-start',
        marginTop: 120,
    },
    title: {
        color: secondary,
        fontSize: 22,
        fontFamily: 'Gotham-Bold',
    },
    text: {
        color: secondary,
        fontSize: 15,
        fontFamily: 'Gotham-Light',
    },
    input: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 320,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    inputPassword:{
        height:50,  
        color: colorInput, 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none',
        flexDirection:'row',
      },
    inputPhone: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 180,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily: 'Gotham-Bold',
    },
    button1: {
        marginTop: 100,
        height: 50,
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-50,
        alignItems: "center",
    },
    pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 56,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    }, 
})
