import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Alert,Modal,Dimensions,TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faChevronRight,faTimes} from '@fortawesome/free-solid-svg-icons';
import {faHeart} from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import { primary800,secondary,surface, colorGray, colorGrayOpacity,primary700,primary600,surfaceHighEmphasis,surfaceMediumEmphasis } from '../utils/colorVar';
import { API_URL } from '../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {productIdReserve,productIdValet,productIdPasadia} from '../utils/varService';
import { MaterialIcons,Feather,Ionicons,FontAwesome, MaterialCommunityIcons} from '@expo/vector-icons';
var width = Dimensions.get('window').width;
export default function DetailParking({navigation}) {
    const [dialogReserveVisible,setDialogReserveVisible] = useState(false);
    const [spinner,setSpinner] = useState(false);
    const [modalRegister,setModalRegister] = useState(false);
    const [parking,setParking] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);

    const goReserve = () => {
        setDialogReserveVisible(false);
        navigation.navigate('Reserve');
    }
    ///Método para traer la info del parking
    const getParkingInfo = () => {
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            navigation.navigate('BarNavigationRegister',{register:register});
            console.log(error.response.data.ResponseMessage);
          })
    }
    //Método para ir a servicio seleccionado
    const goToService = (productId) => {
        if(productId === productIdReserve && global.register){
            let config = {
                headers: { Authorization: global.token }
                };
            setSpinner(true);
            axios.get(`${API_URL}customer/app/product/get/status/`+productIdReserve,config).then(response => {
                setSpinner(false);
                const cod = response.data.ResponseCode;
                if(cod === 0){
                    console.log(response.data.Data);
                    if(response.data.Data){
                        global.idReserva = response.data.Data.id;
                        if(response.data.Data.status_id == 7){
                            navigation.navigate('SumaryReserve');
                        }else if(response.data.Data.status_id == 8){
                            navigation.navigate('ScheduledReserve');
                        }else if(response.data.Data.status_id == 1){
                            navigation.navigate('ActiveReserve');
                        }else if(response.data.Data.status_id == 9){
                            navigation.navigate('ExtendedReserve');
                        }
                    }else{
                        setDialogReserveVisible(true);
                    }
                }else{
                Alert.alert("ERROR","No se pudo traer el parqueadero");
                }
            }).catch(error => {
                setSpinner(false);
                Alert.alert("",error.response.data.ResponseMessage);
                console.log(error.response);
            })
        }else if(productId === productIdPasadia && global.register){
            navigation.navigate('Pasadia');
        }else if(productId === productIdValet && global.register){
            navigation.navigate('ConsolidatedValet');
        }
        if(!global.register){
            setModalRegister(true);
        }
    }
    
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          //getParkingInfo();
        });
        return unsubscribe;
      }, [navigation]);
    return(
        <ScrollView  showsVerticalScrollIndicator={false} style={{backgroundColor:colorGray}}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('BarNavigationRegister')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
                {/* <Text style={{fontFamily:'Gotham-Bold',color:secondary,marginLeft:10,fontSize:16}}>Detalle</Text> */}
            </View>
            <View style={{paddingLeft: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10,marginBottom:20 }}>
                <View style={{ marginTop: 15}}>
                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                    {
                        parking !== null ?
                        parking.name
                        :
                        ""
                    }
                    </Text>
                    {
                        parking !== null ?
                            parking.address !== null ? 
                                <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                            :
                            <Text style={styles.text}>No hay dirección disponible</Text>
                        :
                        <Text style={styles.text}>No hay dirección disponible</Text>
                    }
                    
                    <View style={{flexDirection: 'row'}} >
                    {
                        parking !== null &&
                            parking.open == 1 ? 
                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                            :
                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                    }    
                        
                        {
                            parking !== null ?
                                parking.finalHour !== "" && parking.initialHour !== "" ? 
                                    <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                            :
                            <Text style={styles.text}>No hay horario disponible</Text>
                        }
                    </View>
                </View>
                <View style={{alignItems: 'center',justifyContent: 'center',}}>
                    <View style = {styles.lineStyle} />
                </View>
                <View>
                    <Text style={{color: surfaceHighEmphasis,fontSize: 13,fontFamily: 'Gotham-Medium',}}>Tarifa base / <Text style={{color:surfaceMediumEmphasis,fontFamily: 'Gotham-Light',}}>Cupos disponibles</Text></Text>
                </View>
                <View style={{flexDirection:'row',}}>
                    {
                        parking !== null ?
                            parking.tariff.length !== 0 ?
                            
                                parking.tariff.map( (tarifa) => {
                                    return <View style={{flexDirection:'row',marginTop:10, marginRight:10}} key={tarifa.vehicleType}>
                                                <FontAwesomeIcon size={20} 
                                                icon={tarifa.vehicleType === 1 ? faCar
                                                    :tarifa.vehicleType === 2 ? faMotorcycle :tarifa.vehicleType === 3 && faBicycle } color={primary700} />
                                                <Text style={{color: surfaceHighEmphasis,fontSize: 13,fontFamily: 'Gotham-Medium',}}> ${tarifa.price} / <Text style={{color: surfaceMediumEmphasis,fontFamily: 'Gotham-Light',}}>24</Text></Text>
                                            </View>
                                    })
                            :
                            <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                            </View>
                        :
                            <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                            </View>
                    }
                </View>
            </View>
            <View style={styles.container}>
                {
                    parking !== null ?
                        parking.services.length > 0 ?
                            parking.services.map((service) => {
                            return <TouchableOpacity style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 5, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:20, marginTop:20}} onPress={() => goToService(service.productId)} key={service.productId}>
                                        <View >
                                            <View style={{flexDirection:'row',}}>
                                                {service.productId === productIdReserve ? <MaterialCommunityIcons name="calendar-multiselect" size={24} color={secondary} style={{marginTop:20}}/> : <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:20}}/>}
                                                <View style={{width:200,marginLeft:30,marginRight:30}}>
                                                    <Text style={{color: secondary,fontSize: 18,fontFamily: 'Gotham-Bold',textTransform:'capitalize'}}>{service.name}</Text>
                                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Light',}}>(Condiciones porcentaje de descuento)</Text>
                                                </View>
                                                <FontAwesomeIcon size={15} icon={faChevronRight} color={secondary} style={{marginTop:20}} />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                            })
                        :
                            <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}> No hay servicios disponibles</Text>
                            </View>
                    :
                        <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                            <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}> No hay servicios disponibles</Text>
                        </View>        
                }
                    <TouchableOpacity style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 5, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:20, marginTop:20}} onPress={() => goToService(productIdPasadia)}>
                        <View >
                            <View style={{flexDirection:'row',}}>
                                <MaterialCommunityIcons name="calendar-multiselect" size={24} color={secondary} style={{marginTop:20}}/>
                                <View style={{width:200,marginLeft:30,marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 18,fontFamily: 'Gotham-Bold',textTransform:'capitalize'}}>Pasadía</Text>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Light',}}>(Condiciones porcentaje de descuento)</Text>
                                </View>
                                <FontAwesomeIcon size={15} icon={faChevronRight} color={secondary} style={{marginTop:20}} />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 5, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:20, marginTop:20}} onPress={() => goToService(productIdValet)}>
                        <View >
                            <View style={{flexDirection:'row',}}>
                                <MaterialCommunityIcons name="calendar-multiselect" size={24} color={secondary} style={{marginTop:20}}/>
                                <View style={{width:200,marginLeft:30,marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 18,fontFamily: 'Gotham-Bold',textTransform:'capitalize'}}>Valet Parking</Text>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Light',}}>(Condiciones porcentaje de descuento)</Text>
                                </View>
                                <FontAwesomeIcon size={15} icon={faChevronRight} color={secondary} style={{marginTop:20}} />
                            </View>
                        </View>
                    </TouchableOpacity>
                
            </View>
            <TouchableOpacity style={styles.container}>
                <View
                style={{
                    backgroundColor: surface,
                    borderRadius: 5,
                    marginLeft: 0,
                    marginRight: 1,
                    shadowColor: "#000",
                    shadowOffset: {
                    width: 0,
                    height: 1,
                    },
                    shadowOpacity: 0.49,
                    shadowRadius: 4.65,
                    elevation: 3,
                    marginBottom:20,
                    marginTop:20
                }}
                >
                <View style={{ marginTop: 10, marginLeft: 15, width: 300 }}>
                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:25 }}>
                        Mensualidades
                    </Text>
                    <View style={{marginBottom:10,width:280}} >
                        <Text style={styles.text}>Sed rutrum enim sodales nascetur neque odio convallis non.</Text>
                    </View>
                </View>
                <View style={{backgroundColor:colorGrayOpacity,paddingLeft:15,paddingBottom:10,paddingTop:10}}>
                    <Text style={{ fontFamily: "Gotham-Bold", color: primary800,fontSize:16 }}>
                        Nombre Plan Destacado
                    </Text>
                    <Text style={styles.text}>$0.00.00 Cop % Descuento</Text>
                </View>
                <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15,}}>
                    <Text style={{ fontFamily: "Gotham-Medium", color: secondary,fontSize:13,marginTop:10 }}>
                    CÓNOCE MÁS PLANES
                    </Text>
                    <FontAwesomeIcon size={15} icon={faChevronRight} color={secondary} style={{marginLeft:40,marginTop:12}}/>
                </View>
                </View>
            </TouchableOpacity>
            <Modal
                animationType="slide"
                transparent={true}
                visible={dialogReserveVisible}
            >
                <TouchableWithoutFeedback onPress={() => setDialogReserveVisible(!dialogReserveVisible)}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                <Text style={styles.title2}>Tienes 10 minutos para completar tu reserva</Text>
                            </View>
                            <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center',marginTop:10}}>
                                <Text style={styles.text2}>Descripción opcional.....</Text>
                            </View>
                            <View style={{marginBottom:10,alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                <TouchableOpacity style={styles.button1} onPress={goReserve}>
                                    <Text style={styles.titleButton1}>ACEPTAR Y CONTINUAR</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
            <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalRegister}
                >
                    <TouchableWithoutFeedback onPress={() => setModalRegister(false)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{alignItems:'center',alignContent:'center'}}>
                                <Text style={styles.title2}>Registrate y accede a más</Text>
                                <Text style={styles.text2}>Registrate y accede a más.....</Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={styles.button1} onPress={() => navigation.navigate('Register')}>
                                        <Text style={styles.titleButton1}>CREAR CUENTA</Text>
                                    </TouchableOpacity>
                                </View>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
        </ScrollView>
    );

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: surface,
      marginBottom:20,
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        width:320
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:30,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:'#1e294d', 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: "rgba(0, 45, 51, 0.3)",
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: "#B5FF17",
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        height:50,
        backgroundColor: "#002D33",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        alignContent:'center',
        justifyContent:'center'
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: "#B5C2C4",
        borderWidth: 1.2,
      },
      titleButton3: {
        color: "#002D33",
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })