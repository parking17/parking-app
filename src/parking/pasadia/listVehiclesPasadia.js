import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,CheckBox,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../../utils/colorVar';
import { productIdPasadia } from '../../utils/varService';
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ListVehiclesPasadia({navigation}) {
    const [vehicles, setVehicles] = useState([]);
    const [spinner,setSpinner] = useState(false);
   

    const selectVehicle =  async (vehicleTypeId,licensePlate,vehicleId, token) => {
        
        //navigation.navigate('SumaryPasadia');
        
    /// Creación de Pasadía
     //const car =  await AsyncStorage.getItem("vehicleTypesId")
     const subProductId = vehicleTypeId ===1 ? 84 : 83 


    console.log(subProductId);
    let config = {
        headers : {Authorization: global.token}
    }


    axios.post(`${API_URL}customer/app/dayPass`,{"subProductId": subProductId,
        "description": "",
        "vehicleId": vehicleId}, config).then(response => {
      setSpinner(false);
      
        save(vehicleTypeId,licensePlate)
      
      const cod = response.data.ResponseCode;
       if(cod === 0){
        //setDialogVisible(false);
        Alert.alert("",response.data.ResponseMessage);
        navigation.navigate('SumaryPasadia');
      }else{
        Alert.alert("ERROR","No se puede registrar");
      }  
    }).catch(error => {
      setSpinner(false);
      //setDialogVisible(false);
      Alert.alert("",error.response.data.ResponseMessage.errors[0].msg);
      console.log(error.response.data);
    })

        
    }
    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken = value
            getVehicles(value);
            //selectVehicle(value);
          }
        } catch (error) { }
      };
    const save = async (vehicleTypeId,licensePlate) => {
        try {

          await AsyncStorage.setItem("vehicleTypeId", vehicleTypeId.toString());
          await AsyncStorage.setItem("licensePlate", licensePlate);
          //await AsyncStorage.setItem("pasadiaId", pasadiaId);
        } catch (e) {
          // saving error
          console.log('Fallo en guardar storage addVehicle');
        }
      };

    ///////////verificar si tiene vehiculos registrados
    const getVehicles = (token) => {
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        //console.log(`Archivo config __________ ${JSON.stringify(config)}`);

        axios.get(`${API_URL}customer/app/get/vehicle/type/1`,config).then(response => {
            
            
            const cod = response.data.ResponseCode;
            setSpinner(false);
            if(cod === 0){
                //console.log(`Archivo config __________ ${JSON.stringify(response.data)}`)
                setVehicles(response.data.ResponseMessage);
                //navigation.navigate('SumaryPasadia');
            }else{
              Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            //console.log(error.response.data.ResponseMessage);
          })
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
        });
        return unsubscribe;
    }, [navigation]);
    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('Pasadia')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
                <Text style={{fontFamily:'Gotham-Bold',color:secondary,marginLeft:10,fontSize:16}}>Pasadía</Text>
            </View>
            <View style={styles.container}>
                <Text style={styles.title2}>Vehículo</Text>
                <Text style={styles.text2}>Relaciona el vehiculo y placas que haras uso en la pasadía</Text>
                <View >
                    {
                        vehicles.map( (vehicle) => {
                            return <TouchableOpacity style={{justifyContent:'center'}} key={vehicle.id} onPress={() => selectVehicle(vehicle.vehicle_type_id,vehicle.plate, vehicle.id)}>
                                        <View
                                        style={{
                                            backgroundColor: surface,
                                            borderRadius: 5,
                                            width:width-40,
                                            marginLeft: 0,
                                            marginRight: 1,
                                            shadowColor: "#000",
                                            shadowOffset: {
                                            width: 0,
                                            height: 1,
                                            },
                                            shadowOpacity: 0.49,
                                            shadowRadius: 4.65,
                                            elevation: 3,
                                            marginBottom:10,
                                            marginTop:10,
                                            paddingTop:5,
                                        }}
                                        >
                                            <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                                                {
                                                    vehicle.vehicle_type_id === 1 &&
                                                    <FontAwesomeIcon size={20} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                {
                                                    vehicle.vehicle_type_id === 2 &&
                                                    <FontAwesomeIcon size={20} icon={faMotorcycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                {
                                                    vehicle.vehicle_type_id === 3 &&
                                                    <FontAwesomeIcon size={20} icon={faBicycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                <View>
                                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                                        {
                                                            vehicle.vehicle_type_id === 1 &&
                                                            "Automóvil"
                                                        }
                                                        {
                                                            vehicle.vehicle_type_id === 2 &&
                                                            "Motocicleta"
                                                        }
                                                        {
                                                            vehicle.vehicle_type_id === 3 &&
                                                            "Bicicleta"
                                                        }
                                                                
                                                    </Text>
                                                    <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                                        {vehicle.plate}
                                                    </Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                            })
                    }
                    
                </View>
                
            </View>
            <View style={{backgroundColor:surface,paddingBottom:20,paddingTop: 120}}>
                <TouchableOpacity onPress={() => navigation.navigate('AddVehicle',{productId:productIdPasadia})} >
                    <Text style={{ fontSize: 15,fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center", color:primary800}}>
                        REGISTRAR NUEVA PLACA
                    </Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingLeft:20,
      paddingBottom:20,
      paddingTop:20,
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:30,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
  })