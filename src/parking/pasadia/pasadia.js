import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function Pasadia({navigation}) {
const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
const [spinner,setSpinner] = useState(false);
const [parking,setParking] = useState(null);
const [openHour, setOpenHour] = useState(null);
const [closeHour, setCloseHour] = useState(null);
const [token, setToken] = useState(null);
const [cant, setCant] = useState('0');



const getToken = async () => {
    try {
      const value = await AsyncStorage.getItem("token");
      if (value !== null) {
        global.token = value;
        setToken(value);
        getCantPass(value);
      }
    } catch (error) { }
  };
// ///Método para traer la info del parking

  const getCantPass = () => {
     setSpinner(true);
     //var id = parseInt(global.parkingId);

    let config = {
            headers: { Authorization: global.token }
            };

     axios.get(`${API_URL}customer/app/getCantPass`, config).then(response => {
         setSpinner(false);
         const cod = response.data.ResponseCode;
         if(cod === 0){
             
             setCant(response.data.ResponseMessage.cantidad)
             /* setParking(response.data.ResponseMessage);
             const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
             const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
             setOpenHour(hourO);
             setCloseHour(hourF); */
         }else{
           Alert.alert("ERROR","No se pudo traer el parqueadero");
         }
       }).catch(error => {
         setSpinner(false);
         Alert.alert("",error.response.data.ResponseMessage);
         navigation.navigate('BarNavigationRegister');
         console.log(error.response.data.ResponseMessage);
       })
 } 

//metodo para contar pasadias


//Metodo para ver parking habilitados
const goToListParking = (vehicleId) => {
    AsyncStorage.setItem("vehicleTypesId", vehicleId.toString());
    if(vehicleId == 1){
        //global.vehicleTypesId = 1;
        navigation.navigate('ListParkingPasadia');
    }else if(vehicleId == 2){
        //global.vehicleTypesId = 2;
        navigation.navigate('ListParkingPasadia');
    } 
    
}

useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      getToken();
    });
    return unsubscribe;
  }, [navigation]);



    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface, marginBottom: 20}}>
                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => navigation.goBack(null)}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                     <Text style={{ color: "#005A6D",fontSize:16, fontFamily:"Gotham-Bold", marginLeft:10 }}></Text>
                </TouchableOpacity>
            </View>
            <Spinner visible={spinner}  color={primary600} />
            {/* <View style={{paddingLeft: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10,marginBottom:20 }}>
                <View style={{ marginTop: 15}}>
                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                    {
                        parking !== null ?
                        parking.name
                        :
                        ""
                    }
                    </Text>
                    {
                        parking !== null ?
                            parking.address !== null ? 
                                <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                            :
                            <Text style={styles.text}>No hay dirección disponible</Text>
                        :
                        <Text style={styles.text}>No hay dirección disponible</Text>
                    }
                    
                    <View style={{flexDirection: 'row'}} >
                    {
                        parking !== null &&
                            parking.open == 1 ? 
                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                            :
                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                    }    
                        
                        {
                            parking !== null ?
                                parking.finalHour !== "" && parking.initialHour !== "" ? 
                                    <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                            :
                            <Text style={styles.text}>No hay horario disponible</Text>
                        }
                    </View>
                </View>
                <View style={{alignItems: 'center',justifyContent: 'center',}}>
                    <View style = {styles.lineStyle} />
                </View>
                <View>
                    <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Medium',}}>Tarifa base / Disponibilidad</Text>
                </View>
                <View style={{flexDirection:'row',}}>
                    {
                        parking !== null ?
                            parking.tariff.length !== 0 ?
                            
                                parking.tariff.map( (tarifa) => {
                                    return <View style={{flexDirection:'row',marginTop:10, marginRight:10}} key={tarifa.vehicleType}>
                                                <FontAwesomeIcon size={20} 
                                                icon={tarifa.vehicleType === 1 ? faCar
                                                    :tarifa.vehicleType === 2 ? faMotorcycle :tarifa.vehicleType === 3 && faBicycle } color={primary700} />
                                                <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> ${tarifa.price} / <Text style={{color: surfaceMediumEmphasis}}>24</Text></Text>
                                            </View>
                                    })
                            :
                            <View style={rle={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                            </View>
                    }
                </View>
            </View> */}
            <View style={styles.container}>
            <Text style={{ fontFamily: "Gotham-Bold", color: "#000000",fontSize:18, marginBottom:5,textTransform:'capitalize' }}>Comprar pasadía</Text>
                <Text style={{color: '#000',fontSize: 14,fontFamily: 'Gotham-Light',marginTop:5,marginBottom:10}}>
                    Estaciona hasta 10 veces en todos los parqueaderos habilitados para utilizar esta tarifa,  el tiempo que desees, finalizando a la media  noche.
                </Text>
               
                <View style={{backgroundColor:secondary50,padding:20,borderRadius:15,marginVertical:10}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <View>
                            <Text style={{color: "#002D33",fontSize: 24,fontFamily: 'Gotham-Bold',marginTop:5,textAlign:'left'}}>Automóvil</Text>
                        </View>
                        <View style={{alignContent:'flex-end',alignItems:'flex-end',justifyContent:'flex-end'}}>
                            <Text style={{color: "#002D33",fontSize: 20,fontFamily: 'Gotham-Bold',marginTop:5,textAlign:'right',textAlignVertical: "center",justifyContent:'flex-end',}}>$30.000</Text>
                        </View>
                    </View>
                    <Text style={{color: surfaceHighEmphasis,fontSize: 15,fontFamily: 'Gotham-Light',marginTop:5}}>
                        Ahorras X valor en relación a la tarifa base
                    </Text>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',marginTop:5}}>
                            Consulta los términos y condiciones
                        </Text>
                        <TouchableOpacity style={{marginTop:5, flexDirection:'row', justifyContent:'space-between'}} onPress={() => goToListParking(1)}>
                           
                        </TouchableOpacity> 
                    </View>

                    <View style={{flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => goToListParking(1)}>
                            <View style={{marginTop: 20}}>
                                <Text style={{color: '#519B00',fontSize: 12,fontFamily: 'Gotham-Bold'}}>VER PARQUEADEROS</Text>
                            </View>   
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.button1} onPress={() => navigation.navigate('ListVehiclesPasadia')}>
                            <Text style={styles.titleButton1} >COMPRAR</Text>
                        </TouchableOpacity>
                    </View> 

                </View>
                <View style={{backgroundColor:primary50,padding:20,borderRadius:15,marginVertical:10}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <View>
                            <Text style={{color: "#002D33",fontSize: 24,fontFamily: 'Gotham-Bold',marginTop:5,textAlign:'left'}}>Moto</Text>
                        </View>
                        <View style={{alignContent:'flex-end',alignItems:'flex-end',justifyContent:'flex-end'}}>
                            <Text style={{color: "#002D33",fontSize: 20,fontFamily: 'Gotham-Bold',marginTop:5,textAlign:'right',textAlignVertical: "center",justifyContent:'flex-end',}}>$15.000</Text>
                        </View>
                    </View>
                    <Text style={{color: surfaceHighEmphasis,fontSize: 15,fontFamily: 'Gotham-Light',marginTop:5}}>
                        Ahorras X valor en relación a la tarifa base
                    </Text>

                    
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',marginTop:5}}>
                            Consulta los términos y condiciones
                        </Text>
                         <TouchableOpacity style={{marginTop:5, flexDirection:'row', justifyContent:'space-between'}} onPress={() => goToListParking(2)}>
                           
                        </TouchableOpacity> 
                    </View>
                    <View style={{flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => goToListParking(2)}>
                            <View style={{marginTop: 20}}>
                                <Text style={{color: '#519B00',fontSize: 12,fontFamily: 'Gotham-Bold'}}>VER PARQUEADEROS</Text>
                            </View>   
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.button1} onPress={() => navigation.navigate('ListVehiclesMoto')}>
                            <Text style={styles.titleButton1} >COMPRAR</Text>
                        </TouchableOpacity>
                    </View> 
                   
                </View>

                <View>
                    <Text style={{ fontFamily: "Gotham-Bold", color: "#000000",fontSize:18, marginBottom:5, marginLeft:10, textTransform:'capitalize' }}>Pasadias disponibles</Text>
                </View>
                
                <TouchableOpacity onPress={() => navigation.navigate('ListPasadia')}>
                <View  
                    style={{ backgroundColor: surface,
                             borderRadius: 5,
                             width:width-40,
                             marginLeft: 8,
                             marginRight: 1,
                             shadowColor: "#000",
                             shadowOffset: {
                             width: 0,
                             height: 1,
                             },
                             shadowOpacity: 0.49,
                             shadowRadius: 4.65,
                             elevation: 3,
                             marginBottom:10,
                             marginTop:10,
                             paddingTop:5,
                    }}
                    > 
                    <View>
                        <Text style={{ fontFamily: "Gotham-Bold", color: "#002D33",fontSize:16,marginTop:10, marginLeft:20 }}>
                            Seleccionar pasadía                                           
                        </Text>
                        <Text style={{ fontFamily: "Gotham-Bold", color: "rgba(0, 45, 51, 0.8)",fontSize:16,marginTop:3, marginLeft:20, marginBottom:10 }}>
                            Tienes {cant} pasadias disponibles 
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>

                
                                
                <Dialog.Container visible={dialogTermsVisible}>
                    <Text style={styles.title}>Terminos y condiciones</Text>
                    <Text style={styles.text2}>Tortor bibendum pretium lacinia at risus. Suspendisse volutpat, neque felis, dui, sagittis sapien commodo vulputate. Quis eget tortor amet ipsum, morbi mollis semper. Commodo leo imperdiet fermentum lobortis suspendisse. Dictumst eget in sed nibh. Eu volutpat ut vel adipiscing erat gravida maecenas ut vitae. Nunc sodales arcu magna in libero in. Ligula eget integer sed diam elit tristique. </Text>
                    <View style={{marginBottom:10}}>
                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setDialogTermsVisible(false)}>
                            <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>ACEPTAR Y CONTINUAR</Text>
                        </TouchableOpacity>
                    </View>
                </Dialog.Container>
                
            </View>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingHorizontal:20,
      paddingBottom:20,
      paddingTop:10,
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: "#F2FAE0",
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 10,
        borderRadius: 12,
        width: width-230,
        marginTop:10,
        marginLeft: 20,
        alignItems: "center",
      },
      button4: {
        backgroundColor: secondary,
        padding: 5,
        borderRadius: 12,
        width: width-250,
        marginTop:10,
        alignItems: "center",
      },
      button5: {
        backgroundColor: secondary,
        padding: 5,
        borderRadius: 12,
        width: width-150,
        marginTop:10,
        alignItems: "center",
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
  })