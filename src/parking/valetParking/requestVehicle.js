import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions,Alert,Modal,LogBox,Image,TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faDirections,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import RadioButtonRN from 'radio-buttons-react-native';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { ProgressBar, Colors, Card } from 'react-native-paper';
import CountDown from 'react-native-countdown-component';
import { RadioButton } from 'react-native-paper';
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,errorColor,primary500,
    colorPrimarySelect,onSurfaceOverlay8,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, surfaseDisabled, surfaceHighEmphasis, errorLight, primary50 } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions.']);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function RequestVehicle({navigation}) {
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [licensePlate, setLicensePlate] = useState(null);
    const sheetRef = React.useRef(null);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [vehicles, setVehicles] = useState(false);
    const [token, setToken] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [dateMax, setDateMax] = useState(null);
    const [modalCancel, setModalCancel] = useState(false);
    const [progress, setProgress] = useState(null);
    const [data, setData] = useState( [
        {
          label: 'data 1',
          accessibilityLabel: 'Your label'
         },
         {
          label: 'data 2',
          accessibilityLabel: 'Your label'
         }
        ]);
    const [checked, setChecked] = useState(false);
    const [pay, setPay] = useState(true);
    const [payOk, setPayOk] = useState(false);
    const [modalArrived, setModalArrived] = useState(true);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
      };

    const getDataStorage = async () => {
        try {
          const value1 = await AsyncStorage.getItem("vehicleTypeId");
          const value2 = await AsyncStorage.getItem("licensePlate");
          if (value1 !== null && value2 !== null) {
            var id = parseInt(value1);  
            setTypeVehicleId(id);
            setLicensePlate(value2);
            AsyncStorage.removeItem("vehicleTypeId");
            AsyncStorage.removeItem("licensePlate");
          }
        } catch (error) {
            console.log(error);
         }
      };

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
//Metodo de fecha mayor
    const dateMaxCalc = () => {
        const dateString = moment(String(new Date)).add(2,'M');
        const date = new Date(dateString);
        setDateMax(date);
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getDataStorage();
          dateMaxCalc();
          getParkingInfo();
          getToken();
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('ActiveValet')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{paddingHorizontal: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10 }}>
                    <Text style={{fontFamily:'Gotham-Bold',color:secondary,fontSize:24,textAlign:'center'}}>Vehículo Solicitado</Text>
                    <Text style={{fontFamily:'Gotham-Medium',color:secondary,fontSize:16,marginTop:10}}>Nuestro Valet recibio tu solicitud</Text>
                    <View style={{flexDirection:'row'}}>
                        <MaterialCommunityIcons style={{marginTop:10}} name="alert-circle-outline" size={20} color={primary800} />
                        <Text style={{fontFamily:'Gotham-Light',color:secondary,fontSize:13,marginTop:10}}>Por favor realiza el pago de tu servicio de Valet</Text>
                    </View>
                    <View style={{paddingRight:10,marginVertical:20}}>
                        <ProgressBar progress={progress} color={primary500} />
                    
                    </View>
                    <View style={{flexDirection:'row',backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 2,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,}}>
                        <Image style={{width:40,height:40,borderRadius:50}} width={40} height={40} source={require('../../../assets/valet/ImageAvatar.png')} ></Image>
                        <View style={{marginLeft:20}}>
                        <Text style={{fontFamily:'Gotham-Medium',color:secondary,fontSize:16,textAlign:'center'}}>Rubén Gómez</Text>
                    <Text style={{fontFamily:'Gotham-Light',color:secondary,fontSize:14}}>31201253478</Text>
                        </View>
                    </View>
                </View>
                <View style={{flex: 1, backgroundColor: surface, padding:20,paddingTop:10,marginTop:16}}>
                <Text style={{fontFamily:'Gotham-Bold',color:secondary,paddingTop:10,fontSize:24}}>Detalles</Text>
                <View
                    style={{
                        backgroundColor: surface,
                        borderRadius: 10,
                        width:width-38,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 2,
                        marginBottom:10,
                        marginTop:10,
                        padding:15,
                    }}
                    >
                        <View style={{flexDirection: 'row'}}>
                            <View >
                                <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                    {
                                        parking !== null ?
                                        parking.name
                                        :
                                        ""
                                    }
                                </Text>
                                {
                                    parking !== null ?
                                        parking.address !== null ? 
                                            <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    :
                                    <Text style={styles.text}>No hay dirección disponible</Text>
                                }
                                <View style={{flexDirection: 'row'}} >
                                    {
                                        parking !== null &&
                                            parking.open == 1 ? 
                                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                            :
                                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                    }    
                                        
                                    {
                                        parking !== null ?
                                            parking.finalHour !== "" && parking.initialHour !== "" ? 
                                                <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay horario disponible</Text>
                                    }
                                </View>
                                <View style={{flexDirection:'row',}}>
                                    {
                                        parking !== null ?
                                            parking.tariff.length !== 0 ?
                                            
                                            <View style={{ marginRight:30}}>
                                                <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Light',}}> ${parking.tariff[0].price}</Text>
                                            </View>
                                            :
                                            <View style={{ marginRight:30}}>
                                                <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Light',}}> No hay tarifas disponibles</Text>
                                            </View>
                                        :
                                            <View style={{ marginRight:30}}>
                                                <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Light',}}> No hay tarifas disponibles</Text>
                                            </View>
                                    }
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                        <Text style={styles.text}>Fecha de inicio</Text>
                            <Text style={styles.textBold}>Julio 20,2021</Text>
                        </View>
                        <View style={{marginLeft:50}}>
                            <Text style={styles.text}>Hora de inicio</Text>
                            <Text style={styles.textBold}>00:00:00</Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                        <Text style={styles.text}>Tiempo Transcurrido</Text>
                            <Text style={styles.textBold}>00:00:00</Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View >
                            <Text style={styles.text}>Costo</Text>
                            <Text style={styles.textBold}>$00.00</Text>
                        </View>
                    </View>
                </View>
                <View style={{flex: 1, backgroundColor: surface, paddingHorizontal:20,paddingVertical:10,marginTop:16,}}>
                    <TouchableOpacity onPress={() => navigation.navigate('CheckListValet')}>
                        <View
                            style={{
                                backgroundColor: surface,
                                borderRadius: 10,
                                width:width-38,
                                marginLeft: 0,
                                marginRight: 1,
                                shadowColor: "#000",
                                shadowOffset: {
                                width: 0,
                                height: 1,
                                },
                                shadowOpacity: 0.49,
                                shadowRadius: 4.65,
                                elevation: 2,
                                marginBottom:10,
                                marginTop:10,
                                padding:10,
                            }}
                            >
                            <View style={{flexDirection: 'row'}}>
                                <MaterialCommunityIcons name="format-list-checks" size={20} color={secondary} />
                                <View style={{marginLeft:30}}>
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Checklist del Vehículo</Text>
                                </View>
                                <MaterialIcons name="navigate-next" size={24} color={secondary} style={{marginLeft:'auto'}} />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <Text style={{fontFamily:'Gotham-Bold',color:secondary,paddingHorizontal:20,paddingTop:10,fontSize:18}}>Medio de pago</Text>
                <View style={{flex: 1, backgroundColor: surface, paddingHorizontal:20,paddingVertical:10,marginTop:16,}}>
                    {
                        payOk ?
                            pay ? 
                            <View style={{backgroundColor:primary50,padding:10 }}> 
                                <View style={{flexDirection:'row',}}>
                                    <View style={{justifyContent:'center'}}>
                                        <MaterialCommunityIcons name="check-circle" size={24} color={primary500} />
                                    </View>
                                    <View style={{marginLeft:20}}>
                                        <Text style={{textAlign:'left', color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Pago exitoso</Text>
                                        <Text style={{textAlign:'left', color: surfaceMediumEmphasis,fontSize: 13,fontFamily:'Gotham-Light',}}>VISA *2345</Text>
                                    </View>
                                </View>
                            </View>
                            :
                            <View style={{backgroundColor:errorLight,padding:10 }}> 
                                <View style={{flexDirection:'row',}}>
                                    <View style={{justifyContent:'center'}}>
                                        <MaterialCommunityIcons name="close-circle" size={24} color={errorColor} />
                                    </View>
                                    <View style={{marginLeft:20}}>
                                        <Text style={{textAlign:'left', color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Pago rechazado</Text>
                                        <Text style={{textAlign:'left', color: surfaceMediumEmphasis,fontSize: 13,fontFamily:'Gotham-Light',}}>Selecciona o añade otro medio de pago</Text>
                                    </View>
                                </View>
                            </View>

                        :
                        <View></View>
                    }

                    <TouchableOpacity onPress={() => navigation.navigate('PaymentMethods')}>
                        <View
                            style={{
                                backgroundColor: surface,
                                borderRadius: 10,
                                width:width-38,
                                marginLeft: 0,
                                marginRight: 1,
                                shadowColor: "#000",
                                shadowOffset: {
                                width: 0,
                                height: 1,
                                },
                                shadowOpacity: 0.49,
                                shadowRadius: 4.65,
                                elevation: 2,
                                marginBottom:10,
                                marginTop:10,
                                padding:10,
                            }}
                            >
                            <View style={{flexDirection: 'row'}}>
                                <Ionicons name="card-outline" size={20} color={secondary}/>
                                <View style={{marginLeft:30}}>
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Medios de pago electrónicos</Text>
                                </View>
                                <MaterialIcons name="navigate-next" size={24} color={secondary} style={{marginLeft:'auto'}} />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 2,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        >
                        <View style={{flexDirection: 'row'}}>
                            <RadioButton
                                value={true}
                                status={ checked ? 'checked' : 'unchecked' }
                                onPress={() => setChecked(!checked)}
                                uncheckedColor={surfaceMediumEmphasis}
                                color={primary600}
                            />
                            <View style={{marginLeft:30,marginTop:5}}>
                                <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',justifyContent:'center',textAlign:'center'}}>Pago en eféctivo</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{marginVertical:5}}>
                        <TouchableOpacity style={{padding: 14,borderRadius: 10,}} onPress={() => setModalCancel(true)}>
                            <Text style={{color: surfaceMediumEmphasis, fontSize: 15, fontFamily:'Gotham-Medium',textAlignVertical: "center",textAlign: "center",}}>CANCELAR SOLICITUD</Text>
                        </TouchableOpacity>
                    </View>
                        
                </View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalCancel}
                >
                    <TouchableWithoutFeedback onPress={() => setModalCancel(!modalCancel)}>
                        <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                    <Text style={styles.title}>¿Desea cancelar la solicitud?</Text>
                                    <Text style={styles.text2}>Tenga en cuenta que..</Text>
                                    <View style={{marginBottom:5, flexDirection:'row',alignContent:'center',alignItems:'center'}}>
                                    <TouchableOpacity style={{paddingVertical: 14,paddingHorizontal:70,borderRadius: 10,}} onPress={() => setModalCancel(!modalCancel)}>
                                            <Text style={{color: surfaceMediumEmphasis, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>NO</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{backgroundColor: secondary,paddingVertical: 14,paddingHorizontal:70,borderRadius: 10,}} onPress={() => navigation.goBack(null)}>
                                            <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>SI</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalArrived}
                >
                    <TouchableWithoutFeedback onPress={() => setModalArrived(!modalArrived)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>¡Ya puedes recoger tu vehículo!</Text>
                                <Text style={styles.text2}>Para recibir tu vehiculo, necesitas generar el código QR.</Text>
                                <View style={{marginBottom:5,alignContent:'center',alignItems:'center',justifyContent:'center'}}>
                                    <TouchableOpacity onPress={() => navigation.navigate('QRVehicle')} style={{alignItems: "center",alignContent:'center',justifyContent:'center',backgroundColor: secondary,padding: 14, borderRadius: 10, width: width-30,flexDirection:'row'}} >
                                        <FontAwesome style={{marginRight:10}} name="qrcode" size={24} color={colorPrimaryLigth} />
                                        <Text style={styles.titleButton1}>ACTIVAR CÓDIGO QR</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{paddingVertical: 14,paddingHorizontal:70,borderRadius: 10,}} onPress={() => setModalArrived(!modalArrived)}>
                                        <Text style={{color: surfaceMediumEmphasis, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR SOLICITUD</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </ScrollView>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      marginBottom:height-450
    },
    container2: {
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:10,
        paddingBottom:5,
        paddingTop:10,
        elevation:5,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: secondary,
        fontSize: 24,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 12,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: surfaceHighEmphasis,
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
        textAlign:'center',
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 15,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    button2: {
        backgroundColor: surface,
        padding: 10,
        borderRadius: 10,
        width: width-60,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
        flexDirection:'row',
        alignContent:'center',
        justifyContent:'center'
    },
    titleButton2: {
        color: secondary,
        fontFamily:'Gotham-Medium',
        fontSize: 13,
        textAlign:'center',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    textBoldUp: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'uppercase'
    },
  })