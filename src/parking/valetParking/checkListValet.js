import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions,Alert,Modal,LogBox,Image} from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Carousel,{Pagination} from "react-native-snap-carousel";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,errorColor, errorLight,
    colorPrimarySelect,onSurfaceOverlay8,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, surfaseDisabled, surfaceHighEmphasis, primary50 } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions.']);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function CheckListValet({navigation }) {
    const carouselRef = React.useRef(null);
    const [spinner,setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [activeIndex, setActiveIndex] = useState();
    const [viewsCar,setViewsCar] = useState(
        [
            {
                id:1,
                name:'Vista Frontal',
                img: require('../../../assets/valet/frontal.png'),
                state:'Observación',
                description:'Balazo en la puerta',
                stateId: 0,
            },
            {
                id:2,
                name:'Vista Trasera',
                img: require('../../../assets/valet/trasera.png'),
                state:'Observación',
                description:'Balazo en la puerta',
                stateId: 0,
            },
            {
                id:3,
                name:'Vista Superior',
                img: require('../../../assets/valet/superior.png'),
                state:'Buen estado',
                description:'',
                stateId: 1,
            },
            {
                id:4,
                name:'Vista Lateral Derecha',
                img: require('../../../assets/valet/derecha.png'),
                state:'Observación',
                description:'Balazo en la puerta',
                stateId: 0,
            },
            {
                id:5,
                name:'Vista Lateral Izquierda',
                img: require('../../../assets/valet/Izquierda.png'),
                state:'Observación',
                description:'Balazo en la puerta',
                stateId: 0,
            },
        ],
    );


    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
          }
        } catch (error) { }
      };
      /////////item de carrusel de vistas del vehiculo///
    const _renderItem = ({ item }) =>{
        
        return (
            <View
              style={{
                backgroundColor: surface,
                borderRadius: 15,
                marginLeft: 15,
                marginRight: 1,
                shadowColor: "#000",
                shadowOffset: {
                width: 0,
                height: 1,
                },
                shadowOpacity: 1.69,
                shadowRadius: 5.65,
                elevation: 3,
                marginBottom:5,
              }}
            >
              <View style={{ alignItems:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:18, textAlign:'center', marginTop:30}}>
                    {item.name}
                    </Text>
                    <Image source={item.img} style={{width:200,height:63,marginVertical:20}} width={266} height={84} ></Image>
                    <View style={[{marginTop:10,width:width-95,borderBottomLeftRadius:15,borderBottomRightRadius:15},item.stateId === 0 ? {backgroundColor:errorLight} : {backgroundColor:primary50} ]}>
                        <View style={{alignItems:'center',alignContent:'center'}}>
                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14, textAlign:'center', marginTop:20}}>{item.state}</Text>
                            <Text style={{ fontFamily: "Gotham-Light", color: surfaceMediumEmphasis,fontSize:14, textAlign:'center', marginVertical:20}}>{item.description}</Text>
                        </View>
                    </View>
                </View>
              </View>
            </View>
        );
      }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}}>
                <View style={{ padding:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.goBack(null)}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <Text style={{fontFamily:'Gotham-Bold',color:secondary,padding:20,fontSize:18}}>Checklist del vehículo</Text>
                <View style={{flex: 1, backgroundColor: surface, padding:20,paddingTop:10,}}>
                    <View style={{marginVertical:20,flexDirection:'row',}}>
                        <View>
                            <Text style={styles.text}>Placas del vehículo</Text>
                            <Text style={styles.textBoldUp}>SDF 123</Text>
                        </View>
                        <View style={{marginLeft:40}}>
                            <Text style={styles.text}>Desprendible de llaves</Text>
                            <Text style={styles.textBold}>10000001000</Text>
                        </View>
                    </View>
                    <Text style={{color: secondary,fontSize: 10,fontFamily: 'Gotham-Light',marginBottom:10}}>ESTADO DEL VEHICULO</Text>
                    <View
                    style={{
                        flex: 1,
                        flexDirection: "row",
                        justifyContent: "center",
                        marginTop:10,
                    }}
                    >
                        <Carousel
                            ref={carouselRef}
                            layout={"default"}
                            data={viewsCar}
                            sliderWidth={width-120}
                            itemWidth={width-80}
                            renderItem={_renderItem}
                            onSnapToItem={(index) => setActiveIndex(index)}
                        />
                        
                    </View>
                    {/* <Pagination
                        dotsLength={viewsCar.length}
                        activeDotIndex={activeIndex}
                        containerStyle={{ backgroundColor: 'transparent' }}
                        dotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginHorizontal: 8,
                            backgroundColor: secondary
                        }}
                        inactiveDotStyle={{
                            // Define styles for inactive dots here
                        }}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                        tappableDots={true}
                        carouselRef={carouselRef}
                        /> */}
                </View>
                <Text style={{fontFamily:'Gotham-Light',color:secondary,padding:20,fontSize:12}}>Elementos ingresados</Text>
                <View style={styles.container}>
                    <View style={{flexDirection: 'row',padding:20}}>
                        <MaterialIcons  name="check" size={24} color={secondary} />
                        <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',marginLeft:20}}>Computador Portatil</Text>        
                    </View>
                    <View style={{flexDirection: 'row',padding:20}}>
                        <MaterialIcons  name="check" size={24} color={secondary} />
                        <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',marginLeft:20}}>Ropa</Text>        
                    </View> 
                    <View style={{flexDirection: 'row',padding:20}}>
                        <MaterialIcons  name="check" size={24} color={secondary} />
                        <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',marginLeft:20}}>Maletin</Text>        
                    </View>
                </View>
            </ScrollView>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingTop:20,
      paddingHorizontal:20,
      paddingBottom:100,
    },
    container2: {
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:10,
        paddingBottom:5,
        paddingTop:10,
        elevation:5,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: secondary,
        fontSize: 17,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 12,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: surfaceHighEmphasis,
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
        textAlign:'center',
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 15,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    button2: {
        backgroundColor: surface,
        padding: 10,
        borderRadius: 10,
        width: width-60,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
        flexDirection:'row',
        alignContent:'center',
        justifyContent:'center'
    },
    titleButton2: {
        color: secondary,
        fontFamily:'Gotham-Medium',
        fontSize: 13,
        textAlign:'center',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    textBoldUp: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'uppercase'
    },
  })