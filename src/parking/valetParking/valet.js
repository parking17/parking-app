import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions,Alert,Modal,LogBox,Image} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faDirections,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,onSurfaceOverlay8,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, surfaseDisabled } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions.']);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function Valet({route, navigation }) {
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [licensePlate, setLicensePlate] = useState(null);
    const sheetRef = React.useRef(null);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [vehicles, setVehicles] = useState(false);
    const [token, setToken] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [dateMax, setDateMax] = useState(null);


    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
      };

    const getDataStorage = async () => {
        try {
          const value1 = await AsyncStorage.getItem("vehicleTypeId");
          const value2 = await AsyncStorage.getItem("licensePlate");
          if (value1 !== null && value2 !== null) {
            var id = parseInt(value1);  
            setTypeVehicleId(id);
            setLicensePlate(value2);
            AsyncStorage.removeItem("vehicleTypeId");
            AsyncStorage.removeItem("licensePlate");
          }
        } catch (error) {
            console.log(error);
         }
      };

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            navigation.navigate('BarNavigationRegister');
            console.log(error.response.data.ResponseMessage);
          })
    }

    ///////////verificar si tiene vehiculos registrados
    const getVehicles = (token) => {
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}customer/app/list/vehicle`,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                if(response.data.ResponseMessage.vehicles.length === 0){
                    setVehicles(false);
                }else{
                    setVehicles(true);
                }
            }else{
              Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
//Metodo de fecha mayor
    const dateMaxCalc = () => {
        const dateString = moment(String(new Date)).add(2,'M');
        const date = new Date(dateString);
        setDateMax(date);
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getDataStorage();
          dateMaxCalc();
          getParkingInfo();
          getToken();
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('DetailParking')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                    <Text style={{fontFamily:'Gotham-Bold',color:secondary,marginLeft:10,fontSize:16}}>Valet Parking</Text>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{paddingLeft: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10,marginBottom:20 }}>
                    <View style={{ marginTop: 15}}>
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                        {
                            parking !== null ?
                            parking.name
                            :
                            ""
                        }
                        </Text>
                        {
                            parking !== null ?
                                parking.address !== null ? 
                                    <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                :
                                <Text style={styles.text}>No hay dirección disponible</Text>
                            :
                            <Text style={styles.text}>No hay dirección disponible</Text>
                        }
                        
                        <View style={{flexDirection: 'row'}} >
                        {
                            parking !== null &&
                                parking.open == 1 ? 
                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                :
                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                        }    
                            
                            {
                                parking !== null ?
                                    parking.finalHour !== "" && parking.initialHour !== "" ? 
                                        <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                    :
                                    <Text style={styles.text}>No hay horario disponible</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                            }
                        </View>
                    </View>
                    <View style={{alignItems: 'center',justifyContent: 'center',}}>
                        <View style = {styles.lineStyle} />
                    </View>
                    <View>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Medium',}}>Tarifa</Text>
                    </View>
                    <View style={{flexDirection:'row',}}>
                        {
                            parking !== null ?
                                parking.tariff.length !== 0 ?
                                
                                <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                    <FontAwesomeIcon size={20} 
                                        icon={faCar} color={primary700} />
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> ${parking.tariff[0].price}</Text>
                                </View>
                                :
                                <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                                </View>
                            :
                                <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                                </View>
                        }
                    </View>
                </View>
                <View style={styles.container}>
                    <Text style={{color: secondary,fontSize: 18,fontFamily: 'Gotham-Bold',}}>Bienvenido a Valet Parking</Text>
                    <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Light',marginTop:10}}>Para comenzar, acercate al punto de servicio y solicita allí tu Valet</Text>
                    <View style={{}}>
                        <Image
                        source={require("../../../assets/reserve/ilustracion_valet.png")}
                        style={{ marginVertical:20 }}></Image>
                    </View>
                    <View style={{marginTop:10,alignContent:'center',alignItems:'center'}}>
                        <TouchableOpacity style={styles.button2} onPress={() => navigation.navigate('Login')}>
                            <FontAwesomeIcon size={20} icon={faDirections} color={secondary}  style={{marginRight:5,}}/>
                            <Text style={styles.titleButton2}>INDICACIONES</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
    },
    container2: {
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:5,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    button2: {
        backgroundColor: surface,
        padding: 10,
        borderRadius: 10,
        width: width-60,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
        flexDirection:'row',
        alignContent:'center',
        justifyContent:'center'
    },
    titleButton2: {
        color: secondary,
        fontFamily:'Gotham-Medium',
        fontSize: 13,
        textAlign:'center',
    },
  })