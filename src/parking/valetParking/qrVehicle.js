import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions,Alert,Modal,LogBox,Image,TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faDirections,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import RadioButtonRN from 'radio-buttons-react-native';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { ProgressBar, Colors, Card } from 'react-native-paper';
import CountDown from 'react-native-countdown-component';
import Stars from 'react-native-stars';
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,errorColor,primary500,
    colorPrimarySelect,onSurfaceOverlay8,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, surfaseDisabled, surfaceHighEmphasis, errorLight, primary50, primary400 } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions.']);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function QRVehicle ({navigation}) {
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [licensePlate, setLicensePlate] = useState(null);
    const sheetRef = React.useRef(null);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [vehicles, setVehicles] = useState(false);
    const [token, setToken] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [dateMax, setDateMax] = useState(null);
    const [modalCancel, setModalCancel] = useState(false);
    const [progress, setProgress] = useState(null);
    const [data, setData] = useState( [
        {
          label: 'data 1',
          accessibilityLabel: 'Your label'
         },
         {
          label: 'data 2',
          accessibilityLabel: 'Your label'
         }
        ]);
    const [checked, setChecked] = useState(false);
    const [pay, setPay] = useState(true);
    const [payOk, setPayOk] = useState(false);
    const [modalQualification, setModalQualification] = useState(false);
    const [qualification, setQualification] = useState(null);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
      };

    const getDataStorage = async () => {
        try {
          const value1 = await AsyncStorage.getItem("vehicleTypeId");
          const value2 = await AsyncStorage.getItem("licensePlate");
          if (value1 !== null && value2 !== null) {
            var id = parseInt(value1);  
            setTypeVehicleId(id);
            setLicensePlate(value2);
            AsyncStorage.removeItem("vehicleTypeId");
            AsyncStorage.removeItem("licensePlate");
          }
        } catch (error) {
            console.log(error);
         }
      };

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
//Metodo de fecha mayor
    const dateMaxCalc = () => {
        const dateString = moment(String(new Date)).add(2,'M');
        const date = new Date(dateString);
        setDateMax(date);
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getDataStorage();
          dateMaxCalc();
          getParkingInfo();
          getToken();
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View style={styles.container}>
            <ScrollView style={{backgroundColor:surface}}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('DetailParking')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{paddingHorizontal: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10,marginBottom:height-430}}>
                    <Text style={{fontFamily:'Gotham-Bold',color:secondary,fontSize:24,textAlign:'center'}}>Recibe tu vehículo</Text>
                    <View style={{alignItems:'center',marginTop:5}}>
                        <Image
                        source={require("../../../assets/valet/qrImg.png")}
                        style={{ marginVertical:20 }}></Image>
                    </View>
                    <Text style={{fontFamily:'Gotham-Light',textAlign:'center',color:secondary,fontSize:16,marginVertical:10}}>Para recibir tu vehículo presenta este codigo al operario Valet</Text> 
                    <Text style={{fontFamily:'Gotham-Light',color:secondary,fontSize:10,marginVertical:20}}>ESTADO DEL VEHÍCULO</Text>
                    <TouchableOpacity onPress={() => navigation.navigate('VehicleImperfections')}>
                        <View
                            style={{
                                backgroundColor: surface,
                                borderRadius: 10,
                                width:width-38,
                                marginLeft: 0,
                                marginRight: 1,
                                shadowColor: "#000",
                                shadowOffset: {
                                width: 0,
                                height: 1,
                                },
                                shadowOpacity: 0.49,
                                shadowRadius: 4.65,
                                elevation: 2,
                                marginBottom:10,
                                marginTop:10,
                                padding:10,
                            }}
                            >
                            <View style={{flexDirection: 'row'}}>
                                <View style={{justifyContent:'center'}}>
                                  <MaterialCommunityIcons name="alert-circle-outline" size={24} color={errorColor} />
                                </View>
                                <View style={{marginLeft:30}}>
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Con imperfecciones</Text>
                                    <Text style={{color: secondary,fontSize: 14,fontFamily:'Gotham-Light',}}>Se registran algunos daños</Text>
                                </View>
                                <View style={{justifyContent:'center',marginLeft:'auto'}}>
                                  <Text style={{color: primary800,fontSize: 14,fontFamily:'Gotham-Bold',alignContent:'center'}}>VER</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.container2}>
                  <View style={{marginVertical:10,alignItems:'center',alignContent:'center'}} >
                      <TouchableOpacity style={styles.button1} onPress={() => setModalQualification(true)}>
                              <Text style={styles.titleButton1}>FINALIZAR VALET</Text>
                      </TouchableOpacity> 
                  </View>
                </View>
            </ScrollView>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalQualification}
            >
              <TouchableWithoutFeedback onPress={() => setModalQualification(!modalQualification)}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.title}>Califica nuestro servicio</Text>
                        <Text style={styles.text2}>Eget enim purus egestas scelerisque suspendisse imperdiet vitae, non turpis.</Text>
                        <View style={{alignItems:'center',marginBottom:20}}>
                          <Stars
                            default={3.5}
                            count={5}
                            starSize={70}
                            fullStar={<MaterialIcons name="star" size={24} color={primary400} />}
                            emptyStar={<MaterialIcons name="star-border" size={24} color={surfaceHighEmphasis}/>}
                            halfStar={<MaterialIcons name="star-half" size={24} color={primary400} />}
                            update={(val) => setQualification(val)}
                          />
                        </View>
                        <View style={{marginBottom:10}}>
                            <TouchableOpacity style={styles.button1} onPress={() => navigation.navigate('ValetCompleted')}>
                                <Text style={styles.titleButton1}>ENVÍAR CALIFICACIÓN</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{padding:14}} onPress={() => setModalQualification(!modalQualification)}>
                                <Text style={{fontFamily:'Gotham-Medium',color: surfaceMediumEmphasis,fontSize: 14,textAlign:'center'}}>QUIZÁS DESPÚES</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
              </TouchableWithoutFeedback>
            </Modal>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      alignItems: 'center',
      justifyContent: 'center',
      height:height,
    },
    container2: {
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        backgroundColor: surface,
        paddingHorizontal:10,
        paddingBottom:5,
        paddingTop:10,
        elevation:5,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: secondary,
        fontSize: 22,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 12,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: surfaceHighEmphasis,
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
        textAlign:'center',
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 15,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    button2: {
        backgroundColor: surface,
        padding: 10,
        borderRadius: 10,
        width: width-60,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
        flexDirection:'row',
        alignContent:'center',
        justifyContent:'center'
    },
    titleButton2: {
        color: secondary,
        fontFamily:'Gotham-Medium',
        fontSize: 13,
        textAlign:'center',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    textBoldUp: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'uppercase'
    },
  })