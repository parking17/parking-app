import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions,Alert,Modal,LogBox,Image,TouchableWithoutFeedback} from 'react-native';
import moment from "moment";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { ProgressBar, Colors, Card } from 'react-native-paper';
import CountDown from 'react-native-countdown-component';
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,errorColor,primary500,
    colorPrimarySelect,onSurfaceOverlay8,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, surfaseDisabled, surfaceHighEmphasis } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions.']);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ActiveValet({navigation}) {
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [licensePlate, setLicensePlate] = useState(null);
    const sheetRef = React.useRef(null);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [vehicles, setVehicles] = useState(false);
    const [token, setToken] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [dateMax, setDateMax] = useState(null);
    const [modalCancel, setModalCancel] = useState(false);
    const [progress, setProgress] = useState(null);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
      };

    const getDataStorage = async () => {
        try {
          const value1 = await AsyncStorage.getItem("vehicleTypeId");
          const value2 = await AsyncStorage.getItem("licensePlate");
          if (value1 !== null && value2 !== null) {
            var id = parseInt(value1);  
            setTypeVehicleId(id);
            setLicensePlate(value2);
            AsyncStorage.removeItem("vehicleTypeId");
            AsyncStorage.removeItem("licensePlate");
          }
        } catch (error) {
            console.log(error);
         }
      };

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            navigation.navigate('BarNavigationRegister');
            console.log(error.response.data.ResponseMessage);
          })
    }

    ///////////verificar si tiene vehiculos registrados
    const getVehicles = (token) => {
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        axios.get(`${API_URL}customer/app/list/vehicle`,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                if(response.data.ResponseMessage.vehicles.length === 0){
                    setVehicles(false);
                }else{
                    setVehicles(true);
                }
            }else{
              Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    const goToVehicles = () => {
        if(!vehicles){
            navigation.navigate('NoVehicle');
        }else{
            navigation.navigate('ListVehicles');
        }
    }
//Metodo de fecha mayor
    const dateMaxCalc = () => {
        const dateString = moment(String(new Date)).add(2,'M');
        const date = new Date(dateString);
        setDateMax(date);
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getDataStorage();
          dateMaxCalc();
          getParkingInfo();
          getToken();
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('DetailParking')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{paddingLeft: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10 }}>
                    <Text style={{fontFamily:'Gotham-Bold',color:secondary,fontSize:24,textAlign:'center'}}>Valet Parking Activo</Text>
                    <Text style={{fontFamily:'Gotham-Medium',color:secondary,fontSize:16,marginTop:10}}>Hemos recibido tu vehículo</Text>
                    <View style={{paddingRight:10,marginVertical:20}}>
                        <ProgressBar progress={progress} color={primary500} />
                        <Text style={{fontFamily:'Gotham-Light',color:secondary,paddingTop:10,fontSize:14}}>Tiempo transcurrido:</Text>
                        <View style={{alignContent:'flex-start',justifyContent:'flex-start',marginRight:'auto',paddingLeft:10}}>
                            <CountDown
                                until={80000}
                                style={{fontFamily:'Gotham-Bold'}}
                                digitTxtStyle={{color:secondary,fontFamily:'Gotham-Bold'}}
                                separatorStyle={{color:secondary,fontSize:18}}
                                digitStyle={{backgroundColor:'transparent',marginLeft:0,marginRight:0}}
                                showSeparator={true}
                                timeToShow={['H', 'M', 'S']}
                                timeLabels={{m: null, s: null}}
                                size={14}
                            />
                        </View>
                    </View>
                </View>
                <View style={{flex: 1, backgroundColor: surface, padding:20,paddingTop:10,marginTop:16}}>
                <Text style={{fontFamily:'Gotham-Bold',color:secondary,paddingTop:10,fontSize:24}}>Detalles</Text>
                <View
                    style={{
                        backgroundColor: surface,
                        borderRadius: 10,
                        width:width-38,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 2,
                        marginBottom:10,
                        marginTop:10,
                        padding:15,
                    }}
                    >
                        <View style={{flexDirection: 'row'}}>
                            <View >
                                <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                    {
                                        parking !== null ?
                                        parking.name
                                        :
                                        ""
                                    }
                                </Text>
                                {
                                    parking !== null ?
                                        parking.address !== null ? 
                                            <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    :
                                    <Text style={styles.text}>No hay dirección disponible</Text>
                                }
                                <View style={{flexDirection: 'row'}} >
                                    {
                                        parking !== null &&
                                            parking.open == 1 ? 
                                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                            :
                                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                    }    
                                        
                                    {
                                        parking !== null ?
                                            parking.finalHour !== "" && parking.initialHour !== "" ? 
                                                <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay horario disponible</Text>
                                    }
                                </View>
                                <View style={{flexDirection:'row',}}>
                                    {
                                        parking !== null ?
                                            parking.tariff.length !== 0 ?
                                            
                                            <View style={{ marginRight:30}}>
                                                <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Light',}}> ${parking.tariff[0].price}</Text>
                                            </View>
                                            :
                                            <View style={{ marginRight:30}}>
                                                <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Light',}}> No hay tarifas disponibles</Text>
                                            </View>
                                        :
                                            <View style={{ marginRight:30}}>
                                                <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Light',}}> No hay tarifas disponibles</Text>
                                            </View>
                                    }
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                        <Text style={styles.text}>Fecha de inicio</Text>
                            <Text style={styles.textBold}>Julio 20,2021</Text>
                        </View>
                        <View style={{marginLeft:50}}>
                            <Text style={styles.text}>Hora de inicio</Text>
                            <Text style={styles.textBold}>00:00:00</Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={styles.text}>Nombres</Text>
                            <Text style={styles.textBold}>Freddy</Text>
                        </View>
                        <View style={{marginLeft:100}}>
                            <Text style={styles.text}>Apellidos</Text>
                            <Text style={styles.textBold}>Mercurio</Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                        <Text style={styles.text}>Tipo de documento</Text>
                            <Text style={styles.textBold}>CC</Text>
                        </View>
                        <View style={{marginLeft:40}}>
                            <Text style={styles.text}>Número documento</Text>
                            <Text style={styles.textBold}>10000001000</Text>
                        </View>
                    </View>
                </View>
                <Text style={{fontFamily:'Gotham-Bold',color:secondary,paddingHorizontal:20,paddingTop:10,fontSize:18}}>Información del vehículo</Text>
                <View style={{flex: 1, backgroundColor: surface, paddingBottom:50, paddingHorizontal:20,paddingTop:10,marginTop:16,marginBottom:height-520}}>
                    <View style={{marginVertical:20,flexDirection:'row',}}>
                        <View>
                            <Text style={styles.text}>Placas del vehículo</Text>
                            <Text style={styles.textBoldUp}>SDF 123</Text>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => navigation.navigate('CheckListValet')}>
                        <View
                            style={{
                                backgroundColor: surface,
                                borderRadius: 10,
                                width:width-38,
                                marginLeft: 0,
                                marginRight: 1,
                                shadowColor: "#000",
                                shadowOffset: {
                                width: 0,
                                height: 1,
                                },
                                shadowOpacity: 0.49,
                                shadowRadius: 4.65,
                                elevation: 2,
                                marginBottom:10,
                                marginTop:10,
                                padding:15,
                            }}
                            >
                            <View style={{flexDirection: 'row'}}>
                                <MaterialCommunityIcons name="format-list-checks" size={24} color={secondary} />
                                <View style={{marginLeft:30}}>
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Checklist del Vehículo</Text>
                                </View>
                                <MaterialIcons name="navigate-next" size={24} color={secondary} style={{marginLeft:'auto'}} />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            <View style={styles.container2}>
                <View style={{marginVertical:10,alignItems:'center',alignContent:'center',justifyContent:'center'}} >
                    <TouchableOpacity style={styles.button1} onLongPress={() => navigation.navigate('RequestVehicle')}>
                            <Text style={styles.titleButton1}>SOLICITAR VEHÍCULO</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalCancel}
                >
                    <TouchableWithoutFeedback onPress={() => setModalCancel(!modalCancel)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{marginTop:-10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                    <TouchableOpacity onPress={() => setModalCancel(!modalCancel)}>
                                        <MaterialIcons name="close" size={25} color={secondary} />
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.title}>¿Estas seguro de cancelar el servicio?</Text>
                                <Text style={styles.text2}>Si existen inconsistencias en la informacion enviada, puedes solicitar el servicio de nunevo.</Text>
                                <View style={{marginBottom:5}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setModalCancel(!modalCancel)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR SERVICIO</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      marginBottom:height-450
    },
    container2: {
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:10,
        paddingBottom:5,
        paddingTop:10,
        elevation:5,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: secondary,
        fontSize: 17,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 12,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: surfaceHighEmphasis,
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
        textAlign:'center',
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 15,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    button2: {
        backgroundColor: surface,
        padding: 10,
        borderRadius: 10,
        width: width-60,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
        flexDirection:'row',
        alignContent:'center',
        justifyContent:'center'
    },
    titleButton2: {
        color: secondary,
        fontFamily:'Gotham-Medium',
        fontSize: 13,
        textAlign:'center',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    textBoldUp: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'uppercase'
    },
  })