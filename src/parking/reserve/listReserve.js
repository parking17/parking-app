import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,CheckBox,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../../utils/colorVar';
import { productIdPasadia } from '../../utils/varService';
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ListReserve({navigation}) {
    const [reservas, setReservas] = useState([]);
    const [spinner,setSpinner] = useState(false);
    


    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;

            getReserves(value);
            //selectReserve(value);
          }
        } catch (error) { }
      };
    /* const save = async (reserveId) => {
        try {

          await AsyncStorage.setItem("reserveId", reserveId.toString());
          //await AsyncStorage.setItem("licensePlate", licensePlate);
        } catch (e) {
          // saving error
          console.log('Fallo en guardar storage Reserva');
        }
      }; */

    ///////////verificar si tiene vehiculos registrados

    const selectReserve =  (reserveId, estado, park) =>{

        //await AsyncStorage.setItem("reserveId", reserveId);
        global.idReserva = reserveId;
        global.parkingId = park;
        if( estado === 7  ){
            
            navigation.navigate('SumaryReserve');
        }
        else if (estado === 8){
            
            navigation.navigate('ScheduledReserve');
        }
        else if(estado === 1){
            console.log(global.idReserva);
            navigation.navigate('ActiveReserve');
        }
        else if(estado === 9){
            console.log('ir a reserva extendida');
            navigation.navigate('ExtendedReserve');
        }
        else if(estado === 2){
            navigation.navigate('ReserveCompleted');
        }

    }

    const getReserves = (token) => {
        
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        //console.log(`Archivo config __________ ${JSON.stringify(config)}`);

        axios.get(`${API_URL}customer/app/getResByUser`, config).then(response => {
            
            
            const cod = response.data.ResponseCode;
            
            setSpinner(false);
            if(cod === 0){
                setReservas(response.data.ResponseMessage);
                //navigation.navigate('SumaryPasadia')
            }else{
              Alert.alert("ERROR","No se pudo traer las pasadias");
            } 
          }).catch(error => {
            setSpinner(false);
            console.log(error);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
            console.log(error);
          }) 
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
        });
        return unsubscribe;
    }, [navigation]);
    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('ServiceReserve')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
                <Text style={{fontFamily:'Gotham-Bold',color:secondary,marginLeft:10,fontSize:16}}>Reserva</Text>
            </View>
            <View style={styles.container}>
                <Text style={styles.title2}>Reservas</Text>
                <Text style={styles.text2}>Relaciona la Reserva</Text>
                <View >
                    {
                        reservas.map( (reserva) => {
                            if(reserva.estado === 7 || reserva.estado === 9 || reserva.estado === 8 || reserva.estado === 1){
                                return <TouchableOpacity style={{justifyContent:'center'}} key={reserva.id} onPress={() => selectReserve(reserva.id, reserva.estado, reserva.park)}>
                                        <View
                                        style={{
                                            backgroundColor: surface,
                                            borderRadius: 5,
                                            width:width-40,
                                            marginLeft: 0,
                                            marginRight: 1,
                                            shadowColor: "#000",
                                            shadowOffset: {
                                            width: 0,
                                            height: 1,
                                            },
                                            shadowOpacity: 0.49,
                                            shadowRadius: 4.65,
                                            elevation: 3,
                                            marginBottom:10,
                                            marginTop:10,
                                            paddingTop:5,
                                        }}
                                        >
                                            <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                                                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}>
                                                
                                                    
                                                </Text>
                                                {
                                              
                                                   <FontAwesomeIcon size={30} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:30,marginRight:20}}/>
                                                }
                                                
                                                <View>
                                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                                         Reserva | {reserva.parqueadero}
                                                            
                                                    </Text>
                                                    <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5}}>
                
                                                       {reserva.estado === 7 ? "Pendiente pago" : reserva.estado === 8 ? "Reserva programada" : reserva.estado === 1 ? "Reserva activa": reserva.estado === 9 ? "Reserva extendida" :"" } | {reserva.placa}    
                                                    </Text>
                                                    <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                                      {reserva.fecha}  
                                                    </Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                            }

                                
                        }
                    )
                    }
                    
                </View>
                
            </View>
            <View style={{backgroundColor:surface,paddingBottom:20,paddingTop: 120}}>
                <TouchableOpacity onPress={() => navigation.navigate('ServiceReserve')} >
                    <Text style={{ fontSize: 15,fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center", color:primary800}}>
                        Regresar
                    </Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingLeft:20,
      paddingBottom:20,
      paddingTop:20,
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:30,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
  })

















































































