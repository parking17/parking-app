import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,LogBox,Image,TouchableWithoutFeedback} from 'react-native';
import CountDown from 'react-native-countdown-component';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import moment from "moment";
import 'moment/locale/es';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, primary500,onSurfaceOverlay8 } from '../../utils/colorVar';
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
LogBox.ignoreLogs(['Non-serializable values were found in the navigation state'],["Can't perform a React state update on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function."]);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ActiveReserve({navigation}) {
    const [isSelected, setSelection] = useState(false);
    const sheetRef = React.useRef(null);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [expanded, setExpanded] = useState(true);
    const [hour, setHour] = useState();    
    const [hourFinal, setHourFinal] = useState();
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [plate, setPlate] = useState(null);
    const [duration, setDuration] = useState(null);
    const [price, setPrice] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [totalDuration, setTotalDuration] = useState(null);
    const [progress, setProgress] = useState(null);
    const [date, setDate] = useState(null);
    const [timeElapsed, setTimeElapsed] = useState(null);
    const [dialogReserveVisible, setDialogReserveVisible] = useState(false);
    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
      };

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    //Mostrar acordion
    const _handlePress = () => {
        setExpanded(!expanded);
    }
    //Obtener tiempo trasncurrido
    const getTime = (until) =>{
        setTimeElapsed(until);
    }


    ////Ir tiempo extendido
    const goToTimeExtension = (min) => {
        global.timeElapsed = timeElapsed;
        global.attachedTime = min;
        console.log(global.idReserva)
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.get(`${API_URL}customer/app/reservation/validate/schedule/${global.idReserva}/${min}`,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setDialogReserveVisible(true);
            }else{
              Alert.alert("ERROR","No se en extender el tiempo");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
    }
    const goToExtension = () => {
        setDialogReserveVisible(false);
        navigation.navigate('TimeExtensionReserve');
    }
    //Metodo para traer data de la reserva
    const getDataReserve = () => {
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.get(`${API_URL}customer/app/reservation/get/`+global.idReserva,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                var date = moment(response.data.ResponseMessage.initial_date).format('MMMM DD YYYY');
                var timeI = moment(response.data.ResponseMessage.initial_date).format('HH:mm');
                setHour(timeI);
                var timeF = moment(response.data.ResponseMessage.final_date).format('HH:mm');
                setDuration(response.data.ResponseMessage.duration);
                setHourFinal(timeF);
                setDate(date);
                setPrice(response.data.ResponseMessage.price);
                setTypeVehicleId(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.vehicle_type_id);
                setPlate(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.plate);
            }else{
              Alert.alert("ERROR","No se en traer datos de reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
        
      };
      const getTimeReserve = () =>{
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}customer/app/reservation/time/left/`+global.idReserva,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setTotalDuration(response.data.ResponseMessage);
            }else{
              Alert.alert("ERROR","No se pudo traer el tiempo que queda de la reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
    }
    //Finalizar reserva
    const finishReserve = () => {
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.put(`${API_URL}customer/app/reservation/finish/`+global.idReserva,{},config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                Alert.alert("",response.data.ResponseMessage);
                navigation.navigate('ReserveCompleted');
            }else{
              Alert.alert("ERROR","No se pudo traer el tiempo que queda de la reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
    }


    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getParkingInfo();
          getToken();
          getDataReserve();
          getTimeReserve();
        });
        // ///calcular tiempo restante
        // var date = global.dataVehicle.dateSend;
        // var expirydate = global.dataVehicle.datePlusTime;
        // var diffr = moment.duration(moment(expirydate).diff(moment(date)));

        // var hours = parseInt(diffr.asHours());
        // var minutes = parseInt(diffr.minutes());
        // var seconds = parseInt(diffr.seconds());
        
        // var d = hours * 60 * 60 + minutes * 60 + seconds;
        // setTotalDuration(d);
        // const newDuration = d;
        // var variable = 1;
        // setInterval(()=> {
        //             variable = variable - 1/newDuration;
        //             setProgress(variable);
        //         }, 1000);

        // return unsubscribe;

      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.goBack(null)}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{flex: 1, backgroundColor: surface, padding:20, paddingTop:10,}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',textAlign:'center'}}>Reserva Activa</Text>
                    <View style={{alignItems:'center'}}>
                        <Image
                        source={require("../../../assets/reserve/ilustracion_paseo2.png")}
                        style={{ marginVertical:20 }}></Image>
                    </View>
                    <Text style={{color: secondary,fontSize: 15,fontFamily: 'Gotham-Light',textAlign:'center'}}>Presenta este codigo a la entrada y salida del punto de servicio cuando uses pasadía</Text>
                    <View style={{alignItems:'center'}}>
                        <Image
                        source={require("../../../assets/reserve/qrcode1.png")}
                        style={{ marginVertical:20 }}></Image>
                    </View>    
                    <TouchableOpacity style={{justifyContent:'center'}} >
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:15,
                        }}
                        >
                            <View style={{flexDirection: 'row'}}>
                                <View >
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                        {
                                            parking !== null ?
                                            parking.name
                                            :
                                            ""
                                        }
                                    </Text>
                                    {
                                        parking !== null ?
                                            parking.address !== null ? 
                                                <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                            :
                                            <Text style={styles.text}>No hay dirección disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    }
                                    <View style={{flexDirection: 'row'}} >
                                        {
                                            parking !== null &&
                                                parking.open == 1 ? 
                                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                                :
                                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                        }    
                                            
                                        {
                                            parking !== null ?
                                                parking.finalHour !== "" && parking.initialHour !== "" ? 
                                                    <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                                :
                                                <Text style={styles.text}>No hay horario disponible</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        }
                                    </View>
                                </View>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto',marginTop:20}}>
                                    <MaterialIcons name="assistant-direction" size={24} color={secondary} />    
                                </View>
                            </View>
                        </View> 
                    </TouchableOpacity>
                    <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:15,
                        }}
                        >
                            <Text style={styles.text}>Tu reserva termina en:</Text>
                            <View style={{alignContent:'flex-start',alignItems:'flex-start',fontFamily:'Gotham-Bold'}}>
                                { totalDuration &&
                                    <CountDown
                                        until={totalDuration}
                                        style={{fontFamily:'Gotham-Bold'}}
                                        digitTxtStyle={{color:secondary,fontFamily:'Gotham-Bold'}}
                                        separatorStyle={{color:secondary,fontSize:15}}
                                        digitStyle={{backgroundColor:'transparent'}}
                                        showSeparator={true}
                                        timeLabels={{ h: '', m: '', s: ''}}
                                        timetoShow={['H','M','S']}
                                        size={18}
                                        onChange={getTime}
                                    />
                                }
                                <ProgressBar progress={progress} color={primary500} style={{width:width-80}}/>


                                <View style={styles.containerAccordion}>
                                    <NativeBaseProvider>
                                        <Center flex={1}>
                                            <ScrollView>
                                                <Box m={3} style={{border:0}}>
                                                    <Accordion allowMultiple _text={{border:0}} border={0} onTouchStart={_handlePress}>
                                                        <Accordion.Item _text={{color:primary800}} >
                                                            <Accordion.Summary  _expanded={{backgroundColor:surface,}}>
                                                                <View style={{flexDirection:'row',alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                                                    <Text style={{fontFamily:'Gotham-Medium',fontSize:14,color:primary800}}>EXTENDER TIEMPO</Text>
                                                                    <View style={{marginLeft:20}}>
                                                                        <MaterialIcons name={expanded ? "keyboard-arrow-down":"keyboard-arrow-up"} size={20} color={primary800} />
                                                                    </View> 
                                                                </View>
                                                            </Accordion.Summary>
                                                            <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}} onTouchStart={() => navigation.navigate('ActiveReserve',{attachedTime:30})}>
                                                                <TouchableOpacity style={{backgroundColor:onSurfaceOverlay8,borderRadius:25,width:width-120,padding:10}} onPress={() => goToTimeExtension(30)}>
                                                                    <Text style={{fontFamily:'Gotham-Light',color:secondary,textAlign:'center'}}>30 Mniutos $00.00 -05%</Text>
                                                                </TouchableOpacity>
                                                            </Accordion.Details>
                                                            <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                                <TouchableOpacity style={{backgroundColor:onSurfaceOverlay8,borderRadius:25,width:width-120,padding:10}} onPress={() => goToTimeExtension(60) }>
                                                                    <Text style={{fontFamily:'Gotham-Light',color:secondary,textAlign:'center'}}>60 Mniutos $00.00 -10%</Text>
                                                                </TouchableOpacity>
                                                            </Accordion.Details>
                                                        </Accordion.Item>
                                                    </Accordion>
                                                </Box>
                                            </ScrollView>
                                        </Center>
                                    </NativeBaseProvider>
                                </View>
                            </View>
                        </View>
                </View>
                <View style={{flex: 1, backgroundColor: surface, padding:22, paddingTop:10,marginTop:16}}> 
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',marginTop:10}}>Detalles del servicio</Text> 
                    <View style={{marginTop:20}}>
                        <Text style={styles.text}>Fecha</Text>
                        <Text style={styles.textBold}>{date}</Text>
                    </View>
                    <View style={{marginTop:20}}>
                        <Text style={styles.text}>Tiempo adquirido</Text>
                        <Text style={styles.textBold}>{duration === 1 ? duration+' Hora' : duration && duration+' Horas' }</Text>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={styles.text}>Hora inicial</Text>
                            <Text style={styles.textBold}>
                                {hour}
                            </Text>
                        </View>
                        <View style={{marginLeft:40}}>
                            <Text style={styles.text}>Hora final</Text>
                            <Text style={styles.textBold}>{hourFinal}</Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={styles.text}>Tipo de vehículo</Text>
                            <Text style={styles.textBold}>
                                {typeVehicleId === 1 ? 'Automóvil' : typeVehicleId === 2 ? 'Motocicleta' : typeVehicleId === 3 ? 'Bicicleta':''}
                            </Text>
                        </View>
                        <View style={{marginLeft:40}}>
                            <Text style={styles.text}>Placas del vehículo</Text>
                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',textTransform:'uppercase'}}>{plate}</Text>
                        </View>
                    </View>
                </View>
                <View style={{flex: 1, backgroundColor: surface, padding:22, paddingVertical:10}}> 
                    <TouchableOpacity style={styles.button1} onPress={() => finishReserve()}>
                        <Text style={styles.titleButton1}>FINALIZAR RESERVA</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                animationType="slide"
                transparent={true}
                visible={dialogReserveVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogReserveVisible(!dialogReserveVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <Text style={styles.title2}>Tienes 10 minutos para completar tu extensión</Text>
                                </View>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <Text style={styles.text2}>Descripción opcional.....</Text>
                                </View>
                                <View style={{marginBottom:10,alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <TouchableOpacity style={styles.button1} onPress={goToExtension}>
                                        <Text style={styles.titleButton1}>ACEPTAR Y CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </ScrollView>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-200,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        textAlign: "center",
        justifyContent:'center',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-50,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })