import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions,Alert,Modal,LogBox,Image,StatusBar,Animated, Pressable} from 'react-native';
import moment from "moment";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { TabView, SceneMap } from 'react-native-tab-view';
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
// import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,errorColor,primary500,
    colorPrimarySelect,onSurfaceOverlay8,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, surfaseDisabled, surfaceHighEmphasis, primary50 } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions.']);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;
// const Tab = createMaterialTopTabNavigator();

const FirstRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#ff4081',height:height }]} />
);

const SecondRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#673ab7' }]} />
);

const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
  thirt: FirstRoute,
});
export default function PaymentMethods({navigation }) {
    const sheetRef = React.useRef(null);
    const [spinner,setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [index, setIndex] = useState(0);
    const [routes] = useState([
      { key: 'first', title: 'MEDIOS PARKING' },
      { key: 'second', title: 'PAGO PSE' },
      { key: 'thirt', title: 'TARJETA DE CREDITO' },
    ]);


    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
          }
        } catch (error) { }
      };

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.goBack(null)}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{paddingHorizontal: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10 }}>
                  <Text style={{fontFamily:'Gotham-Bold',color:secondary,fontSize:24}}>Medio de pago</Text>
                  <Text style={{fontFamily:'Gotham-Light',color:secondary,fontSize:16,margin:10}}>Seleccione el método de pago</Text>
                </View>
                <View style={{ marginTop: 10}}>
                  <TabView
                    navigationState={{ index, routes }}
                    renderScene={renderScene}
                    onIndexChange={setIndex}
                    initialLayout={{ width: width }}
                  />
                </View>
            </ScrollView>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      marginBottom:height-450
    },
    container2: {
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:10,
        paddingBottom:5,
        paddingTop:10,
        elevation:5,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: secondary,
        fontSize: 24,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 12,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: surfaceHighEmphasis,
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
        textAlign:'center',
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 15,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    button2: {
        backgroundColor: surface,
        padding: 10,
        borderRadius: 10,
        width: width-60,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
        flexDirection:'row',
        alignContent:'center',
        justifyContent:'center'
    },
    titleButton2: {
        color: secondary,
        fontFamily:'Gotham-Medium',
        fontSize: 13,
        textAlign:'center',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    textBoldUp: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'uppercase'
    },
    scene: {
      flex: 1,
    },
  })