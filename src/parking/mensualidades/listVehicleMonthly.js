import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ListVehicleMonthly ({route, navigation}){
    const [spinner, setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [vehicles, setVehicles] = useState([]);
    const [where, setWhere] = useState('0')

    const   { listMonthlys } = route.params

    //Metodo para obtener el token
    const getToken = async () => {
        try {
            const value = await AsyncStorage.getItem("token");
            if (value !== null) {
                global.token = value;
                setToken(value);
                getVehicles(value);
                const wh = await AsyncStorage.getItem("where");
                setWhere(wh);

                console.log(`Llega listmonthly -- ${JSON.stringify(listMonthlys)}`);
            }
        } catch (error) { }
    };
    
    //Obtener vechiculos de usuario 
    const getVehicles = async (token) => {
        let config = { headers: { Authorization: token } };
        setSpinner(true);
        const vehicleId =  await AsyncStorage.getItem('vehicleId');
        const aux =  await validateVehicle(vehicleId);
        
        axios.get(`${API_URL}customer/app/get/vehicle/type/`+ aux,config).then(response => {
                const cod = response.data.ResponseCode;
                setSpinner(false);
                if(cod === 0){
                    setVehicles(response.data.ResponseMessage);
                }else{
                    Alert.alert("ERROR","No se pudo traer los vehiculos");
                }
            }).catch(error => {
                setSpinner(false);
                Alert.alert("",error.response.data.ResponseMessage);
            })
    }

    //Guardar vehiculo seleccionado y enrutar al resumen de la mensualidad
    const saveVehicle = async (vehicle) =>{

        const vehicle1 = await AsyncStorage.getItem('vehicleId1');

        if (vehicle1) {
            await AsyncStorage.setItem('vehicleId2',vehicle.id.toString());
            await AsyncStorage.setItem('vehicleTypeId2',vehicle.vehicle_type_id.toString());
            await AsyncStorage.setItem('vehiclePlate2',vehicle.plate.toString());

            const data1 = {
                vehicle_id: vehicle.id,
                vehicle_type_id: vehicle.vehicle_type_id,
                plate: vehicle.plate
            };
            listMonthlys[listMonthlys.length - 1].vehicles.push(data1)
        }
        else{
            await AsyncStorage.setItem('vehicleId1',vehicle.id.toString());
            await AsyncStorage.setItem('vehicleTypeId1',vehicle.vehicle_type_id.toString());
            await AsyncStorage.setItem('vehiclePlate1',vehicle.plate.toString());

            const data = [{
                vehicle_id: vehicle.id,
                vehicle_type_id: vehicle.vehicle_type_id,
                plate: vehicle.plate
            }];
            listMonthlys[listMonthlys.length - 1].vehicles = data
        }
        
        navigation.navigate('SumaryMonthly' , {"listMonthlys": listMonthlys});
    }   

    //Validar vehiculo seleccionado
    const validateVehicle =  async (validate) =>{
        const vehicle1 = await AsyncStorage.getItem('vehicleId1');
        const vehicleType1 = await AsyncStorage.getItem('vehicleTypeId1');
        
        if (vehicle1) {
            
            if(validate === '1' || validate === '2'|| validate === '3'){
                return validate
            }
            else if( validate === '4'){
                if(vehicleType1 === '1'){
                    return '2'
                }
                else{
                    return '1'
                }
            }
            else if( validate === '5'){
                if(vehicleType1 === '1'){
                    return '3'
                }
                else{
                    return '1'
                }
            }
            else if( validate === '6'){
                if(vehicleType1 === '2'){
                    return '3'
                }
                else{
                    return '2'
                }
            }

        }
        else{
            return validate;
        }
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
        //getParkingList();
        });
        return unsubscribe;
    }, [navigation]);

    return(

       <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => { where === '0' ?  navigation.navigate('SelectMonthly') : navigation.navigate('SumaryMonthly')}}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>

            <View style={styles.container}>

                <View style={{ marginBottom:15 }}> 
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:24, color:secondary, marginTop:15 }}>Selecciona Vehículo(s)</Text>
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "color: rgba(0, 45, 51, 0.6)"}}>Descripción opcional</Text>
                </View> 
            
            {
                vehicles.map((vehicle) =>{
                    return <TouchableOpacity key={vehicle.id} onPress={() => saveVehicle(vehicle)}>
                     <View style={{
                        backgroundColor: surface,
                        borderRadius: 5,
                        width:width-40,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        paddingTop:5,
                        }}
                    >
                    <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                            {
                                <FontAwesomeIcon size={30} icon={vehicle.vehicle_type_id === 1 ? faCarSide : vehicle.vehicle_type_id === 2 ? faMotorcycle : faBicycle } color={secondary} style={{marginLeft:10,marginTop:20,marginRight:20}}/>
                            }
                                <View>
                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                        {vehicle.vehicle_type_id === 1 ? "Automovil" : vehicle.vehicle_type_id === 2 ? "Moto" : "Bicicleta" }
                                    </Text>
                                    <Text style={{textTransform: 'uppercase', fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5}}>
                                        {vehicle.plate}   
                                    </Text>
                                </View>
                        </View>
                    </View>
                </TouchableOpacity> 


                })
            }
                

                {/*  <TouchableOpacity>
                    <View style={{
                        backgroundColor: surface,
                        borderRadius: 5,
                        width:width-40,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        paddingTop:5,
                        }}
                    >
                    <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                            {
                                <FontAwesomeIcon size={30} icon={faMotorcycle} color={secondary} style={{marginLeft:10,marginTop:30,marginRight:20}}/>
                            }
                                <View>
                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                        Moto 
                                    </Text>
                                    <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5}}>
                                        VBO09E  
                                    </Text>
                                </View>
                        </View>
                    </View>
                </TouchableOpacity> */}
                
                <View style={{ flexDirection: 'column', width:410, height:134, color:"#FFFFFF", padding: 16, }}>
                    <TouchableOpacity onPress={ () => navigation.navigate('AddVehicle')}>
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:14, color:"rgba(0, 45, 51, 0.6)", alignSelf: 'center', marginTop:15, marginRight:30}}>REGISTRAR NUEVO VEHÍCULO</Text>
                    </TouchableOpacity>
                </View>
            </View>

       </ScrollView>



    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        
        paddingTop:10,
    },
    button: {
        width: 378,
        height: 48,
        backgroundColor: '#005A6D',
        borderRadius: 15,
        left: -20,
        marginBottom: 10   
    }
 })  