import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function Mensualidad ({navigation}){

    const [spinner,setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [cont, setCont] = useState('0')
    const [listMonthlys, setListMonthlys] = useState([]);


    const getToken = async () => {
    try {
      AsyncStorage.removeItem("vehicleId1");
      AsyncStorage.removeItem('vehicleTypeId1');
      AsyncStorage.removeItem('vehiclePlate1');
      AsyncStorage.removeItem("vehicleId2");
      AsyncStorage.removeItem('vehicleTypeId2');
      AsyncStorage.removeItem('vehiclePlate2');

      AsyncStorage.removeItem('benName');
      AsyncStorage.removeItem('benEmail');
      AsyncStorage.removeItem('benDocument');
      AsyncStorage.removeItem('benDocumentType');
      AsyncStorage.removeItem('benPhone'); 


      const value = await AsyncStorage.getItem("token");
      if (value !== null) {
        global.token = value;
        setToken(value);
        getContMonthly(value);
      }
    } catch (error) { }
  };

    const getContMonthly = () => {
     setSpinner(true);

            let config = {
                headers: { Authorization: global.token }
            };

     axios.get(`${API_URL}customer/app/getMonthlyCountByUser`, config).then(response => {
         setSpinner(false);

         
          const cod = response.data.ResponseCode;
          if(cod === 0){  
             setCont(response.data.ResponseMessage[0].cantidad)
         }else{
           Alert.alert("ERROR","No se pudo traer el parqueadero");
         } 
       }).catch(error => {
         setSpinner(false);
         console.log(error);
         Alert.alert("",error.response.data.ResponseMessage);
         navigation.navigate('BarNavigationRegister');
         
       })
 } 

    useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      getToken();
    });
    return unsubscribe;
  }, [navigation]);

    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity style={{flexDirection:'row'}} onPress={() => navigation.goBack(null)}>
                <MaterialIcons name="arrow-back" size={24} color={secondary} />     
                </TouchableOpacity>
            </View>
            <Spinner visible={spinner}  color={primary600} />

            <View style={styles.container}>  
                <Text style={{ fontSize:24, fontFamily: "Gotham-Bold", color: "#005A6D", marginTop:15, marginBottom:10 }}>Mensualidades</Text>
                <Text style={{ fontSize:14, fontFamily: "Gotham-Bold", color: "rgba(0, 0, 0, 0.6)", marginBottom: 10}} >Aquí puedes adquirir mensualidades de los diferentes puntos de servicio, para tu carro moto
                    moto, bicicleta o patineta. filtra acorde acada opción.
                </Text>
            </View>


            <View style={{backgroundColor:secondary50,padding:20,borderRadius:15,marginVertical:10, marginRight:20, marginLeft:20}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <View>
                            <Text style={{color: "#002D33",fontSize: 24,fontFamily: 'Gotham-Bold',textAlign:'left'}}>Adquiere tu mensualidad</Text>
                        </View>    
                    </View>
                    <Text style={{color: "rgba(0, 45, 51, 0.8)",fontSize: 16,fontFamily: 'Gotham-Light',marginTop:5}}>
                        Y disfruta de todos los beneficios que tenemos para ti.
                    </Text>
                    <View style={{flexDirection: 'row' }}>
                        {/* <TouchableOpacity style={styles.button1} onPress={ () => navigation.navigate('ListParkingMonthly' , {'listMonthlys' : listMonthlys, 'setListMonthlys' : setListMonthlys })} > */}
                        <TouchableOpacity style={styles.button1} onPress={ () => navigation.navigate('ListParkingMonthly' , {'listMonthlys' : [] })} >
                            <Text style={{ fontSize:14, color: "#F2FAE0",fontFamily: 'Gotham-Bold' }} >COMPRAR</Text>
                        </TouchableOpacity>
                    </View> 
            </View>

            <View style={{backgroundColor: "#F2FAE0",padding:20,borderRadius:15,marginVertical:10, marginRight:20, marginLeft:20, marginTop:10}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <View>
                            <Text style={{color: "#002D33",fontSize: 24,fontFamily: 'Gotham-Bold',textAlign:'left'}}>¿Eres beneficiario?</Text>
                        </View>    
                    </View>
                    <Text style={{color: "rgba(0, 45, 51, 0.8)",fontSize: 16,fontFamily: 'Gotham-Light',marginTop:5}}>
                        Adquiere tu beneficio con el código que se te ha asignado.
                    </Text>
                    <View style={{flexDirection: 'row' }}>
                      
                            <TouchableOpacity style={styles.button2} onPress={()=> navigation.navigate('CodeBeneficiary')}>
                                <Text style={{ fontSize:14, color: "#005A6D",fontFamily: 'Gotham-Bold' }} >FAMILIAR</Text>
                            </TouchableOpacity>
                        
                            <TouchableOpacity style={styles.button3} onPress={()=> navigation.navigate('CodeBeneficiary')}>
                                <Text style={{ fontSize:14, color: "#F2FAE0",fontFamily: 'Gotham-Bold' }} >CORPORATIVO</Text>
                            </TouchableOpacity>
                        
                       
                    </View> 
            </View>

            <TouchableOpacity onPress={() => navigation.navigate('ListMonthly')}>
                <View  
                    style={{ backgroundColor: surface,
                             borderRadius: 5,
                             width:width-40,
                             marginLeft: 20,
                             marginRight: 1,
                             shadowColor: "#000",
                             shadowOffset: {
                             width: 0,
                             height: 1,
                             },
                             shadowOpacity: 0.49,
                             shadowRadius: 4.65,
                             elevation: 3,
                             marginBottom:10,
                             marginTop:10,
                             paddingTop:5,
                    }}
                    > 
                    <View>
                        <Text style={{ fontFamily: "Gotham-Bold", color: "#002D33",fontSize:16,marginTop:10, marginLeft:20 }}>
                            Seleccionar mensualidad                                          
                        </Text>
                        <Text style={{ fontFamily: "Gotham-Bold", color: "rgba(0, 45, 51, 0.8)",fontSize:16,marginTop:3, marginLeft:20, marginBottom:10 }}>
                            Tienes {cont} mensualidades disponibles 
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>  

        </ScrollView>
    );


}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingHorizontal:20,
      paddingBottom:20,
      paddingTop:10,
    },
    button1: {
        borderRadius: 10,
        backgroundColor: "#005A6D",
        alignItems: 'center', 
        padding: 6,
        width: 170,
        height:34,
        marginTop:15,
        justifyContent: 'center',
        marginLeft:'auto'
    },
    button2: {
        borderRadius: 10,
        backgroundColor: "#FFFFFF",
        alignItems: 'center', 
        padding: 6,
        width: 140,
        height:34,
        marginTop:15,
        marginLeft: 150,
        borderWidth: 1,
        borderColor:'#005A6D', 
    },
    button3: {
        borderRadius: 10,
        backgroundColor: "#005A6D",
        right: 300,
        top: 15,
        padding: 6,
        width: 140  ,
        height:34,
        alignItems: 'center',
        justifyContent: 'center'
    }
})