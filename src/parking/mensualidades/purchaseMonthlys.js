import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight, faTrash, faRotate} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
import SumaryPasadia from '../pasadia/sumaryPasadia';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function PurchaseMonthly ({route, navigation}){

    const [isSelected, setSelection] = useState(false);
    const [typeVehicleId1, setTypeVehicleId1] = useState(null);
    const [licensePlate1, setLicensePlate1] = useState(null);
    const [vehicleId1, setvehicleId1] = useState(null);
    const [typeVehicleId2, setTypeVehicleId2] = useState(null);
    const [licensePlate2, setLicensePlate2] = useState(null);
    const [vehicleId2, setvehicleId2] = useState(null);
    const sheetRef = React.useRef(null);
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [dialogReserveVisible,setDialogReserveVisible] = useState(false);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [services, setServices] = useState([]);
    const [price, setPrice] = useState('0');
    const [expanded, setExpanded] = useState(true);
    const [time, setTime] = useState(1);
    const [totalListMonthly, setTotalListMonthly] = useState([])    

    const { listMonthlys } = route.params

    const getToken = async () => {
        try {
        const value = await AsyncStorage.getItem("token");
        if (value !== null) {
            global.cont = 0
            global.token = value;
            setToken(value);
            //getVehicles(value);
            global.parkId =  await AsyncStorage.getItem('parkingId');
            global.monthlyId =  await AsyncStorage.getItem('monthlyId');
            setvehicleId1(await AsyncStorage.getItem('vehicleId1'));
            setLicensePlate1(await AsyncStorage.getItem('vehiclePlate1'));
            setTypeVehicleId1(await AsyncStorage.getItem('vehicleTypeId1'));
            setvehicleId2(await AsyncStorage.getItem('vehicleId2'));
            setLicensePlate2(await AsyncStorage.getItem('vehiclePlate2'));
            setTypeVehicleId2(await AsyncStorage.getItem('vehicleTypeId2'));
            
            console.log(`llega a purchaseMonthlys`);
            console.log(listMonthlys);
            setTotalListMonthly(listMonthlys)
            
            console.log(`__________________________________`);

            getParkingInfo();
            //getMonthlyInfo(value);
            await cargarPrecio()

        }
        } catch (error) { }
    };

    const terms = () => {
        setDialogTermsVisible(true);
        
    }
    const _handlePress = () => {
        setExpanded(!expanded);
    }

    //Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);

            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    //metodo consulta mensualidades de parqueadero
    const getMonthlyInfo =  async (token) =>{

        const config = {
        headers: { Authorization: token },  
        };
        setSpinner(true);
        //const car =  await AsyncStorage.getItem("vehicleTypesId") 
        var id = parseInt(global.monthlyId);
        //console.log(id);
        axios.post(`${API_URL}customer/app/parking/getMonthlyInfo`,{
            monthlyId: id
            }, config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            //console.log(response.data.ResponseMessage[0].price);
            if(cod === 0){
                let list = response.data.ResponseMessage;
                setServices(list);
                //global.services = list
                
                //global.precio = response.data.ResponseMessage[0].price 
            }else{
                Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
            } 
            }).catch(error => {
            setSpinner(false);
            console.log(error.response  );
            Alert.alert("error prueba",error.response.data.ResponseMessage);
            console.log(error);
            }) 
        }

    const saveMonthly = () =>{

        const config = {
        headers: { Authorization: global.token },  
        };
        //setSpinner(true);
        //const car =  await AsyncStorage.getItem("vehicleTypesId") 
        //var id = parseInt(global.monthlyId);

        let data;

        console.log(listMonthlys);
        Alert.alert("INFORMACIÓN","Mensualidad creada correctamente");
        navigation.navigate('ListMonthly');

        // listMonthlys.map((monthly) => {
            

        //     data = {
        //         "vehicles": [],
        //         "months": 1,
        //         "subProductId": monthly.monthly_id,
        //         "parkingId": monthly.parking_id
        //     }

        //     monthly.vehicles.map((vehicle) => {
        //         const val = { id : vehicle.vehicle_id }
        //         data.vehicles.push(val)
        //     })

        //     if(monthly.beneficiaries){

        //         const dat = {'id': '4', 'beneficiaries': monthly.beneficiaries}

        //         console.log(`guardar beneficiarios ${JSON.stringify(dat)}`);
        //     }else{
        //         console.log(`no guarda beneficiarios`);
        //     }

        //     //console.log(data);
        //     // axios.post(`${API_URL}customer/app/naturalMonthlyPayment`,data, config).then(async (response) => {
        //     //     setSpinner(false);
        //     //     const cod = response.data.ResponseCode;
        //     //     //console.log(response.data.Data);
        //     //     if(cod === 0){
        //     //         console.log(response.data.ResponseMessage);
        //     //         await payMonthly(response.data.Data.id);
        //     //     }else{
        //     //         Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
        //     //     }  
        //     // }).catch(error => {
        //     //     setSpinner(false);
        //     //     console.log(error.response  );
        //     //     Alert.alert("error prueba",error.response.data.ResponseMessage);
        //     //     console.log(error);
        //     // })   
        // })

        //Alert.alert("Se creó la mensualidad correctamente");
        //navigation.navigate('ListMonthly');
    }

    //método para pagar mensualidad.
    const payMonthly = (id) =>{

        const config = {
        headers: { Authorization: global.token },  
        };
        setSpinner(true);
        
        axios.put(`${API_URL}customer/app/naturalMonthlyPayment/pay/${id}`,{}, config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            
            if(cod === 0){

                //newMonthly(id);
                return ''
            }else{
                Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
            }   
            }).catch(error => {
            setSpinner(false);
            console.log(error.response  );
            Alert.alert("error prueba",error.response.data.ResponseMessage);
            console.log(error);
            }) 
    }

    const saveBeneficiary = () => {
        const config = {
        headers: { Authorization: global.token },  
        };
        setSpinner(true);
        
        axios.put(`${API_URL}customer/app/naturalMonthlyPayment/pay/${id}`,{}, config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            
            if(cod === 0){

                //newMonthly(id);
                return ''
            }else{
                Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
            }   
            }).catch(error => {
            setSpinner(false);
            console.log(error.response  );
            Alert.alert("error prueba",error.response.data.ResponseMessage);
            console.log(error);
            })
    }
    
    const newMonthly = async (id) => {
        await AsyncStorage.setItem('newMonthlyId',id.toString());
        navigation.navigate('ActiveMonthly'); 
    }


    

    const tiempo = (op, index) =>{
        let cantidad = totalListMonthly[index].monthly_cant
        console.log(`entra cantidades ${cantidad}`);
        let data = totalListMonthly
        if( op === 0){
            if(cantidad != 1){
                cantidad = cantidad - 1
                console.log(`entra cantidades ${cantidad}`);
                data[index].monthly_cant = cantidad
                setTotalListMonthly(data)
            }
        }
        else{
            cantidad = parseInt(cantidad) + 1
            data[index].monthly_cant = cantidad
            setTotalListMonthly(data)
        }
        cargarPrecio()
        navigation.navigate('PurchaseMonthlys' , {"listMonthlys": totalListMonthly});
    }

    const cargarPrecio = () => {
        let precio = 0

        totalListMonthly.map((monthly) => {
            const sub_total = monthly.monthly_cant * monthly.monthly_price
            precio = precio + sub_total
        })
        console.log(`precio total ${precio}`)
        if(precio.toString().length > 3){
            const start = precio.toString().substring(0,(precio.toString().length -3))
            const end = precio.toString().substring((start.length), precio.toString().length)        
            setPrice(`${start}.${end}`)
        }
        else{
            setPrice(precio)
        }
       
    }

    const unidad = (cant) => {
        if(cant.toString().length > 3){
                const start = cant.toString().substring(0,(cant.toString().length -3))
                const end = cant.toString().substring((start.length), cant.toString().length)        
                return `${start}.${end}`
        }else{
                return cant
        }
    }
    
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
        //getParkingList();
        });
        return unsubscribe;
    }, [navigation]);

    return(
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface,marginTop:20}}>
                    <TouchableOpacity onPress={() => navigation.navigate('ListParkingMonthly', {"listMonthly": totalListMonthly})}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
            <View style={styles.container}>
        
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:24, color:secondary, marginBottom:40, marginTop:20}}>Mensualidades</Text>
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:18, color:secondary, marginBottom:40, marginTop:20}}>Selecciona la cantidad de meses</Text>

                   {/*  tarjeta mensualidades */}

                   <View style={{marginBottom:20}} >
                   {   totalListMonthly.map((monthly, index) =>{
                        return <TouchableOpacity style={{marginTop:20}} key={index}>
                                    <View style={{flexDirection:'row'}} >
                                        <View style={{flexDirection:'row',marginRight:10, marginTop:18}}>
                                            <FontAwesomeIcon size={25} icon={monthly.monthly_vehicle1 === 1 ? faCarSide : monthly.monthly_vehicle1 === 2 ? faMotorcycle : faBicycle } color={secondary} /> 
                                            <Text> + </Text> 
                                            <FontAwesomeIcon size={25} icon={monthly.monthly_vehicle2 === 1 ? faCarSide : monthly.monthly_vehicle2 === 2 ? faMotorcycle : faBicycle } color={secondary} /> 
                                        </View>
                                        <View style={{marginRight:30}}>
                                        
                                            <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>{monthly.monthly_name}</Text>
                                            <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>${unidad(monthly.monthly_price)}</Text>
                                            {/* <Text style={{ fontFamily: 'Gotham-Bold', fontSize:14, color:'#1C7A00'}}>Eliminar  <FontAwesomeIcon size={15} icon={faTrash} color={'#1C7A00'}/> </Text> */}
                                        </View>
                                        <View style={{flexDirection:'row', backgroundColor:'#FFF', marginTop:12, height:32, padding:5, borderRadius:16,borderWidth:1, borderColor:'#90D400',justifyContent:'flex-end',marginLeft:'auto' ,right:15,}}>
                                            
                                                <Text style={{ fontSize:20, color:secondary, fontFamily:'Gotham-Bold'}} onPress={()=> tiempo(0, index)}> - </Text>
                                                <Text style={{ fontSize:18, color:secondary, fontFamily:'Gotham-Bold'}}> {monthly.monthly_cant} </Text>
                                                <Text style={{ fontSize:18, color:secondary, fontFamily:'Gotham-Bold'}} onPress={()=> tiempo(1, index)}> + </Text>
                                                
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            })

                    }
                   </View>

                     {/* medio de pago */}
                    <NativeBaseProvider>
                        <Center flex={1}>   
                            <ScrollView>
                                <Box m={3} style={{border:0 ,left: -25 }}>
                                    <Accordion allowMultiple _text={{border:0}} border={0} onTouchStart={_handlePress}>
                                        <Accordion.Item _text={{color:secondary}} >
                                            <Accordion.Summary  _expanded={{backgroundColor:surface,}}>
                                                <View style={{flexDirection:'row'}}>
                                                    <Text style={{fontFamily:'Gotham-Bold',fontSize:24,color:secondary}}>Medio de pago</Text>
                                                    <View style={{marginLeft:160}}>
                                                        <MaterialIcons name={expanded ? "keyboard-arrow-down":"keyboard-arrow-up"} size={30} color={secondary} />
                                                    </View> 
                                                </View>
                                            </Accordion.Summary>
                                                <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <Ionicons name="card-outline" size={24} color={secondary} />
                                                        <View style={{marginLeft:30}}>
                                                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>VISA *111</Text>
                                                            <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color: "rgba(3, 25, 27, 0.75)" }}>Nombre del titular</Text>
                                                        </View>
                                                            <Text style={{ fontSize: 14, marginTop: 10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}}>CAMBIAR</Text>
                                                    </View> 
                                                </Accordion.Details>
                                                <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <Ionicons name="sync-outline" size={24} color={secondary} />
                                                        <View style={{marginLeft:30}}>
                                                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Renovación automática</Text>
                                                            <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color: "rgba(3, 25, 27, 0.75)" }}>Mensual</Text>
                                                        </View>
                                                        
                                                            <Ionicons name="toggle-outline" size={24} color={secondary} style={{ fontSize: 24,right:20, marginTop: 5,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}} />
                                                    </View> 
                                                </Accordion.Details>
                                        </Accordion.Item>
                                    </Accordion>
                                </Box>
                            </ScrollView>
                        </Center>
                    </NativeBaseProvider>
                      
                    <View style={styles.container2}>
                            <View style={{flexDirection:'row'}}>
                                <Checkbox
                                    value={isSelected}
                                    onValueChange={setSelection}
                                    style={{ alignSelf: "center",marginLeft:10,color:primary800, marginRight:10}}
                                    color={isSelected ? primary600 : surfaceMediumEmphasis}
                                />
                                <TouchableOpacity onPress={terms}>
                                    <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Bold',marginTop:10,textDecorationLine:'underline',bottom:4}}>Acepto terminos y condiciones</Text>
                                </TouchableOpacity>
                        </View>

                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={dialogReserveVisible}
                        >
                    <TouchableWithoutFeedback onPress={() => setDialogReserveVisible(!dialogReserveVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <Text style={styles.title2}>Descripcion opcional</Text>
                                </View>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center',marginTop:10}}>
                                    <Text style={styles.text2}>Descripción opcional.....</Text>
                                </View>
                                <View style={{marginBottom:10,alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <TouchableOpacity style={styles.button1} onPress={()=> navigation.navigate('SumaryMonthly')}>
                                        <Text style={styles.titleButton1}>ACEPTAR Y CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>



                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogTermsVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Terminos y condiciones</Text>
                                <Text style={styles.text2}>Tortor bibendum pretium lacinia at risus. Suspendisse volutpat, neque felis, dui, sagittis sapien commodo vulputate. Quis eget tortor amet ipsum, morbi mollis semper. Commodo leo imperdiet fermentum lobortis suspendisse. Dictumst eget in sed nibh. Eu volutpat ut vel adipiscing erat gravida maecenas ut vitae. Nunc sodales arcu magna in libero in. Ligula eget integer sed diam elit tristique. </Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>ACEPTAR Y CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
               
                <View style={{marginVertical:20,alignItems:'center',}}>
                    {
                        isSelected ? 
                        <View style={{ flexDirection: 'row'}}>
                           
                            <TouchableOpacity  style={{backgroundColor: "#005A6D",padding: 14,borderRadius: 15,width: 379, height:48}} onPress={() =>  saveMonthly()}>
                                <View>
                                    <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                        <Text style={{color: "#F2FAE0",fontSize: 15,fontFamily:'Gotham-Bold',}}>PAGAR ${price}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        :
                        <View style={{ flexDirection: 'row'}}>
                            
                            <TouchableOpacity disabled style={{backgroundColor: "rgba(0, 45, 51, 0.15)",padding: 14,borderRadius: 15,width: 379, height:48}}>
                               
                                    <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                        <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>PAGAR ${price}</Text>
                                    </View>
                                      
                            </TouchableOpacity>
                        </View>
                        
                    }
                </View>



                    </View>



            </View>
        
        </ScrollView>

    )

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-100,
        paddingTop:10,
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    box: {
        width:120,
        height:127,
        //borderColor: 'black',
        //  borderWidth: 1,
        borderRadius: 4,
        backgroundColor: "#ffffff",
        marginRight: 10,
        left: -20,
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 1,
        },
        shadowOpacity: 0.49,
        shadowRadius: 4.65,
        elevation: 3,
    },
    box1: {
        flexDirection:'row',
        padding: 16,
        width: 440,
        height: 200,
        backgroundColor: "#FFFFFF"    
    },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        alignContent: 'center',
        justifyContent: 'center',
        marginLeft:80
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },

})