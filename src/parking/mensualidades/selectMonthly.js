import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert, Modal, TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight,faTrash} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function SelectMonthly ({route, navigation}){
    const [spinner, setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null); 
    const [services, setservices] = useState([]);
    const [dialogVisible, setDialogVisible] = useState(false);
    //const [totalMonthly, setTotalMonthly] = useState([]);

    const   { listMonthlys } = route.params

    //Metodo para obtener el token
    const getToken = async () => {
        try {
            const value = await AsyncStorage.getItem("token");
            if (value !== null) {
                global.token = value;
                setToken(value);
                global.parkId =  await AsyncStorage.getItem('parkingId');
                               
                AsyncStorage.removeItem("vehicleId1");
                AsyncStorage.removeItem("vehicleId2");
                AsyncStorage.removeItem("vehicleTypeId1");
                AsyncStorage.removeItem("vehicleTypeId2");
                AsyncStorage.removeItem("vehiclePlate1");
                AsyncStorage.removeItem("vehiclePlate2");
                AsyncStorage.removeItem('benName');
                AsyncStorage.removeItem('benEmail');
                AsyncStorage.removeItem('benDocument');
                AsyncStorage.removeItem('benDocumentType');
                AsyncStorage.removeItem('benPhone');
                getParkingServices(value)
                getParkingInfo(value);
            }
        } catch (error) { }[]
    };

    //Método para obtener la información de parqueadero
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
                setSpinner(false);
                const cod = response.data.ResponseCode;
                if(cod === 0){
                    setParking(response.data.ResponseMessage);
                    const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                    const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                    setOpenHour(hourO);
                    setCloseHour(hourF);

                }else{
                Alert.alert("ERROR","No se pudo traer el parqueadero");
                }
            }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
            })
    }

    //metodo consulta mensualidades de parqueadero
    const getParkingServices =  async (token) =>{

        const config = {
        headers: { Authorization: token },  
        };
        setSpinner(true);
        
        var id = parseInt(global.parkId);
    
        axios.post(`${API_URL}customer/app/parking/parkingMonhtlyServices`,{parkingId: id}, config).then(response => {
                setSpinner(false);
                const cod = response.data.ResponseCode;
                if(cod === 0){
                    let list = response.data.ResponseMessage;
                    setservices(list);
                    console.log(list);
                }else{
                    Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
                } 
            }).catch(error => {
                setSpinner(false);                
                Alert.alert("error prueba",error.response.data.ResponseMessage);                
            }) 
    }
    
    //Metodo para enrutar a la lista de vehiculos
    const listVehicles = async (service) => {
        
        
        listMonthlys[listMonthlys.length - 1].monthly_id = service.id
        listMonthlys[listMonthlys.length - 1].monthly_name = service.name
        listMonthlys[listMonthlys.length - 1].monthly_vehicle1 = service.vehicle_type_id_1
        listMonthlys[listMonthlys.length - 1].monthly_vehicle2 = service.vehicle_type_id_2
        listMonthlys[listMonthlys.length - 1].monthly_price = service.price
        listMonthlys[listMonthlys.length - 1].monthly_cant = 1

        if (service.vehicle_type_id_1 === 1 && service.vehicle_type_id_2 === 1) {
            await AsyncStorage.setItem("vehicleId", '1');
        }
        else if(service.vehicle_type_id_1 === 1 && service.vehicle_type_id_2 === 2){
            await AsyncStorage.setItem("vehicleId", '4');
        }
        else if(service.vehicle_type_id_1 === 1 && service.vehicle_type_id_2 === 3){
            await AsyncStorage.setItem("vehicleId", '5');
        }
        else if(service.vehicle_type_id_1 === 2 && service.vehicle_type_id_2 === 2){
            await AsyncStorage.setItem("vehicleId", '2');
        }
        else if(service.vehicle_type_id_1 === 2 && service.vehicle_type_id_2 === 3){
            await AsyncStorage.setItem("vehicleId", '6');
        }
        else if(service.vehicle_type_id_1 === 3 && service.vehicle_type_id_2 === 3){
            await AsyncStorage.setItem("vehicleId", '3');
        }
        
        await AsyncStorage.setItem("monthlyId", service.id.toString());
        await AsyncStorage.setItem("where", '0');
        navigation.navigate('ListVehicleMonthly', {'listMonthlys' : listMonthlys});
    }

    const evalResponse =  async (resp) => {
        console.log(resp);
        if (resp == 0) {
            setDialogVisible(!dialogVisible);
        }
        else{
            await AsyncStorage.setItem("vehicleId", global.vehicleId);
            navigation.navigate('ListVehicleMonthly');
        }
    }
    const unidad = (cant) => {
        if(cant.toString().length > 3){
                const start = cant.toString().substring(0,(cant.toString().length -3))
                const end = cant.toString().substring((start.length), cant.toString().length)        
                return `${start}.${end}`
        }else{
                return cant
        }
    }   
    

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
        //getParkingList();
        });
        return unsubscribe;
    }, [navigation]);

    return(
       <ScrollView>
             <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('ListParkingMonthly')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
            <View style={styles.container}>
                <View style={{paddingLeft: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10,marginBottom:20 }}>
                    <View style={{ marginTop: 15}}>
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                        {
                            parking !== null ?
                            parking.name
                            :
                            ""
                        }
                        </Text>
                        {
                            parking !== null ?
                                parking.address !== null ? 
                                    <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                :
                                <Text style={styles.text}>No hay dirección disponible</Text>
                            :
                            <Text style={styles.text}>No hay dirección disponible</Text>
                        }
                        
                        <View style={{flexDirection: 'row'}} >
                        {
                            parking !== null &&
                                parking.open == 1 ? 
                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                :
                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                        }    
                            
                            {
                                parking !== null ?
                                    parking.finalHour !== "" && parking.initialHour !== "" ? 
                                        <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                    :
                                    <Text style={styles.text}>No hay horario disponible</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                            }
                        </View>
                    </View>
                    <View style={{alignItems: 'center',justifyContent: 'center',}}>
                        <View style = {styles.lineStyle} />
                    </View>
                    <View>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Medium',}}>Tarifa base / Disponibilidad</Text>
                    </View>
                    <View style={{flexDirection:'row',}}>
                        {
                            parking !== null ?
                                parking.tariff.length !== 0 ?
                                
                                    parking.tariff.map( (tarifa) => {
                                        return <View style={{flexDirection:'row',marginTop:10, marginRight:10}} key={tarifa.vehicleType}>
                                                    <FontAwesomeIcon size={20} 
                                                    icon={tarifa.vehicleType === 1 ? faCar
                                                        :tarifa.vehicleType === 2 ? faMotorcycle :tarifa.vehicleType === 3 && faBicycle } color={primary700} />
                                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> ${tarifa.price} / <Text style={{color: surfaceMediumEmphasis}}>24</Text></Text>
                                                </View>
                                        })
                                :
                                <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                                </View>
                            :
                                <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                                </View>
                        }
                    </View>
                </View>      

                    {/* tarjeta tipo mensualidad */}
                     <View style={{marginBottom:50}} >
                   {   services.map((service) =>{
                        return <TouchableOpacity key={service.id} onPress={() => listVehicles(service) }> 
                            <View style={{flexDirection:'row', marginBottom:20}} >
                                <View style={{flexDirection:'row',marginRight:10, marginTop:18}}>
                                    <FontAwesomeIcon size={25} icon={service.vehicle_type_id_1 === 1 ? faCarSide : service.vehicle_type_id_1 === 2 ? faMotorcycle : faBicycle} color={secondary} /> 
                                    <Text> + </Text> 
                                    <FontAwesomeIcon size={25} icon={service.vehicle_type_id_2 === 1 ? faCarSide : service.vehicle_type_id_2 === 2 ? faMotorcycle : faBicycle} color={secondary} /> 
                                </View>
                                <View style={{marginRight:30}}>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:18, color:secondary}}>Mensualidad</Text>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>{service.name}</Text>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>{service.vehicle_type_id_1 === 1 ? "Carro" : service.vehicle_type_id_1 === 2 ? "Moto" : "Bicicleta" } - {service.vehicle_type_id_2 === 1 ? "Carro" : service.vehicle_type_id_2=== 2 ? "Moto" : "Bicicleta" }</Text>
                                </View>
                                <View style={{marginTop:10,justifyContent:'flex-end',marginLeft:'auto', alignContent: 'center',marginBottom:18,right:10 }}>
                                        <Text style={{ fontSize:18, color:secondary, fontFamily:'Gotham-Bold'}}>${unidad(service.price)}</Text>
                                        <Text style={{ fontSize:14, color:'#1C7A00', fontFamily:'Gotham-Bold'}}>VER MAS</Text>
                                </View>
                             </View>
                        </TouchableOpacity>
                         })

                    }
                   </View>                  
                    
                    

                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color: 'rgba(0, 45, 51, 0.6)', marginBottom:15 }}>Beneficios</Text>

                        <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                                {
                                    <FontAwesomeIcon size={25} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:30,marginRight:20}}/>
                                }
                                    <View>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#008293",fontSize:12,marginTop:15}}>
                                             Ahorro en dinero hasta           
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#1C7A00",fontSize:16,marginTop:5}}>
                                              50% en tarifa regular
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                        </Text>
                                    </View>
                        </View>

                        <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                                {
                                    <FontAwesomeIcon size={25} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:30,marginRight:20}}/>
                                }
                                    <View>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#008293",fontSize:12,marginTop:15}}>
                                             Legal          
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#1C7A00",fontSize:16,marginTop:5}}>
                                              Terminos y condiciones
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                        </Text>
                                    </View>
                        </View>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogVisible}
                >
                    <TouchableWithoutFeedback >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Terminar</Text>
                                <Text style={styles.text2}>Tortor bibendum pretium lacinia at risus. Suspendisse volutpat, neque felis, dui, sagittis sapien commodo vulputate. Quis eget tortor amet ipsum, morbi mollis semper. Commodo leo imperdiet fermentum lobortis suspendisse. Dictumst eget in sed nibh. Eu volutpat ut vel adipiscing erat gravida maecenas ut vitae. Nunc sodales arcu magna in libero in. Ligula eget integer sed diam elit tristique. </Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => evalResponse(0)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => evalResponse(1)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>

            </View>


                
            
       </ScrollView>

    )

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-350,
        paddingTop:10,
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    box: {
        width:120,
        height:140,
        //borderColor: 'black',
        //  borderWidth: 1,
        borderRadius: 4,
        backgroundColor: "#ffffff",
        marginRight: 10,
        paddingTop: 15,
        left: -20,
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 1,
        },
        shadowOpacity: 0.49,
        shadowRadius: 4.65,
        elevation: 3,
    },
    box1: {
        flexDirection:'row',
        padding: 16,
        width: 440,
        height: 200,
        backgroundColor: "#FFFFFF"    
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
     modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    
})