import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,CheckBox,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../../utils/colorVar';
import { productIdPasadia } from '../../utils/varService';
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ListMonthly({navigation}){
    const [spinner, setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [monthlys, setMonthlys] = useState([]);

    const getToken = async () => {
    try {
      const value = await AsyncStorage.getItem("token");
      if (value !== null) {
        global.token = value;
        setToken(value);
        getListMonthly(value);
      }
    } catch (error) { }
  };


  const getListMonthly = async (token) => {
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
    
        axios.get(`${API_URL}customer/app/getListMonthlyByUser`,config).then(response => {
            
            
            const cod = response.data.ResponseCode;
            setSpinner(false);
            if(cod === 0){
                console.log(`Archivo config __________ ${JSON.stringify(response.data)}`)
                const data = {
                    duration: 1,
                    id: 99999,
                    inicia: "2022-02-17 16:06:23",
                    name: "Mensualidad Juridica Tipo 1 $95.000",
                    parking_id: 20,
                    price: 95000,
                    status_id: 1,
                    vehicles: [
                      {
                        id: 34,
                        plate: "bbb123",
                        vehicle_type_id: 1,
                      },
                      {
                        id: 35,
                        plate: "abc00a",
                        vehicle_type_id: 2,
                      },
                    ],
                  }
                  response.data.ResponseMessage.push(data)
                setMonthlys(response.data.ResponseMessage);
                console.log(response.data.ResponseMessage);
                //navigation.navigate('SumaryPasadia');
            }else{
              Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            //console.log(error.response.data.ResponseMessage);
          })
    }
    const viewMonthly =  async (id) =>{
        //aqui se valida si esta pendiente de pago
        if(id == 99999){
          navigation.navigate('LegalActive');
        }else{
          await AsyncStorage.setItem('newMonthlyId',id.toString());
          navigation.navigate('ActiveMonthly');
        }
        
    }

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      getToken();
    });
    return unsubscribe;
  }, [navigation]);

    return(

       <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Mensualidad')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>

                <View style={styles.container}>
                <Text style={{ fontFamily: 'Gotham-Bold', fontSize:24, color:secondary, marginBottom:10}}>Seleccione Mensualidad</Text>
                <Text style={{ fontFamily: 'Gotham-Light', fontSize:14, color: "#000", marginBottom:10}}>Descripción</Text>
                <View >
                    {
                    monthlys.map( (monthly) => {
                        return <TouchableOpacity style={{justifyContent:'center'}} key={monthly.id} onPress={() => viewMonthly(monthly.id) }>
                                    <View
                                        style={{
                                            backgroundColor: surface,
                                            borderRadius: 5,
                                            width:width-40,
                                            marginLeft: 0,
                                            marginRight: 1,
                                            shadowColor: "#000",
                                            shadowOffset: {
                                            width: 0,
                                            height: 1,
                                            },
                                            shadowOpacity: 0.49,
                                            shadowRadius: 4.65,
                                            elevation: 3,
                                            marginBottom:10,
                                            marginTop:10,
                                            paddingTop:5,
                                        }}
                                        >   
                                            <View>
                                                 <Text style={{fontFamily:'Gotham-Bold', fontSize:14, color: 'rgba(0, 45, 51, 0.8)', marginLeft:20  }}>
                                                {
                                                    monthly.name
                                                }      
                                                </Text> 
                                            </View>

                                            <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                                                {/*
                                              
                                                   <FontAwesomeIcon size={25} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:20,marginRight:20}}/>
                                                */}
                                                
                                                <Text style={{textTransform: 'uppercase', fontFamily: "Gotham-Bold", color: "rgba(0, 45, 51, 0.8)",fontSize:14,marginTop:10 }}>
                                                   { monthly.status_id === 1 ? "Activo" : "Pendiente de pago" } | { monthly.vehicles[0].plate } { monthly.vehicles[1] ? ` - ${monthly.vehicles[1].plate}` : ""}                
                                                </Text>
                                            </View>

                                            <View>
                                                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14, marginLeft:15 }}>
                                                       Vigente por { monthly.duration === 1 ? "1 mes" : `${monthly.duration} meses` }                                                     
                                                </Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                            }) 
                        }
                    
                </View>
                
            </View>
       </ScrollView>


    )


}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingLeft:20,
      paddingBottom:20,
      paddingTop:20,
    },




})