import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import { TextInput as PaperTextInput } from 'react-native-paper';
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function filterList({navigation}){
    const [spinner, setSpinner] = useState(false);
    const [isSelected, setSelection] = useState(false);
    const [timeAm, setTimeAm] = useState(false);
    const [timePm, setTimePm] = useState(false);
    const [timeFull, setTimeFull] = useState(false);
    const [selectedCar, setselectedCar] = useState(false);
    const [selectedMot, setSelectedMot] = useState(false);
    const [selectedByci, setSelectedByci] = useState(false);

    //seleccion horario
    const changeFirst = () => {
        setTimeAm(true); 
        setTimePm(false); 
        setTimeFull(false);
    }
     const changeSecond = () => {
        setTimeAm(false);
        setTimePm(true);
        setTimeFull(false);   
    }
    const changeThird = () => {
        setTimeAm(false);
        setTimePm(false);
        setTimeFull(true)   
    }
    //seleccion vehiculo
    const pressFirst = () => {
        setselectedCar(true); 
        setSelectedMot(false); 
        setSelectedByci(false);
    }
     const pressSecond = () => {
        setselectedCar(false); 
        setSelectedMot(true); 
        setSelectedByci(false);   
    }
    const pressThird = () => {
        setselectedCar(false); 
        setSelectedMot(false); 
        setSelectedByci(true);  
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
        //getToken();
        //getParkingList();
        });
        return unsubscribe;
    }, [navigation]);


    return(

        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity style={{flexDirection:'row'}} onPress={() => navigation.navigate('ListParkingMonthly')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary}/>
                        <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color: secondary}}>Filtrar por</Text>
                    </TouchableOpacity>
                </View>

            <View style={ styles.container }>

                <View >
                    <Text style={{ fontFamily: "Gotham-Bold", fontSize: 16, color: secondary , marginTop: 15, marginBottom:25}}>Tipo de vehículo</Text>
                </View>

                <View style={{flexDirection: 'row',marginBottom:30,marginLeft:10,overflow:'scroll'}}>
                    <Chip  textStyle={{fontSize:15}} style={ selectedCar ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor: 'rgba(0, 45, 51, 0.08)', marginRight:6,}}  selected={selectedCar} selectedColor={secondary} mode='flat' onPress={pressFirst}>Automovil</Chip>
                    <Chip textStyle={{fontSize:15}} style={ selectedMot ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor: 'rgba(0, 45, 51, 0.08)', marginRight:6,}}  selected={selectedMot} selectedColor={secondary} mode='flat' onPress={pressSecond}>Motocicleta</Chip>
                    <Chip textStyle={{fontSize:15}} style={ selectedByci ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor: 'rgba(0, 45, 51, 0.08)', marginRight:6,}}  selected={selectedByci} selectedColor={secondary} mode='flat' onPress={pressThird}>bicicleta</Chip>
                </View>

               {/*  <View style={{ flexDirection: 'row', padding: 16 }}>
                    <TouchableOpacity style={styles.buttons}>
                        <View style={{ flexDirection:'row' }}>
                            {
                                <FontAwesomeIcon size={20} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:3,marginRight:8}}/>
                            }
                            <Text style={{ fontSize:14, fontFamily: "Gotham-Bold", color: "#005A6D", marginTop:3, marginBottom:5 }}>Automovil</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity  style={styles.buttons}>
                        <View style={{ flexDirection:'row' }}>
                            {
                                <FontAwesomeIcon size={20} icon={faMotorcycle} color={secondary} style={{marginLeft:10,marginTop:3,marginRight:8}}/>
                            }
                            <Text style={{ fontSize:14, fontFamily: "Gotham-Bold", color: "#005A6D", marginTop:3, marginBottom:5 }}>Motocicleta</Text>
                        </View>
                    </TouchableOpacity>
                </View> */}

                <View>
                    <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color:secondary, marginTop:30, marginBottom:30 }}>Horario</Text>
                </View>

                <View style={{flexDirection: 'row',marginBottom:30,marginLeft:10,overflow:'scroll'}}>
                    <Chip textStyle={{fontSize:12}} style={ timeAm ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor: 'rgba(0, 45, 51, 0.08)', marginRight:6,}}  selected={timeAm} selectedColor={secondary} mode='flat' onPress={changeFirst}>AM</Chip>
                    <Chip textStyle={{fontSize:12}} style={ timePm ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor: 'rgba(0, 45, 51, 0.08)', marginRight:6,}}  selected={timePm} selectedColor={secondary} mode='flat' onPress={changeSecond}>PM</Chip>
                    <Chip textStyle={{fontSize:12}} style={ timeFull ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor: 'rgba(0, 45, 51, 0.08)', marginRight:6,}}  selected={timeFull} selectedColor={secondary} mode='flat' onPress={changeThird}>24 hrs</Chip>
                </View>
            
                <View>
                    <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color:secondary, marginTop:30, marginBottom:30 }}>Ciudad</Text>
                </View>
                
                <View style={styles.checkBox}>
                    <Text style={styles.textCheck}>Bogotá</Text>
                    <Checkbox
                        value={isSelected}
                        onValueChange={setSelection}
                        style={{marginLeft:295,color:primary800}}
                        color={isSelected ? primary600 : surfaceMediumEmphasis}
                    /> 
                </View>
                <View style={styles.checkBox}>
                    <Text style={styles.textCheck}>Cartagena</Text>
                    <Checkbox
                        value={isSelected}
                        onValueChange={setSelection}
                        style={{ alignSelf: "center",marginLeft:270,color:primary800}}
                        color={isSelected ? primary600 : surfaceMediumEmphasis}
                    /> 
                </View>
                <View style={styles.checkBox}>
                    <Text style={styles.textCheck}>Medellin</Text>
                    <Checkbox
                        value={isSelected}
                        onValueChange={setSelection}
                        style={{ alignSelf: "center",marginLeft:285,color:primary800}}
                        color={isSelected ? primary600 : surfaceMediumEmphasis}
                    /> 
                </View>
                 <View style={styles.checkBox}>
                    <Text style={styles.textCheck}>Todas las ciudades</Text>
                    <Checkbox
                        value={isSelected}
                        onValueChange={setSelection}
                        style={{ alignSelf: "center",marginLeft:200,color:primary800}}
                        color={isSelected ? primary600 : surfaceMediumEmphasis}
                    /> 
                </View>

                <View style={{ flexDirection: 'row'}}>
                    <TouchableOpacity  style={{backgroundColor: "#FFFFFF",padding: 14,borderRadius: 15,width: 190, height:48}}>
                        <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                            <Text style={{color: "#005A6D",fontSize: 14,fontFamily:'Gotham-Bold',}}>CANCELAR</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity  style={{backgroundColor: "#005A6D",padding: 14,borderRadius: 15,width: 172, height:48}} onPress={() => navigation.navigate('SelectedVehicleMonthly')}>
                        <View>
                            <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                <Text style={{color: "#F2FAE0",fontSize: 15,fontFamily:'Gotham-Bold',}}>APLICAR</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>

            
            </View>
        
        </ScrollView>
    )


}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-170,
        paddingTop:10,
    },
    buttons: {
        borderRadius: 16,
       // borderColor: 'red', 
        //borderWidth: 1,  //borderBottomWidth
        padding: 4, 
        backgroundColor: "rgba(0, 45, 51, 0.08)",
        width: 140,
        height: 32,  
        marginRight: 15 ,
        left: -15     
    },
    textCheck: {
        fontFamily: 'Gotham-Bold',
        fontSize:16,
        color: secondary,
    },
    checkBox: {
        flexDirection: 'row',
        marginBottom: 30
    }
})