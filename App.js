import React from 'react';
import { StyleSheet, Text, View, StatusBar, ActivityIndicator  } from 'react-native';
import * as Font from "expo-font";
import { NavigationContainer } from '@react-navigation/native';
import Constants from "expo-constants";
import { createStackNavigator } from '@react-navigation/stack';
import Register from './src/register';
import Login from './src/login';
import FormRegister from './src/formRegister';
import Home from './src/registered/home';
import BarNavigationRegister from './src/registered/barNavigationRegister';
import { primary500,surface } from './src/utils/colorVar';
import DetailParking from './src/parking/detailParking';
import Reserve from './src/parking/reserve/reserve';
import NoVehicle from './src/vehicles/noVehicle';
import AddVehicle from './src/vehicles/addVehible';
import ListVehicles from './src/vehicles/listVehicles';
import Pasadia from './src/parking/pasadia/pasadia';
import ListVehiclesPasadia from './src/parking/pasadia/listVehiclesPasadia';
import SumaryPasadia from './src/parking/pasadia/sumaryPasadia';
import SumaryReserve from './src/parking/reserve/sumaryReserve';
import ScheduledReserve from './src/parking/reserve/scheduledReserve';
import ActiveReserve from './src/parking/reserve/activeReserve';
import TimeExtensionReserve from './src/parking/reserve/timeExtensionReserve';
import ExtendedReserve from './src/parking/reserve/extendedReserve';
import ReserveCompleted from './src/parking/reserve/reserveCompleted';
import PurchasedPasadia from './src/parking/pasadia/purchasedPasadia';
import ActivePasadia from './src/parking/pasadia/activePasadia';
import PasadiaCompleted from './src/parking/pasadia/pasadiaCompleted';
import Account from './src/account/account';
import FormBeParking from './src/account/beParking/formBeParking';
import AddVehicleBeParking from './src/account/beParking/addVehicleBeParking';
import BeParking from './src/account/beParking/beParking';
import GetPoints from './src/account/beParking/getPoints';
import Defeated from './src/account/beParking/defeated';
import Courtesy from './src/account/courtesy/courtesy';
import ListVehicleCourtesy from './src/account/courtesy/listVehicleCourtesy';
import AddVehicleCourtesy from './src/account/courtesy/addVehicleCourtesy';
import ActiveCourtesy from './src/account/courtesy/activeCourtesy';
import ListCourtesy from './src/account/courtesy/listCourtesy';
import CourtesyCompleted from './src/account/courtesy/courtesyCompleted';
import ChangeVehicle from './src/account/courtesy/changeVehicle';
import Valet from './src/parking/valetParking/valet';
import MapsPrueba from './src/parking/mapsPrueba';
import ConsolidatedValet from './src/parking/valetParking/ consolidatedValet';
import ActiveValet from './src/parking/valetParking/activeValet';
import CheckListValet from './src/parking/valetParking/checkListValet';
import RequestVehicle from './src/parking/valetParking/requestVehicle';
import PaymentMethods from './src/parking/paymentMethods/paymentMethods';
import DescriptionBenefit from './src/account/beParking/descriptionBenefit';
import QRVehicle from './src/parking/valetParking/qrVehicle';
import VehicleImperfections from './src/parking/valetParking/vehicleImperfections';
import ValetCompleted from './src/parking/valetParking/valetCompleted';
import BeneficiaryCourtesy from './src/account/courtesy/beneficiaryCourtesy';
import ListParkingPasadia from './src/parking/pasadia/listParkingPasadia';
import ListParking from './src/parking/listParking';
import ListPasadia from './src/parking/pasadia/listPasadia';
import listVehiclesMoto from './src/parking/pasadia/listVehiclesMoto';
import ListReserve from './src/parking/reserve/listReserve';
import ServiceReserve from './src/parking/reserve/serviceReserve';
import ListParkingReserve from './src/parking/reserve/listParkingReserve';
import ListVehicleReserve from './src/parking/reserve/listVehicleReserve';
import Mensualidad from './src/parking/mensualidades/mensualidad';
import ListParkingMonthly from './src/parking/mensualidades/listParkingMonthly';
import SelectMonthly from './src/parking/mensualidades/selectMonthly';
import ListVehicleMonthly from './src/parking/mensualidades/listVehicleMonthly';
import SumaryMonthly from './src/parking/mensualidades/sumaryMonthly';
import PurchasedMonthly from './src/parking/mensualidades/purchasedMonthly';
import ActiveMonthly from './src/parking/mensualidades/activeMonthly';
import FilterList from './src/parking/mensualidades/filterList';
import PurchaseMonthly from './src/parking/mensualidades/purchaseMonthly';
import ListMonthly from './src/parking/mensualidades/listMonthly';
import InfoBeneficiaryMonthly from './src/parking/mensualidades/infoBeneficiaryMonthly';
import PurchaseMonthlys from './src/parking/mensualidades/purchaseMonthlys';
import CodeBeneficiary from './src/parking/mensualidades/codeBeneficiary';
import ListParksBeParking from './src/account/beParking/listParksBePaking'
import SelectedPark from './src/account/beParking/selectedPark'
import SelectVehicle from './src/account/beParking/selectVehicle'
import PurchasedBonus from './src/account/beParking/purchasedbonus'
import ActivedBonus from './src/account/beParking/activedBonus'
import FinalyBonus from './src/account/beParking/finalyBonus'
import ExtendBonus from './src/account/beParking/extendBonus'
import AddBeneficiary from './src/account/courtesy/addBeneficiary'
import BeParkingBlue from './src/account/beParking/beParkingBlue'
import DitailsCourtesy from './src/account/courtesy/ditailsCourtesy'
//_____________________________
import LegalListVehicles from './src/parking/mensualidades/legalListVehicles';
import LegalSumaryMonthly from './src/parking/mensualidades/legalSumaryMonthly';
import LegalActive from './src/parking/mensualidades/legalActive';
//_____________________________
export default class App extends React.Component {
  state = {
    assetsLoaded: false,
    version:null,
  };

  async componentDidMount() {
    await Font.loadAsync({
      'Gotham-Black': require('./assets/fonts/Gotham-Black.otf'),
      'Gotham-Bold':require('./assets/fonts/Gotham-Bold.otf'),
      'Gotham-Book':require('./assets/fonts/Gotham-Book.otf'),
      'Gotham-Light':require('./assets/fonts/Gotham-Light.otf'),
      'Gotham-Medium':require('./assets/fonts/Gotham-Medium.otf'),
    });
    this.setState({version:Constants.manifest.version});
    this.setState({ assetsLoaded: true });
  };

  render(){
    const Stack = createStackNavigator();
    const {assetsLoaded} = this.state;
    if( assetsLoaded ) {
      return (
        <NavigationContainer style={styles.container}>
          <StatusBar backgroundColor={primary500} barStyle="dark-content" />
          <Stack.Navigator initialRouteName="Register" headerMode={'none'}>
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="FormRegister" component={FormRegister} />
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="BarNavigationRegister" component={BarNavigationRegister} />
            <Stack.Screen name="DetailParking" component={DetailParking} />
            <Stack.Screen name="ListParking" component={ListParking} />
            <Stack.Screen name="Reserve" component={Reserve} />
            <Stack.Screen name="NoVehicle" component={NoVehicle} />
            <Stack.Screen name="AddVehicle" component={AddVehicle} />
            <Stack.Screen name="ListVehicles" component={ListVehicles} />
            <Stack.Screen name="Pasadia" component={Pasadia} />
            <Stack.Screen name="ListVehiclesPasadia" component={ListVehiclesPasadia} />
            <Stack.Screen name="SumaryPasadia" component={SumaryPasadia} />
            <Stack.Screen name="ListParkingPasadia" component={ListParkingPasadia} />
            <Stack.Screen name="SumaryReserve" component={SumaryReserve} />
            <Stack.Screen name="ScheduledReserve" component={ScheduledReserve} />
            <Stack.Screen name="ActiveReserve" component={ActiveReserve} />
            <Stack.Screen name="TimeExtensionReserve" component={TimeExtensionReserve} />
            <Stack.Screen name="ExtendedReserve" component={ExtendedReserve} />
            <Stack.Screen name="ReserveCompleted" component={ReserveCompleted} />
            <Stack.Screen name="PurchasedPasadia" component={PurchasedPasadia} />
            <Stack.Screen name="ActivePasadia" component={ActivePasadia} />
            <Stack.Screen name="PasadiaCompleted" component={PasadiaCompleted} />
            <Stack.Screen name="Account" component={Account} />
            <Stack.Screen name="FormBeParking" component={FormBeParking} />
            <Stack.Screen name="AddVehicleBeParking" component={AddVehicleBeParking} />
            <Stack.Screen name="BeParking" component={BeParking} />
            <Stack.Screen name="GetPoints" component={GetPoints} />
            <Stack.Screen name="Defeated" component={Defeated} />
            <Stack.Screen name="DescriptionBenefit" component={DescriptionBenefit} />
            <Stack.Screen name="Courtesy" component={Courtesy} />
            <Stack.Screen name="ListVehicleCourtesy" component={ListVehicleCourtesy} />
            <Stack.Screen name="AddVehicleCourtesy" component={AddVehicleCourtesy} />
            <Stack.Screen name="ActiveCourtesy" component={ActiveCourtesy} />
            <Stack.Screen name="ListCourtesy" component={ListCourtesy} />
            <Stack.Screen name="CourtesyCompleted" component={CourtesyCompleted} />
            <Stack.Screen name="BeneficiaryCourtesy" component={BeneficiaryCourtesy} />
            <Stack.Screen name="ChangeVehicle" component={ChangeVehicle} />
            <Stack.Screen name="Valet" component={Valet} />
            <Stack.Screen name="MapsPrueba" component={MapsPrueba} />
            <Stack.Screen name="ConsolidatedValet" component={ConsolidatedValet} />
            <Stack.Screen name="ActiveValet" component={ActiveValet} />
            <Stack.Screen name="CheckListValet" component={CheckListValet} />
            <Stack.Screen name="RequestVehicle" component={RequestVehicle} />
            <Stack.Screen name="PaymentMethods" component={PaymentMethods} />
            <Stack.Screen name="QRVehicle" component={QRVehicle} />
            <Stack.Screen name="VehicleImperfections" component={VehicleImperfections} />
            <Stack.Screen name="ValetCompleted" component={ValetCompleted} />
            <Stack.Screen name='ListPasadia' component={ListPasadia} /> 
            <Stack.Screen name='ListVehiclesMoto' component={listVehiclesMoto}/>
            <Stack.Screen name='ListReserve' component={ListReserve}/>
            <Stack.Screen name='ServiceReserve' component={ServiceReserve}/>
            <Stack.Screen name='ListParkingReserve' component={ListParkingReserve}/>
            <Stack.Screen name='ListVehicleReserve' component={ListVehicleReserve}/>
            <Stack.Screen name='Mensualidad' component={Mensualidad}/>
            <Stack.Screen name='ListParkingMonthly' component={ListParkingMonthly}/>
            <Stack.Screen name='SelectMonthly' component={SelectMonthly}/>
            <Stack.Screen name='ListVehicleMonthly' component={ListVehicleMonthly}/>
            <Stack.Screen name='SumaryMonthly' component={SumaryMonthly}/>
            <Stack.Screen name='PurchasedMonthly' component={PurchasedMonthly}/>
            <Stack.Screen name='ActiveMonthly' component={ActiveMonthly}/>
            <Stack.Screen name='FilterList' component={FilterList}/>
            <Stack.Screen name='PurchaseMonthly' component={PurchaseMonthly}/>
            <Stack.Screen name='ListMonthly' component={ListMonthly} />
            <Stack.Screen name='InfoBeneficiaryMonthly' component={InfoBeneficiaryMonthly} />
            <Stack.Screen name='PurchaseMonthlys' component={PurchaseMonthlys}/>
            <Stack.Screen name='CodeBeneficiary' component={CodeBeneficiary}/>
            <Stack.Screen name='ListParksBeParking' component={ListParksBeParking}/>
            <Stack.Screen name='SelectedPark' component={SelectedPark}/>
            <Stack.Screen name='SelectVehicle' component={SelectVehicle}/>
            <Stack.Screen name='PurchasedBonus' component={PurchasedBonus}/>
            <Stack.Screen name='ActivedBonus' component={ActivedBonus}/>
            <Stack.Screen name='FinalyBonus' component={FinalyBonus}/>
            <Stack.Screen name='ExtendBonus' component={ExtendBonus}/>
            <Stack.Screen name='AddBeneficiary' component={AddBeneficiary}/>
            <Stack.Screen name='BeParkingBlue' component={BeParkingBlue}/>
            <Stack.Screen name='DitailsCourtesy' component={DitailsCourtesy}/>
            {/* //______________________________________________ */}
            <Stack.Screen name='LegalListVehicles' component={LegalListVehicles}/>
            <Stack.Screen name='LegalSumaryMonthly' component={LegalSumaryMonthly}/>
            <Stack.Screen name='LegalActive' component={LegalActive}/>
            {/* //______________________________________________ */}

          </Stack.Navigator>
        </NavigationContainer>  
      );
    }else {
      return (
        <View style={styles.container}>
            <ActivityIndicator />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: surface
  },
});
